<h3>关于APP</h3>
<p>&nbsp;</p>
<p>该小程序是一个工具平台，励志打造更多适用小功能</p>
<h3>为什么要开发</h3>
<p>&nbsp;</p>
<p>只是有那么一瞬间产生了那么一个小小的想法，来开发了该应用&nbsp;</p>
<p>&nbsp;</p>
<h3>关于作者</h3>
<h4>基础信息</h4>
<p>&nbsp;</p>
<ul>
	<li>姓名：保密</li>
	<li>艺名：兮家小二（叫我小二就好了）</li>
	<li>作者qq: 1720696548</li>
	<li>作者微信: 1720696548</li>
	<li>出生地：四川宜宾叙州区 (具体地址保密)</li>
	<li>现居：四川成都</li>
	<li>当前工作公司：保密</li>
	<li>当前职业年限：2018年-至今</li>
	<li>作者 csdn 地址：<a href="https://gitee.com/link?target=https%3A%2F%2Fblog.csdn.net%2Fqq_41463655">https://blog.csdn.net/qq_41463655</a></li>
	<li>作者 码云地址：<a href="https://gitee.com/wslxm">https://gitee.com/wslxm</a></li>
	<li>作者 bilbil 用户名：兮家小二</li>
</ul>
<h4><a id="性格爱好" class="anchor" href="https://gitee.com/wslxm/spring-boot-plus2/wikis/%E6%9B%B4%E5%A4%9A%E4%BF%A1%E6%81%AF/%E4%BD%9C%E8%80%85%E4%BF%A1%E6%81%AF#%E6%80%A7%E6%A0%BC%E7%88%B1%E5%A5%BD"></a>性格爱好</h4>
<p>&nbsp;</p>
<ul>
	<li>喜欢动漫，不太喜欢看电视剧，喜欢周星驰的电影，喜欢希腊, 北欧神话, 灾难片等电影</li>
	<li>喜欢专研技术，不喜欢勾心斗角，不喜欢管理 (自己都管不好自己,emmm....)</li>
	<li>喜欢单机游戏( 没时间玩，也没ps4等设备，emmm....)， 偶尔玩玩王者荣耀等游戏</li>
	<li>喜欢下雨天，有点小抑郁，不太会说话，也没太多朋友，经常一个人宅家</li>
	<li>喜欢风景好的地方，以及可爱的人</li>
</ul>
<p>&nbsp;</p>
<h4>作者照片</h4>
<p>&nbsp;</p>
<h4><img src="http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vueTinymce/54365000-mceclip0.png" width="302"
	 height="300" /></h4>
