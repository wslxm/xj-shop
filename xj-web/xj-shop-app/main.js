import Vue from 'vue'
import App from './App'
import cuCustom from './colorui/components/cu-custom.vue'
import dict from '@/static/js/dict.js' // 字典
import crud from '@/static/js/crud.js' // 请求
import pageRoute from '@/static/js/pageRoute.js' // 请求


// 在程序中科院通过 this.path 获取 请求地址
//Vue.prototype.path = 'https://api.toolapp.xijia.plus/'; 
let path = 'http://192.168.0.5:12001/';
//let path = 'https://api.toolapp.xijia.plus/';

Vue.config.productionTip = false
Vue.component('cu-custom', cuCustom)
App.mpType = 'app'
Vue.prototype.path = path;
Vue.prototype.dict = dict;
Vue.prototype.crud = crud;
Vue.prototype.pageRoute = pageRoute;

const app = new Vue({
	...App
})
app.$mount()
