export default {

	//上一页 （原数据需定义为data，查询方法需定义为 findPage）
	prePage(ths) {
		if (ths.current == 1) {
			// 没有了
			uni.showToast({
				title: '第一页了哦',
				icon: 'none',
				duration: 2000
			});
			return false;
		} else {
			ths.current -= 1;
			ths.findPage(ths.current);
		}

	},

	//下一页（原数据需定义为data，查询方法需定义为 findPage）
	nextPage(ths) {
		if (ths.current >= ths.data.pages) {
			// 没有了
			uni.showToast({
				title: '最后一页了哦',
				icon: 'none',
				duration: 2000
			});
			return false;
		} else {
			ths.current += 1;
			ths.findPage(ths.current);
		}
	},
	

}
