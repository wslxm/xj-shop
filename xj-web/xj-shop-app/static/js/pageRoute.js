export default {



		/**
		 * 获取当前页面路由  
		 */
		getPage() {
			let currentRoutes = getCurrentPages(); // 获取当前打开过的页面路由数组
			let currentRoute = currentRoutes[currentRoutes.length - 1].route //获取当前页面路由
			let currentParam = currentRoutes[currentRoutes.length - 1].options; //获取路由参数
			// 拼接参数
			let param = ''
			for (let key in currentParam) {
				param += '?' + key + '=' + currentParam[key]
			}
			let localRoute = currentRoute + param;
			console.log(localRoute, "当前页面路由");
			return localRoute;
		}
	}
