export default {
	// 查询
	get(path, uri, params) {
		return this.request(path, uri, null, params, "GET");
	},

	// 添加
	post(path, uri, data, params) {
		return this.request(path, uri, data, params, "POST");
	},

	// 编辑
	put(path, uri, data, params) {
		return this.request(path, uri, data, params, "PUT");
	},

	// 删除
	del(path, uri, params) {
		return this.request(path, uri, null, params, "DELETE");
	},


	request(path, uri, data, params, method) {
		console.debug("post => uri:", uri, "  data:", data, "  params:", params)
		// 处理get请求参数
		for (var key in params) {
			if (uri.indexOf("?") === -1) {
				uri += "?" + key + "=" + params[key];
			} else {
				uri += "&" + key + "=" + params[key];
			}
		}

		let promise = new Promise(function(resolve, reject) {
			return uni.request({
				url: path + uri,
				method: method,
				//params: params,
				data: data,
				header: {
					"TOKEN": uni.getStorageSync("XJ-TOKEN"),
				},
				success: (res) => {
					if (res.data.code !== 200) {
						uni.showToast({
							title: res.data.msg,
							icon: 'none'
						})

						// 判断是否为未登录， 未登录跳转至个人页
						if (res.data.code == 10000 || res.data.code == 10001) {
							uni.showModal({
								title: '提示',
								content: '没有登录,跳转至个人中心进行登录',
								cancelText: "取消", // 取消按钮的文字  
								confirmText: "确定", // 确认按钮文字  
								showCancel: false, // 是否显示取消按钮，默认为 true  
								success: function(res) {
									if (res.confirm) {
										uni.reLaunch({
											url: "/pages/fpage4/index"
										});
									}
								}
							});
						}
						throw new Error("接口返回错误信息：" + res.data.msg)
					} else {
						resolve(res);
					}
				},
				fail: function(e) {
					console.log('请求失败', e);
				},
				complete: function(e) {} //接口调用结束的回调函数（调用成功、失败都会执行）
			});
		})
		console.log("请求结束")
		return promise;
	},


	/**
	 * 文件上传
	 * @author wangsong
	 * @mail  1720696548@qq.com
	 * @date  2021/10/16 0016 12:58
	 * @version 1.0.0
	 */
	upload(path, uri, filePath, params) {
		// 处理请求参数
		for (var key in params) {
			if (uri.indexOf("?") === -1) {
				uri += "?" + key + "=" + params[key];
			} else {
				uri += "&" + key + "=" + params[key];
			}
		}
		console.debug("upload")
		// var newFile = new File([file], file.name, {type: file.type});
		// 开始上传
		// var formData = new FormData();
		// formData.append("file", file);

		let promise = new Promise(function(resolve, reject) {
			return uni.uploadFile({
				url: path + uri,
				method: 'post',
				headers: {
					"Content-Type": "multipart/form-data;charset=UTF-8",
					"TOKEN": uni.getStorageSync("XJ-TOKEN"),
				},
				meta: {
					isSerialize: false
				},
				//data: formData,
				filePath: filePath,
				name: 'file',
				success: (res) => {
					let data = JSON.parse(res.data)
					if (data.code !== 200) {
						uni.showToast({
							title: data.msg,
							icon: 'none'
						})
					}
					resolve(data);
				},
				fail: function(e) {
					console.log('请求失败', e);
				},
			})
		});
		console.log("请求结束")
		return promise;
	},


	// 下载
	download(uri, data) {
		console.debug("download => uri:", uri, "  data:", data)
		return request({
			url: baseUrl + uri,
			method: 'post',
			data: data,
			// 下载zip文件需要使用的响应格式,这是区别于普通post请求的地方,重点!!!
			responseType: "blob"
		}).then(res => {
			// 下载格式为zip { type: "application/zip" }
			let blob = new Blob([res.data], {
				type: "application/zip"
			});
			let elink = document.createElement("a"); // 创建一个<a>标签
			elink.style.display = "none"; // 隐藏标签
			elink.href = window.URL.createObjectURL(blob); // 配置href
			// 获取名称
			let filename = res.headers["content-disposition"]
			elink.download = filename.split(';')[1].split('=')[1]
			elink.click();
			URL.revokeObjectURL(elink.href); // 释放URL 对象
			document.body.removeChild(elink); // 移除<a>标签
		})
	},
}
