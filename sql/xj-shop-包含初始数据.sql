/*
 Navicat Premium Data Transfer

 Source Server         : 39.103.135.29(jie)
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-8vbkpoec225c821d8vo.mysql.zhangbei.rds.aliyuncs.com:3306
 Source Schema         : xj-shop

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 26/11/2022 20:11:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_basic
-- ----------------------------
DROP TABLE IF EXISTS `t_basic`;
CREATE TABLE `t_basic`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统通用字段表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_basic
-- ----------------------------

-- ----------------------------
-- Table structure for t_gc_datasource
-- ----------------------------
DROP TABLE IF EXISTS `t_gc_datasource`;
CREATE TABLE `t_gc_datasource`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `db_title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'db -标题',
  `db_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'db 库名',
  `db_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'db 连接地址',
  `db_username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'db 账号',
  `db_password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'db 密码',
  `author` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `describe` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述信息',
  `project_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名/路径，如：xj-server/xj-test-server',
  `pack_path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '包路径 (如: io.github.wslxm)',
  `root_module` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '根模块 (固定为：modules(管理端), 用户端为：client)',
  `modules_name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子模块 (业务分类,如用户管理,订单管理模块拆分，也可以统一一个名称放在一起)',
  `db_table_prefix` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'db 表前缀 (生成的类名会过滤掉前缀)',
  `db_field_prefix` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'db 字段前缀 (生成的字段名会过滤掉前缀)',
  `entity_swagger` tinyint(1) NULL DEFAULT NULL COMMENT '实体类是否使用swagger注释 (false情况下使用doc注释)',
  `filter_crud` tinyint(1) NULL DEFAULT NULL COMMENT '是否过滤crud方法- 默认生成 (controller/service/mapper/xml)',
  `father_path` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成路径(不填默认当前项目跟目录,可指定绝对路径)',
  `vue_field_types` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '排除vue字段类型 (字典code值，参考字典生成字段类型，如: 18=富文本 19=md编辑器 )',
  `base_fields` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库通用字段',
  `keyword_array` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库关键字',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统增强表--代码生成动态数据源' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gc_datasource
-- ----------------------------

-- ----------------------------
-- Table structure for t_gc_test
-- ----------------------------
DROP TABLE IF EXISTS `t_gc_test`;
CREATE TABLE `t_gc_test`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称 (文本)',
  `age` double(10, 2) NULL DEFAULT NULL COMMENT '年龄 (数字)',
  `sex` int(1) NULL DEFAULT NULL COMMENT '性别 (单选--字典)',
  `like` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '爱好 (多选--字典)',
  `city` int(1) NULL DEFAULT NULL COMMENT '城市 (下拉选--字典)',
  `disable` int(1) NULL DEFAULT NULL COMMENT '禁用 (开关--字典)',
  `head_url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像 (文件上传)',
  `time` datetime NULL DEFAULT NULL COMMENT '时间',
  `text` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更多信息(大文本)',
  `text_two` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '更多信息(富文本)',
  `text_three` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '更多信息(md编辑器)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成测试表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_gc_test
-- ----------------------------
INSERT INTO `t_gc_test` VALUES ('1456780575175118849', NULL, NULL, '2021-11-06 08:28:48', '2021-11-09 21:32:51', 1, 0, '兮家小二', 20.00, 3, '3,2', 2, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/32859734-timg(3).jpg', '2021-11-06 08:28:31', '啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦啦啦啦啦啦啦啊啊啦啦', NULL, NULL);
INSERT INTO `t_gc_test` VALUES ('1458065155967528961', NULL, NULL, '2021-11-09 21:33:17', '2022-01-13 22:09:16', 1, 0, '测试', 22.00, 2, '2,3', 3, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/09798637-timg(2).jpg', '2021-11-09 09:33:11', '啦啦啦啦啦啦啦', NULL, NULL);
INSERT INTO `t_gc_test` VALUES ('1475676902860955649', NULL, NULL, '2021-12-28 11:56:04', '2022-01-13 22:09:19', 1, 0, '测试数据1', 23.00, 2, '2,3', 1, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/56163330-6226ba315230576c0c8c99a6c1fbc4b7.jpeg', '2021-12-28 11:56:00', '测试数据', NULL, NULL);
INSERT INTO `t_gc_test` VALUES ('1481629780875939841', NULL, NULL, '2022-01-13 22:10:40', '2022-06-15 16:08:46', 0, 0, '代码生成测试', 221.00, 1, '2,1', 1, 1, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/01628736-qs44ufe2024qs44ufe2024.jpg', '2022-01-13 10:10:07', 'asjdasnjkasnkdnaskdkasa', NULL, NULL);
INSERT INTO `t_gc_test` VALUES ('1482723544927358977', NULL, NULL, '2022-01-16 22:36:53', '2022-01-25 14:55:24', 1, 0, '测试', 22.00, 2, '1,2', 2, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/49117698-timg.jpg', '2022-01-16 18:36:52', '测试', NULL, NULL);
INSERT INTO `t_gc_test` VALUES ('1485868996178743298', NULL, NULL, '2022-01-25 14:55:49', '2022-06-15 16:08:45', 0, 0, '111', 0.00, 1, '1', 1, 1, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/24735070-234308-16202293885efd.jpg', '2022-01-26 04:00:00', '111', '<div class=\"c-font-normal c-color-text ellipsis_efK7X\" aria-label=\"职业：演员、导演\">\n<div class=\"text_2NOr6\">职业：演员、导演</div>\n</div>\n<div class=\"c-font-normal c-color-text ellipsis_efK7X\" aria-label=\"生日：1984年6月26日\">\n<div class=\"text_2NOr6\">生日：1984年6月26日</div>\n</div>\n<div class=\"c-font-normal c-color-text ellipsis_efK7X\" aria-label=\"个人信息：172 cm/巨蟹座/O型\">\n<div class=\"text_2NOr6\">个人信息：172 cm/巨蟹座/O型</div>\n</div>\n<div class=\"c-font-normal c-color-text ellipsis_efK7X\" aria-label=\"代表作品：雪豹、西游&middot;降魔篇、裸婚时代、失恋33天、海洋天堂、小爸爸、奋斗、少帅、陆垚知马俐、剃刀边缘\">\n<div class=\"text_2NOr6\">代表作品：雪豹、西游&middot;降魔篇、裸婚时代、失恋33天、海洋天堂、小爸爸、奋斗、少帅、陆垚知马俐、剃刀边缘</div>\n</div>\n<div>\n<div class=\"c-font-normal c-color-text\" aria-label=\"文章，1984年6月26日出生于陕西省西安市雁塔区，中国内地男演员、导演，毕业于中央戏剧学院表演系。2006年，参演电视剧《与青春有关的日子》，开始在影视圈崭露头角。2005年，拍摄古装剧《锦衣卫》。2007年，主演赵宝刚导演的青春剧《奋斗》；同年，主演首部电影《走着瞧》。2008年，主演滕华涛执导的电视剧《蜗居》，饰演80后城市青年小贝。2009年，在电影《海洋天堂》中扮演自闭症患者王大福；同年，参演抗战剧《雪豹》。2011年，主演的电视剧《裸婚时代》播出；同年，连续2年获得北京大学生电影节最受大学生欢迎男演员奖。2012年，凭借电影《失恋33天》获得第31届大众电影百花奖最佳男主角奖；同年，成立北京君竹影视文化有限公司，并导演第一部影视作品《小爸爸》。2013年2月，主演的电影《西游&middot;降魔篇》在全国上映。2014年3月28日，主演的爱情片《我在路上最爱你》上映。2014年，在姜文执导的\">\n<div class=\"text_2NOr6\">简介：<em>文章</em>，1984年6月26日出生于陕西省西安市雁塔区，中...</div>\n<div class=\"text_2NOr6\">&nbsp;</div>\n<div class=\"text_2NOr6\"><img src=\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vueTinymce/50305961-mceclip0.png\" /></div>\n</div>\n</div>', NULL);
INSERT INTO `t_gc_test` VALUES ('1536984392793067521', NULL, NULL, '2022-06-15 16:10:09', '2022-06-26 11:43:38', 0, 0, 'ht', 12.00, 0, '2,1', 4, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/file/gc/53319591-1.jpg', '2022-06-15 12:09:59', '123', '<p>啦啦啦啦</p>', '啦啦');
INSERT INTO `t_gc_test` VALUES ('1540901585683599362', NULL, NULL, '2022-06-26 11:35:42', '2022-06-26 11:35:50', 0, 0, '小二', 10.00, 1, '1', 1, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/42424699-5.png', '2022-06-26 12:00:00', '啦啦啦啦啦啦啦啦啦啦', '<p><img src=\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vueTinymce/14503340-mceclip0.png\" width=\"207\" height=\"224\" /> &nbsp;</p>\n<table style=\"border-collapse: collapse; width: 100%;\" border=\"1\">\n<tbody>\n<tr>\n<td style=\"width: 25%;\">&nbsp;</td>\n<td style=\"width: 25%;\">&nbsp;</td>\n<td style=\"width: 25%;\">&nbsp;</td>\n<td style=\"width: 25%;\">&nbsp;</td>\n</tr>\n<tr>\n<td style=\"width: 25%;\">&nbsp;</td>\n<td style=\"width: 25%;\">&nbsp;</td>\n<td style=\"width: 25%;\">&nbsp;</td>\n<td style=\"width: 25%;\">&nbsp;</td>\n</tr>\n</tbody>\n</table>\n<p>啦啦啦啦啦啦啦啦</p>', '![image.png](http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vMdEditor/13388095-image.png)\n\nlla\n## 一级目录\n### 二级目录');
INSERT INTO `t_gc_test` VALUES ('1540913681519374338', NULL, NULL, '2022-06-26 12:23:46', '2022-07-27 16:14:14', 0, 0, '小五', 20.00, 1, '2', 1, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/14829444-4.png', '2022-06-23 12:00:00', '啦啦啦啦啦啦啦', '<p><img src=\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vueTinymce/14020004-mceclip1.png\" width=\"364\" height=\"158\" /></p>', '## 一级目录\n### 二级目录\n\n\n![image.png](http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vMdEditor/26206899-image.png)');
INSERT INTO `t_gc_test` VALUES ('1540915502342238210', NULL, NULL, '2022-06-26 12:31:00', '2022-07-27 16:14:12', 0, 0, '小刘', 100.00, 1, '1,2', 4, 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/43718824-5.png', '2022-06-26 20:29:52', '1\n', '<p>1</p>', '## 一级目录\n### 二级目录\n![image.png](http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vMdEditor/51645894-image.png)');

-- ----------------------------
-- Table structure for t_sys_authority
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_authority`;
CREATE TABLE `t_sys_authority`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '权限类Id(方法与类/层级关系展示)',
  `method` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求方式(GET/POST/PUT/DELETE)',
  `url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限url',
  `desc` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限备注信息',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '接口禁用(0-否 1-是)',
  `type` int(1) NOT NULL COMMENT '终端(字典code, 如 0-管理端 1-用户端 更多待定)',
  `state` int(1) NOT NULL COMMENT '授权状态(字典code  0-无需登录 1-需登录 2-需登录+授权(已废弃) 3-需Oauth2 授权 )',
  `is_sign` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否需要验签(不受限于登录授权)',
  PRIMARY KEY (`id`, `url`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--权限接口' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_authority
-- ----------------------------
INSERT INTO `t_sys_authority` VALUES ('697255086296010752', NULL, '0', '2022-06-21 03:29:37', '2022-06-21 03:29:37', 0, 347, '0', '', '/api/open/redis', 'Redis  -->  Redis 测试', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('697398103711551488', NULL, '0', '2022-06-21 04:57:58', '2022-06-21 04:57:58', 0, 345, '697255086296010752', 'GET', '/api/open/redis/redissonDistributedLockTest2/{key}', 'redis 分布式锁加锁测试2', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('697398103711551489', NULL, '0', '2022-06-21 04:57:58', '2022-06-21 04:57:58', 0, 345, '697255086296010752', 'GET', '/api/open/redis/redissonDistributedLockTest1/{key}', 'redis 分布式锁加锁测试', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('697398103711551490', NULL, '0', '2022-06-21 04:57:58', '2022-06-21 04:57:58', 0, 345, '697255086296010752', 'GET', '/api/open/redis/redissonDistributedLockTest3', 'redis 分布式注解锁测试', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('700879734145421312', NULL, '0', '2022-06-29 03:32:41', '2022-09-07 10:12:44', 0, 311, '0', '', '/api/admin/test/gcTest', '代码生成测试表', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('700879734145421314', NULL, '0', '2022-06-29 03:32:41', '2022-09-07 10:12:44', 0, 311, '700879734145421312', 'POST', '/api/admin/test/gcTest', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('700879734145421315', NULL, '0', '2022-06-29 03:32:41', '2022-09-07 10:12:44', 0, 311, '700879734145421312', 'DELETE', '/api/admin/test/gcTest/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('700879734149615616', NULL, '0', '2022-06-29 03:32:41', '2022-09-07 10:12:44', 0, 311, '700879734145421312', 'PUT', '/api/admin/test/gcTest/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('700879734149615617', NULL, '0', '2022-06-29 03:32:41', '2022-09-07 10:12:44', 0, 311, '700879734145421312', 'GET', '/api/admin/test/gcTest/{id}', 'ID查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('720779973740335104', NULL, NULL, '2022-08-21 09:29:10', '2022-08-21 09:29:10', 0, 175, '0', '', '/api/open/websocket', 'Websocket  -->  消息通知/即时通讯', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('720779973740335105', NULL, NULL, '2022-08-21 09:29:10', '2022-08-21 09:29:10', 0, 175, '720779973740335104', 'GET', '/api/open/websocket/getPath', '获取模拟游客登录的 websocket 连接地址', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('720779973740335106', NULL, NULL, '2022-08-21 09:29:10', '2022-08-21 09:29:10', 0, 175, '720779973740335104', 'POST', '/api/open/websocket/send', '发送消息', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('720779973740335107', NULL, NULL, '2022-08-21 09:29:10', '2022-08-21 09:29:10', 0, 175, '720779973740335104', 'GET', '/api/open/websocket/getOnlineUsersList', '获取当前在线用户列表', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('720779973740335108', NULL, NULL, '2022-08-21 09:29:10', '2022-08-21 09:29:10', 0, 175, '720779973740335104', 'GET', '/api/open/websocket/getOnlineCount', '获取在线人数', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('721146444106567680', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:45', 0, 153, '0', '', '/api/admin/sys/user', 'base--sys--用户管理', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444106567681', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:45', 0, 153, '721146444106567680', 'POST', '/api/admin/sys/user', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444106567682', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:45', 0, 153, '721146444106567680', 'GET', '/api/admin/sys/user/{id}', 'ID查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444106567683', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:45', 0, 153, '721146444106567680', 'PUT', '/api/admin/sys/user/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761984', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:45', 0, 153, '721146444106567680', 'DELETE', '/api/admin/sys/user/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761985', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:45', 0, 153, '721146444106567680', 'GET', '/api/admin/sys/user/findPage', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761986', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:46', 0, 153, '721146444106567680', 'PUT', '/api/admin/sys/user/{id}/resetPassword', '重置任意用户密码', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761987', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:46', 0, 153, '721146444106567680', 'PUT', '/api/admin/sys/user/updByPassword', '修改当前登录人的密码', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761988', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:46', 0, 153, '721146444106567680', 'GET', '/api/admin/sys/user/findByRoleId', '获取指定角色的用户列表', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761989', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:46', 0, 153, '721146444106567680', 'GET', '/api/admin/sys/user/findUser', '查询当前登录人的个人信息', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761990', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:46', 0, 153, '721146444106567680', 'PUT', '/api/admin/sys/user/updUser', '修改当前登录人的信息', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761991', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:46', 0, 153, '721146444106567680', 'GET', '/api/admin/sys/user/list/keyData', '查询所有-只返回关键数据(姓名/昵称/电话/id)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444110761992', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:47', 0, 153, '721146444106567680', 'POST', '/api/admin/sys/user/login', '用户登录', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956288', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:47', 0, 153, '0', '', '/api/admin/sys/dictionary', 'base--sys--字典管理', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956289', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:47', 0, 153, '721146444114956288', 'GET', '/api/admin/sys/dictionary/list', '列表查询 (默认返回Tree数据,可指定Tree或List)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956290', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:47', 0, 153, '721146444114956288', 'POST', '/api/admin/sys/dictionary', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956291', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:47', 0, 153, '721146444114956288', 'PUT', '/api/admin/sys/dictionary/{id}', '编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956292', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:47', 0, 153, '721146444114956288', 'DELETE', '/api/admin/sys/dictionary/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956293', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:48', 0, 153, '721146444114956288', 'GET', '/api/admin/sys/dictionary/findCodeGroup', '查询所有-code分组', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444114956294', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:48', 0, 153, '721146444114956288', 'GET', '/api/admin/sys/dictionary/generateEnum', '生成枚举', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444119150592', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:48', 0, 153, '721146444114956288', 'GET', '/api/admin/sys/dictionary/list/category', '获取类别(级联数据)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444119150593', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:48', 0, 153, '0', '', '/api/admin/sys/dep', 'base--sys--组织机构', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444119150594', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:48', 0, 153, '721146444119150593', 'GET', '/api/admin/sys/dep/list', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444119150595', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:48', 0, 153, '721146444119150593', 'POST', '/api/admin/sys/dep', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444119150596', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:49', 0, 153, '721146444119150593', 'PUT', '/api/admin/sys/dep/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444119150597', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:49', 0, 153, '721146444119150593', 'DELETE', '/api/admin/sys/dep/{id}', 'ID删除(并删除子数据)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444123344896', NULL, NULL, '2022-08-22 09:45:22', '2022-08-22 09:45:22', 0, 153, '0', '', '/api/client/sys/dictionary', 'yh--base--字典管理', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721146444123344897', NULL, NULL, '2022-08-22 09:45:22', '2022-09-02 14:55:29', 0, 153, '721146444123344896', 'GET', '/api/client/sys/dictionary/findByCode', 'Code查询(Tree)', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('721146444123344898', NULL, NULL, '2022-08-22 09:45:22', '2022-09-02 14:55:28', 0, 153, '721146444123344896', 'GET', '/api/client/sys/dictionary/findCodeGroup', '查询所有-code分组', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('721146444123344899', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:49', 0, 153, '0', '', '/api/admin/sys/menu', 'base--sys--菜单管理', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444123344900', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:49', 0, 153, '721146444123344899', 'GET', '/api/admin/sys/menu/list', '列表查询(不支持分页)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444123344901', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:49', 0, 153, '721146444123344899', 'POST', '/api/admin/sys/menu', '菜单添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444123344902', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:49', 0, 153, '721146444123344899', 'PUT', '/api/admin/sys/menu/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444123344903', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:50', 0, 153, '721146444123344899', 'DELETE', '/api/admin/sys/menu/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444123344904', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:50', 0, 153, '721146444123344899', 'GET', '/api/admin/sys/menu/findTree', '左导航菜单', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444131733504', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:50', 0, 153, '0', '', '/api/admin/sys/role', 'base--sys--角色管理', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444131733505', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:50', 0, 153, '721146444131733504', 'POST', '/api/admin/sys/role', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444131733506', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:50', 0, 153, '721146444131733504', 'PUT', '/api/admin/sys/role/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444131733507', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:51', 0, 153, '721146444131733504', 'DELETE', '/api/admin/sys/role/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444131733508', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:51', 0, 153, '721146444131733504', 'GET', '/api/admin/sys/role/findPage', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444135927808', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:51', 0, 153, '0', '', '/api/admin/sys/blacklist', 'base--sys--黑名单', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444135927809', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:51', 0, 153, '721146444135927808', 'POST', '/api/admin/sys/blacklist', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444135927810', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:51', 0, 153, '721146444135927808', 'PUT', '/api/admin/sys/blacklist/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444135927811', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:51', 0, 153, '721146444135927808', 'DELETE', '/api/admin/sys/blacklist/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444135927812', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:52', 0, 153, '721146444135927808', 'GET', '/api/admin/sys/blacklist/findPage', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444140122112', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:52', 0, 153, '0', '', '/api/admin/sys/authority', 'base--sys--URL权限管理', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444140122113', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:52', 0, 153, '721146444140122112', 'GET', '/api/admin/sys/authority/list', '查询所有-接口管理', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444140122114', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:52', 0, 153, '721146444140122112', 'PUT', '/api/admin/sys/authority/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444140122115', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:52', 0, 153, '721146444140122112', 'PUT', '/api/admin/sys/authority/refreshAuthority', '扫描权限', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316416', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:52', 0, 153, '0', '', '/api/admin/sys/config', 'base--sys--全局配置', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316417', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:53', 0, 153, '721146444144316416', 'POST', '/api/admin/sys/config', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316418', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:53', 0, 153, '721146444144316416', 'GET', '/api/admin/sys/config/{id}', 'ID查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316419', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:53', 0, 153, '721146444144316416', 'PUT', '/api/admin/sys/config/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316420', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:53', 0, 153, '721146444144316416', 'DELETE', '/api/admin/sys/config/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316421', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:53', 0, 153, '721146444144316416', 'GET', '/api/admin/sys/config/findPage', '分页查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721146444144316422', NULL, NULL, '2022-08-22 09:45:22', '2022-09-07 10:12:53', 0, 153, '721146444144316416', 'GET', '/api/admin/sys/config/findByCode', 'CODE查询', 0, 0, 0, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805924446208', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:53', 0, 148, '0', '', '/api/admin/gc/dataBase', 'base--gc--代码生成--查询表数据', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805928640512', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:54', 0, 148, '721151805924446208', 'GET', '/api/admin/gc/dataBase/table/list', '查询所有表名', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805928640513', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:54', 0, 148, '721151805924446208', 'GET', '/api/admin/gc/dataBase/table/field', '查询指定表下所有字段内容', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834816', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:54', 0, 148, '0', '', '/api/admin/gc/generate', 'base--gc--代码生成', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834817', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:54', 0, 148, '721151805932834816', 'GET', '/api/admin/gc/generate/getPath', '代码生成路径', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834818', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:54', 0, 148, '721151805932834816', 'POST', '/api/admin/gc/generate/generateCode', '生成代码', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834819', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:54', 0, 148, '721151805932834816', 'POST', '/api/admin/gc/generate/preview', '生成预览代码', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834820', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:55', 0, 148, '721151805932834816', 'POST', '/api/admin/gc/generate/generateCodeVue', '生成Vue代码(将直接下载)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834821', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:55', 0, 148, '721151805932834816', 'POST', '/api/admin/gc/generate/generateCodeJavaAndVue', '生成java + vue代码(将直接下载)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805932834822', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:55', 0, 148, '0', '', '/api/admin/gc/datasource', 'base--gc--代码生成--数据源维护', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029120', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:55', 0, 148, '721151805932834822', 'POST', '/api/admin/gc/datasource', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029121', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:55', 0, 148, '721151805932834822', 'GET', '/api/admin/gc/datasource/findPage', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029122', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:55', 0, 148, '721151805932834822', 'DELETE', '/api/admin/gc/datasource/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029123', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:56', 0, 148, '721151805932834822', 'PUT', '/api/admin/gc/datasource/{id}/updPwd', '修改/重置密码', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029124', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:56', 0, 148, '721151805932834822', 'GET', '/api/admin/gc/datasource/{id}', 'id查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029125', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:56', 0, 148, '721151805932834822', 'POST', '/api/admin/gc/datasource/dataSourceTest/{id}', '数据源连接测试', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721151805937029126', NULL, NULL, '2022-08-22 10:06:44', '2022-09-07 10:12:56', 0, 148, '721151805932834822', 'PUT', '/api/admin/gc/datasource/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721158911297720320', NULL, NULL, '2022-08-22 10:34:57', '2022-09-07 10:12:56', 0, 146, '0', '', '/api/admin/sys/jvm', 'base--sys--jvm信息获取', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721158911297720321', NULL, NULL, '2022-08-22 10:34:57', '2022-09-07 10:12:56', 0, 146, '721158911297720320', 'GET', '/api/admin/sys/jvm/jvmInfo', '获取系统的jvm信息', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721158911301914624', NULL, NULL, '2022-08-22 10:34:57', '2022-09-07 10:12:57', 0, 146, '0', '', '/api/admin/sys/log', 'base--sys--操作记录', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721158911301914625', NULL, NULL, '2022-08-22 10:34:57', '2022-09-07 10:12:57', 0, 146, '721158911301914624', 'GET', '/api/admin/sys/log/findPage', '分页查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721160445699952640', NULL, NULL, '2022-08-22 10:41:00', '2022-08-22 10:41:00', 0, 144, '0', '', '/api/client/sys/msg', 'yh--base-plus--消息通知', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445699952641', NULL, NULL, '2022-08-22 10:41:00', '2022-08-22 10:41:00', 0, 144, '721160445699952640', 'GET', '/api/client/sys/msg/findUnreadNum', '查询未读数量(当前登录用户)', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445699952642', NULL, NULL, '2022-08-22 10:41:00', '2022-08-22 10:41:00', 0, 144, '721160445699952640', 'PUT', '/api/client/sys/msg/{id}/read', '消息修改为已读', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445699952643', NULL, NULL, '2022-08-22 10:41:00', '2022-08-22 10:41:00', 0, 144, '721160445699952640', 'GET', '/api/client/sys/msg/findPage', '分页查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445729312768', NULL, NULL, '2022-08-22 10:41:00', '2022-08-22 10:41:00', 0, 144, '0', '', '/api/client/sys/banner', 'yh--base-plus--banner', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445729312769', NULL, NULL, '2022-08-22 10:41:00', '2022-09-03 15:28:18', 0, 144, '721160445729312768', 'GET', '/api/client/sys/banner/list/{position}', '列表-位置查询', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445733507072', NULL, NULL, '2022-08-22 10:41:00', '2022-08-22 10:41:00', 0, 144, '0', '', '/api/client/sys/config', 'yh--base-plus--全局配置', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('721160445733507073', NULL, NULL, '2022-08-22 10:41:00', '2022-09-03 16:05:17', 0, 144, '721160445733507072', 'GET', '/api/client/sys/config/findByCode', 'CODE查询', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('721160918934884352', NULL, NULL, '2022-08-22 10:42:53', '2022-09-07 10:12:57', 0, 143, '0', '', '/api/admin/sys/msg', 'base--sys--消息通知', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721160918934884353', NULL, NULL, '2022-08-22 10:42:53', '2022-09-07 10:12:57', 0, 143, '721160918934884352', 'POST', '/api/admin/sys/msg', '添加/发送消息', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721160918934884354', NULL, NULL, '2022-08-22 10:42:53', '2022-09-07 10:12:57', 0, 143, '721160918934884352', 'GET', '/api/admin/sys/msg/findPage', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721160918934884355', NULL, NULL, '2022-08-22 10:42:53', '2022-09-07 10:12:57', 0, 143, '721160918934884352', 'GET', '/api/admin/sys/msg/findUnreadNum', '查询未读数量(当前登录用户)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721160918934884356', NULL, NULL, '2022-08-22 10:42:53', '2022-09-07 10:12:58', 0, 143, '721160918934884352', 'PUT', '/api/admin/sys/msg/{id}/read', '消息修改为已读', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721160918934884357', NULL, NULL, '2022-08-22 10:42:53', '2022-09-07 10:12:58', 0, 143, '721160918934884352', 'GET', '/api/admin/sys/msg/findAllNum', '查询全部/已读/未读数量(当前登录用户)', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721162557955641344', NULL, NULL, '2022-08-22 10:49:22', '2022-09-07 10:12:58', 0, 141, '0', '', '/api/admin/sys/banner', 'base--sys--banner', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721162557955641345', NULL, NULL, '2022-08-22 10:49:22', '2022-09-07 10:12:58', 0, 141, '721162557955641344', 'POST', '/api/admin/sys/banner', '添加', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721162557955641346', NULL, NULL, '2022-08-22 10:49:22', '2022-09-07 10:12:58', 0, 141, '721162557955641344', 'GET', '/api/admin/sys/banner/findPage', '列表查询', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721162557955641347', NULL, NULL, '2022-08-22 10:49:22', '2022-09-07 10:12:59', 0, 141, '721162557955641344', 'DELETE', '/api/admin/sys/banner/{id}', 'ID删除', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('721162557955641348', NULL, NULL, '2022-08-22 10:49:22', '2022-09-07 10:12:59', 0, 141, '721162557955641344', 'PUT', '/api/admin/sys/banner/{id}', 'ID编辑', 0, 0, 1, 1);
INSERT INTO `t_sys_authority` VALUES ('726223199154606080', NULL, NULL, '2022-09-04 09:58:40', '2022-09-04 09:58:40', 0, 26, '697255086296010752', 'GET', '/api/open/redis/getOrderNo', '获取分布式唯一订单号', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('726223199158800384', NULL, NULL, '2022-09-04 09:58:40', '2022-09-04 09:58:40', 0, 26, '697255086296010752', 'GET', '/api/open/redis/getDataNo', '获取分布式唯一数据编号', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441838243840', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/admin/all/goods', '商品表', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441838243841', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441838243840', 'POST', '/api/admin/all/goods', '添加', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441838243842', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441838243840', 'GET', '/api/admin/all/goods/findPage', '列表查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441842438144', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441838243840', 'DELETE', '/api/admin/all/goods/{id}', 'ID删除', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441842438145', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441838243840', 'GET', '/api/admin/all/goods/{id}', 'ID查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441842438146', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441838243840', 'PUT', '/api/admin/all/goods/{id}', 'ID编辑', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441842438147', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441838243840', 'PUT', '/api/admin/all/goods/{id}/state', '商品上下架', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021056', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/admin/all/orderLog', '订单变化相关记录表', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021057', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441855021056', 'GET', '/api/admin/all/orderLog/findPage', '列表查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021058', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/order', '订单表', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021059', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441855021058', 'GET', '/api/client/all/order/findPage', '列表查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021060', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441855021058', 'GET', '/api/client/all/order/{id}', 'ID查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021061', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441855021058', 'PUT', '/api/client/all/order/updState/{id}', '状态变更', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021062', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:56:46', 0, 1, '756440441855021058', 'POST', '/api/client/all/order/priceCalculation', '价格计算', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021063', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441855021058', 'PUT', '/api/client/all/order/pay', '修改订单为已支付(模拟接口,实际为支付回调处理)', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441855021064', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441855021058', 'POST', '/api/client/all/order/createOrder', '创建订单,返回交易号', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441859215360', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/user', '用户表', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441859215361', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441859215360', 'POST', '/api/client/all/userregister', '注册账号', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441859215362', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441859215360', 'POST', '/api/client/all/userlogin', '登录', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441859215363', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441859215360', 'GET', '/api/client/all/userfindLoginUser', '查询当前登录人的信息', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441859215364', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441859215360', 'PUT', '/api/client/all/user/updLoginUser', '编辑当前登录人的个人信息', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441859215365', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441859215360', 'POST', '/api/client/all/usersendEmailAuthCode', '发送电子邮件验证码', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441867603968', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/collection', '我的收藏', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441867603969', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441867603968', 'PUT', '/api/client/all/collection/collect/{id}', '收藏/取消收藏', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441867603970', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441867603968', 'GET', '/api/client/all/collection/findPage', '列表查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441867603971', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/shoppingCart', '购物车', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441871798272', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441867603971', 'POST', '/api/client/all/shoppingCart', '加入购物车', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441871798273', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441867603971', 'GET', '/api/client/all/shoppingCart/findPage', '列表查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441871798274', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441867603971', 'DELETE', '/api/client/all/shoppingCart/{id}', '删除购物车商品', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441875992576', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/admin/all/user', '工具--用户表', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441875992577', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441875992576', 'POST', '/api/admin/all/user', '添加', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441875992578', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441875992576', 'GET', '/api/admin/all/user/findPage', '列表查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441875992579', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441875992576', 'DELETE', '/api/admin/all/user/{id}', 'ID删除', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441875992580', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441875992576', 'GET', '/api/admin/all/user/{id}', 'ID查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441875992581', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441875992576', 'PUT', '/api/admin/all/user/{id}', 'ID编辑', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441884381184', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '700879734145421312', 'GET', '/api/admin/test/gcTest/list', '列表查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441888575488', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/category', '类别表', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441888575489', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:55:21', 0, 1, '756440441888575488', 'GET', '/api/client/all/category/tree', '树结构数据查询', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441888575490', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:55:23', 0, 1, '756440441888575488', 'GET', '/api/client/all/category/page', '分页查询', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769792', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/admin/all/address', '地址管理(收货)', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769793', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769792', 'POST', '/api/admin/all/address', '添加', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769794', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769792', 'GET', '/api/admin/all/address/findPage', '列表查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769795', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769792', 'DELETE', '/api/admin/all/address/{id}', 'ID删除', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769796', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769792', 'GET', '/api/admin/all/address/{id}', 'ID查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769797', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769792', 'PUT', '/api/admin/all/address/{id}', 'ID编辑', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769798', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/open/aliOssFile', 'AliYun --> OSS文件管理', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769799', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769798', 'POST', '/api/open/aliOssFile/upload', 'OSS-文件上传,可在指定路径后追加子路径,以/结尾，返回完整可访问当前服务内网访问OSS的完整URL', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769800', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769798', 'GET', '/api/open/aliOssFile/download', 'OSS-文件下载--单文件下载', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769801', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769798', 'GET', '/api/open/aliOssFile/downloadZip', 'OSS-文件下载--多文件下载', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769802', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769798', 'GET', '/api/open/aliOssFile/fileList', 'OSS-文件Object列表', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441892769803', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441892769798', 'DELETE', '/api/open/aliOssFile/del', 'OSS-文件删除', 0, 2, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441896964096', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/admin/all/order', '订单表', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441896964097', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'GET', '/api/admin/all/order/findPage', '列表查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441896964098', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'DELETE', '/api/admin/all/order/{id}', 'ID删除', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441896964099', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'GET', '/api/admin/all/order/{id}', 'ID查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441896964100', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'PUT', '/api/admin/all/order/{id}', 'ID编辑(只能备注/物流单号)', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441896964101', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'PUT', '/api/admin/all/order/updState/{id}', '状态变更', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158400', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'PUT', '/api/admin/all/order/deliverGoods/{id}', '发货', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158401', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441896964096', 'PUT', '/api/admin/all/order/updTotalPrice/{id}', '改价', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158402', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/admin/all/category', '类别表', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158403', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441901158402', 'POST', '/api/admin/all/category', '添加', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158404', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441901158402', 'GET', '/api/admin/all/category/tree', '树结构数据查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158405', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441901158402', 'DELETE', '/api/admin/all/category/{id}', 'ID删除', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441901158406', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441901158402', 'GET', '/api/admin/all/category/{id}', 'ID查询', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441905352704', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441901158402', 'PUT', '/api/admin/all/category/{id}', 'ID编辑', 0, 0, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441913741312', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/goods', '商品表', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441913741313', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:53:36', 0, 1, '756440441913741312', 'GET', '/api/client/all/goods/findPage', '列表查询', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441913741314', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:53:39', 0, 1, '756440441913741312', 'GET', '/api/client/all/goods/{id}', 'ID查询', 0, 1, 0, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441913741315', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '0', '', '/api/client/all/address', '地址管理(收货)', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441913741316', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441913741315', 'POST', '/api/client/all/address', '添加', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441913741317', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441913741315', 'GET', '/api/client/all/address/findPage', '列表查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441917935616', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441913741315', 'DELETE', '/api/client/all/address/updIsDefault/{id}', '设置为默认地址', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441917935617', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441913741315', 'DELETE', '/api/client/all/address/{id}', 'ID删除', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441917935618', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441913741315', 'GET', '/api/client/all/address/{id}', 'ID查询', 0, 1, 1, 0);
INSERT INTO `t_sys_authority` VALUES ('756440441917935619', NULL, NULL, '2022-11-26 19:11:08', '2022-11-26 19:11:08', 0, 1, '756440441913741315', 'PUT', '/api/client/all/address/{id}', 'ID编辑', 0, 1, 1, 0);

-- ----------------------------
-- Table structure for t_sys_banner
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_banner`;
CREATE TABLE `t_sys_banner`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `position` int(2) NOT NULL COMMENT '位置(字典code)',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'banner标题',
  `desc` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'banner描叙',
  `img_url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'banner图片',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT 'banner排序',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT 'banner禁用(0-启用 1-禁用)',
  `is_skip` int(1) NOT NULL DEFAULT 0 COMMENT '是否跳转(0-无  1-内部链接 2-外部链接)',
  `skip_url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转地址url(地址直接添加或字典表配置)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统增强表--banner' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_banner
-- ----------------------------
INSERT INTO `t_sys_banner` VALUES ('1300260217146548226', NULL, NULL, '2020-08-31 10:32:48', '2022-09-04 17:04:50', 0, 0, 1, '测试2', '测试数据2', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/48177469-O1CN01ODVW2C1KlTcPMU2ir_!!1588911204.jpg', 1, 0, 0, '/page/logoBanner/page/logoBanner');
INSERT INTO `t_sys_banner` VALUES ('1300262684328435714', NULL, NULL, '2020-08-31 10:42:36', '2022-09-04 17:04:23', 0, 0, 1, '测试1', '测试数据一', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/21343522-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg', 0, 0, 2, 'http://www.baidu.com');
INSERT INTO `t_sys_banner` VALUES ('1309111625118248961', NULL, NULL, '2020-09-24 20:45:06', '2022-09-04 17:04:02', 0, 0, 1, '测试', '测试描叙', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00884790-O1CN010iaZKy1vznZRhnkqQ_!!1960646244.jpg', 0, 0, 0, '');
INSERT INTO `t_sys_banner` VALUES ('1459838027261095938', NULL, NULL, '2021-11-14 18:58:02', '2022-09-04 17:05:34', 0, 0, 1, '测试', '啦啦啦啦', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/29944950-O1CN01dAfzUX1gfuDG8zjlh_!!2699734170-0-lubanu-s.jpg', 0, 0, 0, '');

-- ----------------------------
-- Table structure for t_sys_blacklist
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_blacklist`;
CREATE TABLE `t_sys_blacklist`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `type` int(1) NOT NULL COMMENT '1-白名单(* 表示允许除黑名单外的所有ip请求, 否则只允许白名单中的ip请求) 2-黑名单(禁止ip访问)',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '白名单ip/黑名单ip',
  `desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '禁用(0-启用 1-禁用)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统增强表--黑名单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_blacklist
-- ----------------------------
INSERT INTO `t_sys_blacklist` VALUES ('1332333202949324802', NULL, NULL, '2020-11-27 22:39:21', '2021-11-21 11:21:10', 0, 0, 1, '*', '允许所有 ip 地址访问，优先级比黑名单(*) 高 ， 开启了白名单(*), 黑名单（*）将无效', 0);
INSERT INTO `t_sys_blacklist` VALUES ('1332337401510551554', NULL, NULL, '2020-11-27 22:56:02', '2022-07-12 23:05:17', 1, 0, 2, '*', '禁止所有 ip 访问,除本地 [127.0.0.1 / localhost] ,不建议配置在所有资源上，一旦配置，所有用户(包括自己) 将无法访问所有资源，因为每个用户的ip地址都不一样， 开启此功能需提前配置所有用户的ip地址为白名单', 0);
INSERT INTO `t_sys_blacklist` VALUES ('1421369811404894210', NULL, NULL, '2021-07-31 15:19:05', '2022-07-14 23:32:22', 0, 0, 2, '192.168.1.10', '本地', 0);
INSERT INTO `t_sys_blacklist` VALUES ('1462259668311121921', NULL, NULL, '2021-11-21 11:20:47', '2021-12-09 13:48:18', 1, 0, 1, '1', '1', 0);

-- ----------------------------
-- Table structure for t_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_config`;
CREATE TABLE `t_sys_config`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置code|搜索值(不能重复)',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置名称',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置内容',
  `sort` int(11) NOT NULL COMMENT '排序',
  `type` int(1) NOT NULL DEFAULT 0 COMMENT '类型(0-文本 1-图片 2-开关 3-富文本)',
  `desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `ext1` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统全局配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_config
-- ----------------------------
INSERT INTO `t_sys_config` VALUES ('1365174805250260994', NULL, '-1', '2021-02-26 13:40:11', '2022-08-25 14:20:19', 0, 0, 'entry_name', '项目名称', '兮家商城DEMO', 0, 0, '登录页和左菜单顶部标题', NULL, NULL, NULL);
INSERT INTO `t_sys_config` VALUES ('1365182627308433409', NULL, '-1', '2021-02-26 14:11:17', '2022-06-04 15:30:01', 0, 0, 'login_bg_img', '背景图(登录页)', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/config/20210311113615990505-1.jpg', 0, 1, '未启用', NULL, NULL, NULL);
INSERT INTO `t_sys_config` VALUES ('1383627414470467586', NULL, '-1', '2021-04-18 11:44:16', '2022-06-04 10:44:12', 0, 0, 'is_sign', '验签开关', 'true', 0, 2, '验签总开关 |  true  需验签(默认)   false=无需验签, 开启后可在接口管理中对单个接口进行配置', NULL, NULL, NULL);
INSERT INTO `t_sys_config` VALUES ('1383636872395255809', NULL, '-1', '2021-04-18 12:21:51', '2022-06-04 10:46:12', 0, 0, 'is_swagger', 'swagger文档开关', 'true', 0, 2, '动态开关是否可在线查看接口文档，关闭后所有接口将隐藏展示', NULL, NULL, NULL);
INSERT INTO `t_sys_config` VALUES ('1432597381643304961', NULL, '-1', '2021-08-31 14:53:26', '2022-08-21 10:50:56', 0, 0, 'is_auth', '接口是否验权 (已废弃)', 'true', 0, 2, '接口权限总开关, 接口权限管理，开启后在接口管理中可单独配置，和登录人当前拥有角色是否分配指定接口权限相关', NULL, NULL, NULL);
INSERT INTO `t_sys_config` VALUES ('1441701074921598977', NULL, '-1', '2021-09-25 17:48:16', '2022-08-20 14:17:41', 0, 0, 'login_expiration_manage', '登录有效期', '60', 0, 0, '登录状态切对当前系统无如何操作后，当前登录状态保持时长,  防止离开后被别人操作， 单位分 （配置内容针对的是管理端）', NULL, NULL, NULL);
INSERT INTO `t_sys_config` VALUES ('1564234813681000449', NULL, NULL, '2022-08-29 20:53:33', '2022-08-29 20:53:33', 0, 0, 'tool_about', '关于', '<p>富文本内容</p>\n<p><img src=\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/vueTinymce/32118898-mceclip0.png\" /></p>', 0, 3, '', '', '', '');

-- ----------------------------
-- Table structure for t_sys_dep
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dep`;
CREATE TABLE `t_sys_dep`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '父Id (顶级父id=0)',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门编码 (开始查询使用,不可重复)',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门/公司名称',
  `desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门/公司描叙',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '禁用(0-否 1-是)',
  `root` int(1) NULL DEFAULT 1 COMMENT '级别( 1-一级 2-二级 3-三级)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--组织机构' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_dep
-- ----------------------------
INSERT INTO `t_sys_dep` VALUES ('1443502302433439746', NULL, '-1', '2021-09-30 17:05:42', '2021-10-08 16:24:00', 0, 0, '1443502090977603585', 'csb', '测试部', '-', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1443502428644241409', NULL, '-1', '2021-09-30 17:06:12', '2021-11-29 19:50:02', 0, 0, '1443502157943861250', 'yyb', '运营部', '-', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1481913168983756802', NULL, '-1', '2022-01-14 16:56:46', '2022-05-15 10:45:21', 0, 0, '1481913127925714945', 'xx-dep', 'xx部门', '-', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1481913213086863362', NULL, '-1', '2022-01-14 16:56:57', '2022-05-15 10:45:21', 0, 0, '1481913127925714945', 'xx-dep2', 'xx部门2', '-', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1548901388543594498', NULL, NULL, '2022-07-18 13:24:02', '2022-07-26 14:41:00', 0, 0, '0', 'TEST01', '测试公司1', '测试公司1的描述', 0, 0, 1);
INSERT INTO `t_sys_dep` VALUES ('1548901468621246466', NULL, NULL, '2022-07-18 13:24:22', '2022-07-18 13:25:13', 0, 0, '1548901388543594498', '1', '部门1', '部门1描述', 3, 0, 2);
INSERT INTO `t_sys_dep` VALUES ('1548901576150618114', NULL, NULL, '2022-07-18 13:24:47', '2022-07-18 13:24:47', 0, 0, '1548901468621246466', '2', '部门1_1', '部门1_1desc', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1548901827913715713', NULL, NULL, '2022-07-18 13:25:47', '2022-07-18 13:25:47', 0, 0, '1548901388543594498', '11', '部门2', '部门2desc', 0, 0, 2);
INSERT INTO `t_sys_dep` VALUES ('1548901950412558337', NULL, NULL, '2022-07-18 13:26:16', '2022-07-18 13:26:16', 0, 0, '1548901827913715713', '11', '部门2_1', '部门2_1', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1549253663384408066', NULL, NULL, '2022-07-19 12:43:51', '2022-07-19 12:43:51', 0, 0, '1548901827913715713', '12', '11', '-', 0, 0, 3);
INSERT INTO `t_sys_dep` VALUES ('1560880103058071554', NULL, NULL, '2022-08-20 14:43:09', '2022-08-20 14:43:09', 0, 0, '1548901468621246466', '2', '2', '-', 0, 0, 3);

-- ----------------------------
-- Table structure for t_sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dictionary`;
CREATE TABLE `t_sys_dictionary`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '父Id',
  `desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描叙',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '禁用(0-否 1-是)',
  `ext1` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--字典' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_dictionary
-- ----------------------------
INSERT INTO `t_sys_dictionary` VALUES ('1290684671448936449', NULL, '-1', '2020-08-05 00:23:00', '2021-12-02 09:01:04', 0, 0, 'ENUMS', '枚举字典', '0', '状态/动态字段值，如：state，type，gender等, 可直接生成 前/ 后端枚举对象类代码', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290686507555844098', NULL, '-1', '2020-08-05 00:30:17', '2022-06-26 10:54:17', 0, 0, 'ADMIN', '系统枚举(动态值)', '1290684671448936449', '-', 3, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290687277911076865', NULL, '-1', '2020-08-05 00:33:21', '2022-05-15 10:39:20', 0, 0, 'MENU_ROOT', '菜单级别', '1290688121255587841', '【固定值】', 1000, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290687351005212673', NULL, '-1', '2020-08-05 00:33:38', '2021-11-10 15:33:28', 0, 0, '1', '顶部菜单', '1290687277911076865', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290687461252493314', NULL, '-1', '2020-08-05 00:34:05', '2021-11-10 15:33:28', 0, 0, '2', '菜单', '1290687277911076865', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290687547940368386', NULL, '-1', '2020-08-05 00:34:25', '2021-11-10 15:33:28', 0, 0, '3', '页面', '1290687277911076865', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290688121255587841', NULL, '-1', '2020-08-05 00:36:42', '2022-05-15 10:38:15', 0, 0, 'BASE', '系统枚举(固定值)', '1290684671448936449', '-', 2, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290688660164931586', NULL, '-1', '2020-08-05 00:38:51', '2022-05-15 10:39:14', 0, 0, 'GENDER', '性别', '1290688121255587841', '【固定值】', 700, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290688703043301378', NULL, '-1', '2020-08-05 00:39:01', '2021-11-10 15:33:28', 0, 0, '1', '男', '1290688660164931586', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1290688742289403906', NULL, '-1', '2020-08-05 00:39:10', '2022-03-05 17:59:57', 0, 0, '2', '女', '1290688660164931586', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1291341478399897601', NULL, '-1', '2020-08-06 11:52:54', '2021-11-10 15:33:29', 0, 0, '0', '未知', '1290688660164931586', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296469448399593474', NULL, '-1', '2020-08-20 15:29:38', '2022-05-15 10:39:12', 0, 0, 'DISABLE', '是否禁用', '1290688121255587841', '【固定值】', 600, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296469518025039873', NULL, '-1', '2020-08-20 15:29:55', '2021-12-09 14:31:33', 0, 0, '1', '禁用', '1296469448399593474', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296469564455985154', NULL, '-1', '2020-08-20 15:30:06', '2021-11-10 15:33:29', 0, 0, '0', '启用', '1296469448399593474', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296995475064434690', NULL, '-1', '2020-08-22 02:19:52', '2022-05-15 10:39:19', 0, 0, 'AUTHORITY_TYPE', '权限类型', '1290688121255587841', '【固定值】：用于新接口自动生成【权限状态】', 900, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296995560007479298', NULL, '-1', '2020-08-22 02:20:12', '2021-11-10 15:33:29', 0, 0, '0', '管理端接口', '1296995475064434690', '管理端, 类上标有该参数所有接口都会默认被列为-[需登录+需授权]', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296995610632728578', NULL, '-1', '2020-08-22 02:20:24', '2021-11-10 15:33:29', 0, 0, '1', '用户端接口', '1296995475064434690', '用户端, 类上标有该参数所有接口都会默认被列为-[需登录]', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296995742778470401', NULL, '-1', '2020-08-22 02:20:55', '2022-05-15 10:39:16', 0, 0, 'AUTHORITY_STATE', '权限状态', '1290688121255587841', '【固定值】：用于编辑指定接口的【权限状态】', 800, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296995839977271297', NULL, '-1', '2020-08-22 02:21:19', '2021-11-10 15:33:29', 0, 0, '0', '无', '1296995742778470401', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296996062090833922', NULL, '-1', '2020-08-22 02:22:12', '2021-11-10 15:33:29', 0, 0, '1', '需登录', '1296995742778470401', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1296996224863383554', NULL, '-1', '2020-08-22 02:22:50', '2022-08-20 08:38:06', 0, 0, '2', '需登录+授权', '1296995742778470401', '-已移除角色关联url, 菜单即权限', 0, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1297705363906273282', NULL, '-1', '2020-08-24 01:20:43', '2021-11-10 15:33:30', 0, 0, '2', '通用接口', '1296995475064434690', '通用接口, 类上标有该参数所有接口都会默认被列为-[无需登录,无需授权]', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298191840981327873', NULL, '-1', '2020-08-25 09:33:48', '2022-05-15 10:39:11', 0, 0, 'DELETED', '逻辑删除', '1290688121255587841', '【固定值】', 500, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298191898313269249', NULL, '-1', '2020-08-25 09:34:02', '2021-11-10 15:33:30', 0, 0, '0', '正常', '1298191840981327873', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298191981998022658', NULL, '-1', '2020-08-25 09:34:22', '2021-11-10 15:33:30', 0, 0, '1', '已删除', '1298191840981327873', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298194668697198594', NULL, '-1', '2020-08-25 09:45:02', '2022-05-15 10:39:22', 0, 0, 'BANNER_IS_SKIP', 'banner是否跳转', '1290688121255587841', '【固定值】', 1100, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298194722350735361', NULL, '-1', '2020-08-25 09:45:15', '2021-11-10 15:33:30', 0, 0, '0', '否', '1298194668697198594', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298194801014906881', NULL, '-1', '2020-08-25 09:45:34', '2021-11-10 15:33:30', 0, 0, '1', '内部跳转', '1298194668697198594', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1298194850285395969', NULL, '-1', '2020-08-25 09:45:46', '2021-11-10 15:33:30', 0, 0, '2', '外部跳转', '1298194668697198594', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1303872194494435330', NULL, '-1', '2020-09-10 09:45:30', '2021-11-10 15:33:30', 0, 0, 'BANNER_POSITION', 'banner 位置', '1290686507555844098', '【动态值】', 300, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1303872308608864257', NULL, '-1', '2020-09-10 09:45:57', '2021-11-10 15:33:30', 0, 0, '1', '用户端首页', '1303872194494435330', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1308580167513370625', NULL, '-1', '2020-09-23 09:33:17', '2021-11-10 15:33:30', 0, 0, 'MSG_USER_TYPE', '及时消息终端', '1290686507555844098', '【动态值】', 500, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1308580236161544193', NULL, '-1', '2020-09-23 09:33:33', '2021-11-10 15:33:30', 0, 0, '1', '用户端', '1308580167513370625', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1308580275248263169', NULL, '-1', '2020-09-23 09:33:42', '2021-11-10 15:33:31', 0, 0, '2', '管理端', '1308580167513370625', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1308585499920769025', NULL, '-1', '2020-09-23 09:54:28', '2021-11-10 15:33:31', 0, 0, 'MSG_TYPE', '及时消息类型', '1290686507555844098', '【动态值】', 400, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1308585615968772098', NULL, '-1', '2020-09-23 09:54:56', '2022-06-22 10:33:35', 0, 0, '1', '管理端 - 系统通知', '1308585499920769025', '-', 1, 0, '系统通知', NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1332329973427494913', NULL, '-1', '2020-11-27 22:26:31', '2022-05-15 10:39:26', 0, 0, 'BLACKLIST_TYPE', '黑/白名单类型', '1290688121255587841', '【固定值】', 1300, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1332330105569042434', NULL, '-1', '2020-11-27 22:27:02', '2021-11-10 15:33:31', 0, 0, '1', '白名单', '1332329973427494913', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1332330148963311619', NULL, '-1', '2020-11-27 22:27:13', '2021-11-10 15:33:31', 0, 0, '2', '黑名单', '1332329973427494913', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352826209829978114', NULL, '-1', '2021-01-23 11:51:17', '2021-11-10 15:33:31', 0, 0, '3', 'Oauth2 接口', '1296995475064434690', '接口默认默认需通过 appId，appSecret生成的 accessToken 来进行oauth2 令牌验证可访问', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352826389480407041', NULL, '-1', '2021-01-23 11:52:00', '2021-11-10 15:33:31', 0, 0, '3', '需Oauth2 授权', '1296995742778470401', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856400451117058', NULL, '-1', '2021-01-23 13:51:15', '2022-05-15 10:39:33', 0, 0, 'PAY_TYPE', '支付类型', '1290688121255587841', '【固定值】', 1600, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856492264431617', NULL, '-1', '2021-01-23 13:51:37', '2021-11-10 15:33:31', 0, 0, '1', '支付', '1352856400451117058', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856528012484610', NULL, '-1', '2021-01-23 13:51:45', '2021-11-10 15:33:31', 0, 0, '2', '充值', '1352856400451117058', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856603073748994', NULL, '-1', '2021-01-23 13:52:03', '2021-11-10 15:33:31', 0, 0, '3', '退款', '1352856400451117058', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856663249428482', NULL, '-1', '2021-01-23 13:52:18', '2021-11-10 15:33:32', 0, 0, '4', '商家打款', '1352856400451117058', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856892170346498', NULL, '-1', '2021-01-23 13:53:12', '2022-05-15 10:39:28', 0, 0, 'PAY_CHANNEL', '支付渠道', '1290688121255587841', '【固定值】', 1400, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856932938981378', NULL, '-1', '2021-01-23 13:53:22', '2021-11-10 15:33:32', 0, 0, '1', '支付宝', '1352856892170346498', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352856979743219713', NULL, '-1', '2021-01-23 13:53:33', '2021-11-10 15:33:32', 0, 0, '2', '微信', '1352856892170346498', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857025708597250', NULL, '-1', '2021-01-23 13:53:44', '2021-11-10 15:33:32', 0, 0, '3', '银联', '1352856892170346498', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857215228223489', NULL, '-1', '2021-01-23 13:54:29', '2022-05-15 10:39:30', 0, 0, 'PAY_STATE', '支付状态', '1290688121255587841', '【固定值】用于记录支付交易请求状态', 1500, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857264104448001', NULL, '-1', '2021-01-23 13:54:41', '2021-11-10 15:33:32', 0, 0, '0', '已发起', '1352857215228223489', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857305888104450', NULL, '-1', '2021-01-23 13:54:51', '2021-11-10 15:33:32', 0, 0, '1', '回调成功', '1352857215228223489', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857349592752130', NULL, '-1', '2021-01-23 13:55:01', '2021-11-10 15:33:32', 0, 0, '2', '交易失败', '1352857215228223489', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857389069541377', NULL, '-1', '2021-01-23 13:55:11', '2021-11-10 15:33:32', 0, 0, '3', '交易成功', '1352857215228223489', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857426407235585', NULL, '-1', '2021-01-23 13:55:20', '2021-11-10 15:33:32', 0, 0, '4', '订单异常', '1352857215228223489', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857793505304577', NULL, '-1', '2021-01-23 13:56:47', '2021-11-10 15:37:17', 0, 0, 'PAY_BUSINESS', '支付业务', '1290686507555844098', '【动态值】当前支付业务', 600, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352857906709569537', NULL, '-1', '2021-01-23 13:57:14', '2021-11-10 15:37:17', 0, 0, '1', '用户下单', '1352857793505304577', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1352858096959004674', NULL, '-1', '2021-01-23 13:57:59', '2021-11-10 15:37:18', 0, 0, '2', 'vip 充值/续费', '1352857793505304577', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1357528050694148097', NULL, '-1', '2021-02-05 11:14:43', '2022-05-15 10:39:35', 0, 0, 'WALLET_TYPE', '流水类型', '1290688121255587841', '【固定值】', 1700, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1357528121372364801', NULL, '-1', '2021-02-05 11:15:00', '2021-11-10 15:33:33', 0, 0, '1', '收入', '1357528050694148097', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1357528154167627779', NULL, '-1', '2021-02-05 11:15:07', '2021-11-10 15:33:33', 0, 0, '2', '支出', '1357528050694148097', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1357612114939858945', NULL, '-1', '2021-02-05 16:48:45', '2022-05-15 10:39:23', 0, 0, 'IS_READ', '是否已读', '1290688121255587841', '【固定值】', 1200, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1357612150536916994', NULL, '-1', '2021-02-05 16:48:54', '2021-11-10 15:33:33', 0, 0, '0', '未读', '1357612114939858945', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1357612182854029315', NULL, '-1', '2021-02-05 16:49:01', '2021-11-10 15:33:33', 0, 0, '1', '已读', '1357612114939858945', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1368739295631798273', NULL, '-1', '2021-03-08 09:44:11', '2021-11-10 15:33:33', 0, 0, 'POSITION', '部门职位', '1290686507555844098', '【动态值】, 如有需要根据业务指定', 200, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1368739394596401154', NULL, '-1', '2021-03-08 09:44:35', '2022-06-22 23:37:04', 0, 0, '0', '系统管理员(老板)', '1368739295631798273', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1384697257961463810', NULL, '-1', '2021-04-21 10:35:27', '2022-01-13 22:19:46', 0, 0, '1', '部门经理', '1368739295631798273', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1399968409441050625', NULL, '-1', '2021-06-02 13:57:32', '2022-05-15 10:39:09', 0, 0, 'DEFAULT', '默认字典(代码生成默认字典)', '1290688121255587841', '【固定值】用于代码生成默认使用的code值', 400, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1399968449656037377', NULL, '-1', '2021-06-02 13:57:42', '2021-11-10 15:33:34', 0, 0, '1', '默认值 1', '1399968409441050625', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1399968504043577346', NULL, '-1', '2021-06-02 13:57:55', '2021-11-10 15:33:34', 0, 0, '2', '默认值 2', '1399968409441050625', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1399968544350838786', NULL, '-1', '2021-06-02 13:58:04', '2022-06-04 10:29:20', 0, 0, '3', '默认值 3', '1399968409441050625', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1404005220177985537', NULL, '-1', '2021-06-13 17:18:21', '2022-06-22 10:33:19', 0, 0, '0', '管理端 - 测试消息', '1308585499920769025', '-', 0, 0, '测试消息', NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1427513445955194882', NULL, '-1', '2021-08-17 14:11:42', '2021-11-10 15:33:34', 0, 0, '4', '其他', '1352856892170346498', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1427513925234118658', NULL, '-1', '2021-08-17 14:13:36', '2021-11-10 15:37:18', 0, 0, '3', '月卡购买', '1352857793505304577', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1427513998571524097', NULL, '-1', '2021-08-17 14:13:53', '2021-11-10 15:37:18', 0, 0, '4', '其他', '1352857793505304577', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1432260997380349954', NULL, '-1', '2021-08-30 16:36:49', '2021-11-10 15:33:34', 0, 0, 'TERMINAL', '终端', '1290686507555844098', '【动态值】 如有需要根据业务指定', 100, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1432261183641001986', NULL, '-1', '2021-08-30 16:37:33', '2022-01-17 14:33:51', 0, 0, '1', 'vue 主系统端', '1432260997380349954', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1432261342928084993', NULL, '-1', '2021-08-30 16:38:11', '2022-01-18 20:11:54', 0, 0, '2', '商家端', '1432260997380349954', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1443767289248788481', NULL, '-1', '2021-10-01 10:38:39', '2022-05-15 10:39:07', 0, 0, 'ORGAN_ROOT', '机构组织级别', '1290688121255587841', '-', 200, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1443767335407104002', NULL, '-1', '2021-10-01 10:38:50', '2021-11-10 15:33:35', 0, 0, '1', '一级', '1443767289248788481', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1443767375802445826', NULL, '-1', '2021-10-01 10:39:00', '2021-11-10 15:33:35', 0, 0, '2', '二级', '1443767289248788481', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1443767410984267777', NULL, '-1', '2021-10-01 10:39:08', '2021-11-10 15:33:35', 0, 0, '3', '三级', '1443767289248788481', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455153732051349505', NULL, '-1', '2021-11-01 20:44:19', '2022-05-15 10:43:12', 0, 0, 'VUE_FIELD_TYPE', 'VUE字段类型', '1290688121255587841', 'vue代码生成使用', 300, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455153814570086402', NULL, '-1', '2021-11-01 20:44:39', '2022-06-30 10:12:07', 0, 0, '1', '文本-(input)', '1455153732051349505', '-', 1, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455153864008347650', NULL, '-1', '2021-11-01 20:44:51', '2022-05-15 10:43:13', 0, 0, '2', '数字-(number)', '1455153732051349505', '-', 2, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455154454599905281', NULL, '-1', '2021-11-01 20:47:12', '2022-05-15 10:43:13', 0, 0, '4', '单选-(radio)', '1455153732051349505', '-', 4, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455154827834241025', NULL, '-1', '2021-11-01 20:48:41', '2022-05-15 10:43:13', 0, 0, '9', '开关-(switch)', '1455153732051349505', '-', 9, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455154939453059073', NULL, '-1', '2021-11-01 20:49:07', '2022-05-15 10:43:13', 0, 0, '5', '多选-(checkbox)', '1455153732051349505', '-', 5, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455155390269435905', NULL, '-1', '2021-11-01 20:50:55', '2022-05-15 10:43:13', 0, 0, '10', '日期-(data)', '1455153732051349505', 'yyyy-MM-dd', 10, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455155744738455553', NULL, '-1', '2021-11-01 20:52:19', '2022-05-15 10:43:14', 0, 0, '11', '日期时间-(datetime)', '1455153732051349505', 'yyyy-MM-dd hh:mm:ss', 11, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455155995658498049', NULL, '-1', '2021-11-01 20:53:19', '2022-05-15 10:43:14', 0, 0, '12', '时间-(time)', '1455153732051349505', '-', 12, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455156254728073217', NULL, '-1', '2021-11-01 20:54:21', '2022-05-15 10:43:14', 0, 0, '6', '下拉选择-(select-单选)', '1455153732051349505', '-', 6, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455156611625594881', NULL, '-1', '2021-11-01 20:55:46', '2022-05-15 10:43:14', 0, 0, '3', '密码-(password)', '1455153732051349505', '-', 3, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455157509512835073', NULL, '-1', '2021-11-01 20:59:20', '2022-05-15 10:43:14', 0, 0, '7', '下拉选择 (select-单选+搜索)', '1455153732051349505', '-', 7, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455157594946613250', NULL, '-1', '2021-11-01 20:59:40', '2022-05-15 10:43:15', 0, 0, '8', '下拉选择 (select-多选+搜索)', '1455153732051349505', '-', 8, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455158056173252609', NULL, '-1', '2021-11-01 21:01:30', '2022-05-15 10:43:15', 0, 0, '13', '文件上传(默认单图)', '1455153732051349505', '-', 13, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455158128055234561', NULL, '-1', '2021-11-01 21:01:47', '2022-05-15 10:43:15', 0, 0, '14', '文件上传(多图)', '1455153732051349505', '-', 14, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455158254165372929', NULL, '-1', '2021-11-01 21:02:18', '2022-05-15 10:43:15', 0, 0, '15', '文件上传（缩略图）', '1455153732051349505', '-', 15, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1455160116759310338', NULL, '-1', '2021-11-01 21:09:42', '2022-05-15 10:43:16', 0, 0, '16', '文件上传（附件）', '1455153732051349505', '-', 16, 1, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1456761651003920386', NULL, '-1', '2021-11-06 07:13:37', '2022-05-15 10:43:16', 0, 0, '17', '大文本(textarea)', '1455153732051349505', '-', 17, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1459853262768320513', NULL, '-1', '2021-11-14 19:58:34', '2022-05-15 10:43:16', 0, 0, 'CONFIG_TYPE', '全局配置类型', '1290688121255587841', '-', 100, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1459853371690201089', NULL, '-1', '2021-11-14 19:59:00', '2022-05-15 10:43:16', 0, 0, '0', '文本', '1459853262768320513', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1459853401289404418', NULL, '-1', '2021-11-14 19:59:07', '2022-06-17 16:31:40', 0, 0, '1', '图片', '1459853262768320513', '-', 1, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1459853475214012418', NULL, '-1', '2021-11-14 19:59:25', '2022-06-17 16:31:41', 0, 0, '2', '开关', '1459853262768320513', '-', 2, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1477131575404531714', NULL, '-1', '2022-01-01 12:16:26', '2022-08-24 09:59:24', 0, 0, 'SHOP', '商城模块', '1290684671448936449', '-', 1, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1477131743994580994', NULL, '-1', '2022-01-01 12:17:06', '2022-08-24 10:07:06', 0, 0, 'ORDER_STATE', '订单状态', '1477131575404531714', '-', 100, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1477131857387589634', NULL, '-1', '2022-01-01 12:17:33', '2022-09-02 16:41:42', 0, 0, '1', '待支付', '1477131743994580994', '-', 1, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1477131918590873601', NULL, '-1', '2022-01-01 12:17:47', '2022-08-24 10:28:42', 0, 0, '2', '已支付', '1477131743994580994', '-', 2, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1477131953227436034', NULL, '-1', '2022-01-01 12:17:56', '2022-08-24 10:28:46', 0, 0, '3', '已发货', '1477131743994580994', '-', 3, 0, '1', '2', '3');
INSERT INTO `t_sys_dictionary` VALUES ('1481632150259240961', NULL, '-1', '2022-01-13 22:20:04', '2022-05-15 10:43:18', 0, 0, '2', '员工', '1368739295631798273', '-', 0, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1524590015236018178', NULL, '-1', '2022-05-12 11:19:19', '2022-06-17 16:31:41', 0, 0, '3', '富文本', '1459853262768320513', '-', 3, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1525481792788762625', NULL, '-1', '2022-05-14 22:22:56', '2022-05-18 21:20:41', 0, 0, '18', '富文本(tinymce)', '1455153732051349505', 'vue-tinymce 富文本插件', 18, 0, NULL, NULL, NULL);
INSERT INTO `t_sys_dictionary` VALUES ('1537714554153410561', NULL, NULL, '2022-06-17 16:31:31', '2022-06-17 16:31:31', 0, 0, '4', 'markdown 文本', '1459853262768320513', '-', 4, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1539432646667579394', NULL, NULL, '2022-06-22 10:18:38', '2022-06-22 11:04:12', 0, 0, '2', '管理端 - 用户信息变动', '1308585499920769025', '-已配置路由', 2, 0, '用户信息变动', '/views/admin/user/user', '/app/user/details');
INSERT INTO `t_sys_dictionary` VALUES ('1540889700338765825', NULL, NULL, '2022-06-26 10:48:27', '2022-06-30 10:13:47', 0, 0, '19', 'md 编辑器', '1455153732051349505', '-', 19, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562265557593550850', NULL, NULL, '2022-08-24 10:28:28', '2022-08-24 10:28:33', 0, 0, '4', '已完成', '1477131743994580994', '-', 4, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562265692159406082', NULL, NULL, '2022-08-24 10:29:00', '2022-08-24 10:29:00', 0, 0, '5', '用户取消', '1477131743994580994', '-', 5, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562265731158044674', NULL, NULL, '2022-08-24 10:29:10', '2022-08-24 10:29:10', 0, 0, '6', '平台拒单', '1477131743994580994', '-', 6, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562265768982278146', NULL, NULL, '2022-08-24 10:29:19', '2022-08-24 10:29:19', 0, 0, '7', '支付失败', '1477131743994580994', '-', 7, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562265937194840066', NULL, NULL, '2022-08-24 10:29:59', '2022-08-24 10:29:59', 0, 0, '8', '订单超时', '1477131743994580994', '15分钟时间不支付自动超时', 8, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562266309766475778', NULL, NULL, '2022-08-24 10:31:28', '2022-08-24 10:32:17', 0, 0, 'OPERATION_TYPE', '订单流程', '1477131575404531714', '-', 0, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562266623852736513', NULL, NULL, '2022-08-24 10:32:42', '2022-08-24 10:32:42', 0, 0, '1', '用户下单', '1562266309766475778', '-', 1, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562266725677854721', NULL, NULL, '2022-08-24 10:33:07', '2022-08-24 10:33:07', 0, 0, '2', '用户支付', '1562266309766475778', '-', 2, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562266888572039169', NULL, NULL, '2022-08-24 10:33:46', '2022-08-24 10:40:14', 0, 0, '4', '平台发货', '1562266309766475778', '-', 4, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562266924659830785', NULL, NULL, '2022-08-24 10:33:54', '2022-08-24 10:40:19', 0, 0, '5', '平台拒单', '1562266309766475778', '-', 5, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562266953336287233', NULL, NULL, '2022-08-24 10:34:01', '2022-08-24 10:40:27', 0, 0, '6', '用户收货', '1562266309766475778', '-', 6, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562267014778646529', NULL, NULL, '2022-08-24 10:34:16', '2022-08-24 10:40:33', 0, 0, '7', '支付失败', '1562266309766475778', '-支付回调，检查到支付失败触发', 7, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562267046445641729', NULL, NULL, '2022-08-24 10:34:23', '2022-08-24 10:40:04', 0, 0, '3', '用户取消', '1562266309766475778', '-', 3, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562267795036631042', NULL, NULL, '2022-08-24 10:37:22', '2022-08-24 10:40:38', 0, 0, '8', '订单自动超时', '1562266309766475778', '-订单创建后超过15分钟自动超时', 8, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562268238395535361', NULL, NULL, '2022-08-24 10:39:07', '2022-08-24 10:40:43', 0, 0, '9', '平台改价', '1562266309766475778', '-用户下单未支付情况下改价', 9, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562269020268326914', NULL, NULL, '2022-08-24 10:42:14', '2022-08-24 10:42:14', 0, 0, 'IS_MAIL', '是否包邮', '1477131575404531714', '-', 0, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562269092980781058', NULL, NULL, '2022-08-24 10:42:31', '2022-08-24 10:43:00', 0, 0, '1', '包邮', '1562269020268326914', '-', 0, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562269127629926402', NULL, NULL, '2022-08-24 10:42:39', '2022-08-24 10:42:48', 0, 0, '0', '不包邮', '1562269020268326914', '-', 1, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562461990625009665', NULL, NULL, '2022-08-24 23:29:01', '2022-08-24 23:29:01', 0, 0, 'GOODS_STATE', '商品状态', '1477131575404531714', '-', 0, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562462067070394370', NULL, NULL, '2022-08-24 23:29:20', '2022-08-24 23:29:20', 0, 0, '0', '下架中', '1562461990625009665', '-', 0, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1562462105024651265', NULL, NULL, '2022-08-24 23:29:29', '2022-08-24 23:29:29', 0, 0, '1', '已上架', '1562461990625009665', '-', 1, 0, '', '', '');
INSERT INTO `t_sys_dictionary` VALUES ('1566369946911498241', NULL, NULL, '2022-09-04 18:17:51', '2022-09-04 18:37:22', 0, 0, '3', '订单下单通知', '1308585499920769025', '-', 3, 0, '收到新的订单', '/views-shop/all/order/order', '');

-- ----------------------------
-- Table structure for t_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_log`;
CREATE TABLE `t_sys_log`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `full_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求人',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求人Id (根据端来区分)',
  `type` int(1) NULL DEFAULT NULL COMMENT '请求终端(字典code, 如 0-管理端 1-用户端 -1-其他)',
  `referer` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求来源',
  `url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求url',
  `uri` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求uri',
  `ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户真实Ip',
  `host` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户主机名',
  `method` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式(post-get)',
  `server_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务器地址',
  `port` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务器端口',
  `package_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求包',
  `class_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求类',
  `class_desc` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求类--swagger注释',
  `method_desc` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法--swagger注释',
  `request_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求数据',
  `response_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回数据',
  `state` int(1) NULL DEFAULT 0 COMMENT '1-请求成功 0-请求失败',
  `execute_time` bigint(20) NULL DEFAULT NULL COMMENT '程序响应总耗时',
  `business_time` bigint(20) NULL DEFAULT NULL COMMENT '业务执行总耗时',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统增强表--请求记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_log
-- ----------------------------
INSERT INTO `t_sys_log` VALUES ('1566404662083563522', NULL, NULL, '2022-09-04 20:35:48', '2022-09-04 20:35:48', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '49685', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1},{\"goodsSpecsId\":\"1566343777826635777\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406755897345\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"totalPrice\":49.00},{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/50018462-O1CN01ODVW2C1KlTcPMU2ir_!!1588911204.jpg\",\"goodsSpecsId\":\"1566343777826635777\",\"name\":\"安妮的包裹原创正版lolita 蔷薇op复古优雅cla系短袖连衣裙现货夏\",\"num\":1,\"specsName\":\"S\",\"specsPrice\":218.00,\"totalPrice\":218.00}],\"totalPrice\":267.00},\"msg\":\"编辑成功\"}', 1, 410, 395);
INSERT INTO `t_sys_log` VALUES ('1566405407843397634', NULL, NULL, '2022-09-04 20:38:46', '2022-09-04 20:38:46', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:9000/login', 'http://127.0.0.1:12001/api/admin/sys/user/login', '/api/admin/sys/user/login', '127.0.0.1', '127.0.0.1', 'POST', '127.0.0.1', '49857', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"123456\",\"username\":\"admin\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 597, 597);
INSERT INTO `t_sys_log` VALUES ('1566411579493298178', NULL, NULL, '2022-09-04 21:03:17', '2022-09-04 21:03:17', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:9000/login', 'http://127.0.0.1:12001/api/admin/sys/user/login', '/api/admin/sys/user/login', '127.0.0.1', '127.0.0.1', 'POST', '127.0.0.1', '50781', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"123456\",\"username\":\"admin\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 846, 846);
INSERT INTO `t_sys_log` VALUES ('1566412147032961026', NULL, NULL, '2022-09-04 21:05:33', '2022-09-04 21:05:33', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/goods/goods', 'http://127.0.0.1:12001/api/admin/all/goods/1566343776610287617/state', '/api/admin/all/goods/1566343776610287617/state', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51012', 'com.xj.shop.manage.all.controller', 'com.xj.shop.manage.all.controller.GoodsController', '商品表', '商品上下架', '[\"1566343776610287617\",0]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 346, 339);
INSERT INTO `t_sys_log` VALUES ('1566412148253503489', NULL, NULL, '2022-09-04 21:05:33', '2022-09-04 21:05:33', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/goods/goods', 'http://127.0.0.1:12001/api/admin/all/goods/1566345405799596033/state', '/api/admin/all/goods/1566345405799596033/state', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51016', 'com.xj.shop.manage.all.controller', 'com.xj.shop.manage.all.controller.GoodsController', '商品表', '商品上下架', '[\"1566345405799596033\",0]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 247, 238);
INSERT INTO `t_sys_log` VALUES ('1566412194814472193', NULL, NULL, '2022-09-04 21:05:44', '2022-09-04 21:05:44', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/goods/goods', 'http://127.0.0.1:12001/api/admin/all/goods/1566345405799596033/state', '/api/admin/all/goods/1566345405799596033/state', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51063', 'com.xj.shop.manage.all.controller', 'com.xj.shop.manage.all.controller.GoodsController', '商品表', '商品上下架', '[\"1566345405799596033\",1]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 240, 227);
INSERT INTO `t_sys_log` VALUES ('1566412286766198786', NULL, NULL, '2022-09-04 21:06:06', '2022-09-04 21:06:06', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '50960', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":10001,\"msg\":\"没有登录\"}', 0, 15, 0);
INSERT INTO `t_sys_log` VALUES ('1566412498880540674', NULL, NULL, '2022-09-04 21:06:56', '2022-09-04 21:06:56', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/user/login', '//api/client/all/user/login', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '50960', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UuserController', '用户表', '登录', '[{\"password\":\"123456\",\"username\":\"1720696548\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 362, 343);
INSERT INTO `t_sys_log` VALUES ('1566412528576212993', NULL, NULL, '2022-09-04 21:07:04', '2022-09-04 21:07:04', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51153', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":1,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":218.00}],\"totalPrice\":218.00},\"msg\":\"编辑成功\"}', 1, 265, 248);
INSERT INTO `t_sys_log` VALUES ('1566412541444337666', NULL, NULL, '2022-09-04 21:07:07', '2022-09-04 21:07:07', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51153', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\"}]]', '{\"code\":500,\"errorMsg\":\"字符串转换为数字异常,比如int i= Integer.parseInt(“ab3”) : null\\r\\n异常类:java.lang.NumberFormatException\\r\\n详细错误内容:\\r\\njava.math.BigDecimal.<init>(BigDecimal.java:494)\\r\\njava.math.BigDecimal.<init>(BigDecimal.java:383)\\r\\njava.math.BigDecimal.<init>(BigDecimal.java:806)\\r\\ncom.xj.shop.manage.all.service.impl.OrderServiceImpl.priceCalculation(OrderServiceImpl.java:358)\\r\\ncom.xj.shop.manage.all.service.impl.OrderServiceImpl$$FastClassBySpringCGLIB$$f278eaef.invoke(<generated>)\\r\\norg.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\\r\\norg.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:685)\\r\\ncom.xj.shop.manage.all.service.impl.OrderServiceImpl$$EnhancerBySpringCGLIB$$22c9ece2.priceCalculation(<generated>)\\r\\ncom.xj.shop.client.all.controller.UorderController.priceCalculation(UorderController.java:69)\\r\\ncom.xj.shop.client.all.controller.UorderController$$FastClassBySpringCGLIB$$1845ccc0.invoke(<generated>)\\r\\norg.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\\r\\norg.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:769)\\r\\norg.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\\r\\norg.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:747)\\r\\norg.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:100)\\r\\nio.github.wslxm.springbootplus2.config.gateway.accessauthaop.SysAspect.run(SysAspect.java:224)\\r\\nio.github.wslxm.springbootplus2.config.gateway.accessauthaop.SysAspect.aroundSave(SysAspect.java:138)\\r\\nsun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\\r\\nsun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\\r\\nsun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\\r\\njava.lang.reflect.Method.invoke(Method.java:498)\\r\\norg.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\\r\\norg.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\\r\\norg.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\\r\\norg.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\\r\\norg.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:747)\\r\\norg.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:93)\\r\\norg.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\\r\\norg.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:747)\\r\\norg.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\\r\\ncom.xj.shop.client.all.controller.UorderController$$EnhancerBySpringCGLIB$$9e374b59.priceCalculation(<generated>)\\r\\nsun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\\r\\nsun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\\r\\nsun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\\r\\njava.lang.reflect.Method.invoke(Method.java:498)\\r\\norg.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:190)\\r\\norg.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\\r\\norg.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:106)\\r\\norg.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:888)\\r\\norg.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:793)\\r\\norg.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\\r\\norg.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1040)\\r\\norg.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:943)\\r\\norg.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\\r\\norg.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:909)\\r\\njavax.servlet.http.HttpServlet.service(HttpServlet.java:660)\\r\\norg.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\\r\\njavax.servlet.http.HttpServlet.service(HttpServlet.java:741)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.springframework.web.filter.CorsFilter.doFilterInternal(CorsFilter.java:92)\\r\\norg.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\nio.github.wslxm.springbootplus2.config.gateway.swaggerfilter.SwaggerFilter.doFilter(SwaggerFilter.java:53)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\nio.github.wslxm.springbootplus2.config.gateway.singfilter.SysSingFilter.doFilter(SysSingFilter.java:70)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:100)\\r\\norg.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:93)\\r\\norg.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter.doFilterInternal(WebMvcMetricsFilter.java:108)\\r\\norg.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\\r\\norg.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\\r\\norg.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\\r\\norg.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\\r\\norg.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:202)\\r\\norg.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\\r\\norg.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:526)\\r\\norg.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:139)\\r\\norg.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\\r\\norg.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:74)\\r\\norg.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\\r\\norg.apache.coyote.http11.Http11Processor.service(Http11Processor.java:367)\\r\\norg.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\\r\\norg.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:860)\\r\\norg.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1591)\\r\\norg.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\\r\\njava.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\\r\\njava.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\\r\\norg.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\\r\\njava.lang.Thread.run(Thread.java:748)\\r\\n\",\"msg\":\"服务器错误\"}', 1, 303, 276);
INSERT INTO `t_sys_log` VALUES ('1566412543696678913', NULL, NULL, '2022-09-04 21:07:07', '2022-09-04 21:07:07', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51047', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":2,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":436.00}],\"totalPrice\":436.00},\"msg\":\"编辑成功\"}', 1, 453, 436);
INSERT INTO `t_sys_log` VALUES ('1566412558125084674', NULL, NULL, '2022-09-04 21:07:11', '2022-09-04 21:07:11', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/shoppingCart', '//api/client/all/shoppingCart', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51047', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '加入购物车', '[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2}]', '{\"code\":200,\"data\":\"1566412557256863746\",\"msg\":\"添加成功\"}', 1, 622, 607);
INSERT INTO `t_sys_log` VALUES ('1566412581885816834', NULL, NULL, '2022-09-04 21:07:17', '2022-09-04 21:07:17', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51047', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":2,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":436.00}],\"totalPrice\":436.00},\"msg\":\"编辑成功\"}', 1, 359, 342);
INSERT INTO `t_sys_log` VALUES ('1566412590425419778', NULL, NULL, '2022-09-04 21:07:18', '2022-09-04 21:07:18', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51153', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":2,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":436.00}],\"totalPrice\":436.00},\"msg\":\"编辑成功\"}', 1, 279, 253);
INSERT INTO `t_sys_log` VALUES ('1566412650244583425', NULL, NULL, '2022-09-04 21:07:33', '2022-09-04 21:07:33', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51153', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2},{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":2,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":436.00},{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406755897345\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"totalPrice\":49.00}],\"totalPrice\":485.00},\"msg\":\"编辑成功\"}', 1, 581, 556);
INSERT INTO `t_sys_log` VALUES ('1566412735162462210', NULL, NULL, '2022-09-04 21:07:53', '2022-09-04 21:07:53', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/createOrder', '//api/client/all/order/createOrder', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51153', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '创建订单,返回交易号', '[{\"addressId\":\"1564258321182003201\",\"specses\":[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2},{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1}],\"userRemark\":\"买两个商品\"}]', '{\"code\":200,\"data\":\"20220904210752000001\",\"msg\":\"添加成功\"}', 1, 2465, 2448);
INSERT INTO `t_sys_log` VALUES ('1566412857325760513', NULL, NULL, '2022-09-04 21:08:22', '2022-09-04 21:08:22', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/order/order?orderNo=20220904210752000002&time=1662296901633', 'http://127.0.0.1:12001/api/admin/sys/msg/1566412737133785089/read', '/api/admin/sys/msg/1566412737133785089/read', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51245', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.MsgController', 'base--sys--消息通知', '消息修改为已读', '[\"1566412737133785089\"]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 586, 580);
INSERT INTO `t_sys_log` VALUES ('1566413026217799682', NULL, NULL, '2022-09-04 21:09:02', '2022-09-04 21:09:02', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/updState/1566412732926898177', '//api/client/all/order/updState/1566412732926898177', '192.168.0.4', '192.168.0.4', 'PUT', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '状态变更', '[\"1566412732926898177\",5]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 380, 366);
INSERT INTO `t_sys_log` VALUES ('1566413101522333698', NULL, NULL, '2022-09-04 21:09:20', '2022-09-04 21:09:20', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/order/order?orderNo=20220904210752000002&time=1662296901633', 'http://127.0.0.1:12001/api/admin/all/order/updTotalPrice/1566412732935286786', '/api/admin/all/order/updTotalPrice/1566412732935286786', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51309', 'com.xj.shop.manage.all.controller', 'com.xj.shop.manage.all.controller.OrderController', '订单表', '改价', '[\"1566412732935286786\",0.01]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 359, 353);
INSERT INTO `t_sys_log` VALUES ('1566413222410563586', NULL, NULL, '2022-09-04 21:09:49', '2022-09-04 21:09:49', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/pay', '//api/client/all/order/pay', '192.168.0.4', '192.168.0.4', 'PUT', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '修改订单为已支付(模拟接口,实际为支付回调处理)', '[\"20220904210752000001\"]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 388, 377);
INSERT INTO `t_sys_log` VALUES ('1566413412609667073', NULL, NULL, '2022-09-04 21:10:34', '2022-09-04 21:10:34', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51358', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 475, 464);
INSERT INTO `t_sys_log` VALUES ('1566413417055629313', NULL, NULL, '2022-09-04 21:10:35', '2022-09-04 21:10:35', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51357', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 405, 386);
INSERT INTO `t_sys_log` VALUES ('1566413505232482305', NULL, NULL, '2022-09-04 21:10:56', '2022-09-04 21:10:56', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/address/2', '//api/client/all/address/2', '192.168.0.4', '192.168.0.4', 'PUT', '192.168.0.4', '51357', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UaddressController', '地址管理(收货)', 'ID编辑', '[\"2\",{\"area\":\"青羊\",\"city\":\"成都\",\"fullName\":\"小二\",\"isDefault\":true,\"phone\":\"17628681111\",\"province\":\"四川\",\"specificAddress\":\"xxxx地址\",\"userId\":\"724218126669058048\"}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 895, 874);
INSERT INTO `t_sys_log` VALUES ('1566413541253165057', NULL, NULL, '2022-09-04 21:11:06', '2022-09-04 21:11:06', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51357', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 348, 338);
INSERT INTO `t_sys_log` VALUES ('1566413545996922882', NULL, NULL, '2022-09-04 21:11:06', '2022-09-04 21:11:06', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 389, 371);
INSERT INTO `t_sys_log` VALUES ('1566413616146657282', NULL, NULL, '2022-09-04 21:11:23', '2022-09-04 21:11:23', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 255, 238);
INSERT INTO `t_sys_log` VALUES ('1566413622220009473', NULL, NULL, '2022-09-04 21:11:24', '2022-09-04 21:11:24', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51357', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 304, 280);
INSERT INTO `t_sys_log` VALUES ('1566413637252395010', NULL, NULL, '2022-09-04 21:11:28', '2022-09-04 21:11:28', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51357', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":12}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":12,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":983.76}],\"totalPrice\":983.76},\"msg\":\"编辑成功\"}', 1, 357, 343);
INSERT INTO `t_sys_log` VALUES ('1566413644042973186', NULL, NULL, '2022-09-04 21:11:30', '2022-09-04 21:11:30', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":12}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":12,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":983.76}],\"totalPrice\":983.76},\"msg\":\"编辑成功\"}', 1, 385, 368);
INSERT INTO `t_sys_log` VALUES ('1566413661839405057', NULL, NULL, '2022-09-04 21:11:34', '2022-09-04 21:11:34', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/createOrder', '//api/client/all/order/createOrder', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '创建订单,返回交易号', '[{\"addressId\":\"1564258321182003201\",\"specses\":[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":12}],\"userRemark\":\"\"}]', '{\"code\":200,\"data\":\"20220904211132000001\",\"msg\":\"添加成功\"}', 1, 1795, 1780);
INSERT INTO `t_sys_log` VALUES ('1566413674007080961', NULL, NULL, '2022-09-04 21:11:37', '2022-09-04 21:11:37', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/pay', '//api/client/all/order/pay', '192.168.0.4', '192.168.0.4', 'PUT', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '修改订单为已支付(模拟接口,实际为支付回调处理)', '[\"20220904211132000001\"]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 422, 407);
INSERT INTO `t_sys_log` VALUES ('1566413848032948225', NULL, NULL, '2022-09-04 21:12:18', '2022-09-04 21:12:18', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/shoppingCart/1566365714489643010', '//api/client/all/shoppingCart/1566365714489643010', '192.168.0.4', '192.168.0.4', 'DELETE', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '删除购物车商品', '[\"1566365714489643010\"]', '{\"code\":200,\"data\":true,\"msg\":\"删除成功\"}', 1, 426, 413);
INSERT INTO `t_sys_log` VALUES ('1566413851883319297', NULL, NULL, '2022-09-04 21:12:19', '2022-09-04 21:12:19', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/shoppingCart/1566376946701561858', '//api/client/all/shoppingCart/1566376946701561858', '192.168.0.4', '192.168.0.4', 'DELETE', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '删除购物车商品', '[\"1566376946701561858\"]', '{\"code\":200,\"data\":true,\"msg\":\"删除成功\"}', 1, 249, 231);
INSERT INTO `t_sys_log` VALUES ('1566413856757100546', NULL, NULL, '2022-09-04 21:12:20', '2022-09-04 21:12:20', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/shoppingCart/1566365786602311681', '//api/client/all/shoppingCart/1566365786602311681', '192.168.0.4', '192.168.0.4', 'DELETE', '192.168.0.4', '51284', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '删除购物车商品', '[\"1566365786602311681\"]', '{\"code\":200,\"data\":true,\"msg\":\"删除成功\"}', 1, 242, 226);
INSERT INTO `t_sys_log` VALUES ('1566413898364596226', NULL, NULL, '2022-09-04 21:12:30', '2022-09-04 21:12:30', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.4', '51357', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2},{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":2,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":436.00},{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406755897345\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"totalPrice\":49.00}],\"totalPrice\":485.00},\"msg\":\"编辑成功\"}', 1, 462, 443);
INSERT INTO `t_sys_log` VALUES ('1566414152401006593', NULL, NULL, '2022-09-04 21:13:31', '2022-09-04 21:13:31', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/order/order?orderNo=20220904211132000001&time=1662297101859', 'http://127.0.0.1:12001/api/admin/all/order/1566413657317945345', '/api/admin/all/order/1566413657317945345', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51519', 'com.xj.shop.manage.all.controller', 'com.xj.shop.manage.all.controller.OrderController', '订单表', 'ID编辑(只能备注/物流单号)', '[\"1566413657317945345\",{\"platformRemark\":\"这个订单用户想早点发货\"}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 317, 310);
INSERT INTO `t_sys_log` VALUES ('1566414208143306753', NULL, NULL, '2022-09-04 21:13:44', '2022-09-04 21:13:44', 0, 0, '平台主账号', '1', 0, 'http://localhost:9000/views-shop/all/order/order?orderNo=20220904211132000001&time=1662297101859', 'http://127.0.0.1:12001/api/admin/all/order/deliverGoods/1566413657317945345', '/api/admin/all/order/deliverGoods/1566413657317945345', '127.0.0.1', '127.0.0.1', 'PUT', '127.0.0.1', '51540', 'com.xj.shop.manage.all.controller', 'com.xj.shop.manage.all.controller.OrderController', '订单表', '发货', '[\"1566413657317945345\",\"123456789\"]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 402, 393);
INSERT INTO `t_sys_log` VALUES ('1566414346521784323', NULL, NULL, '2022-09-04 21:14:17', '2022-09-04 21:14:17', 0, 0, '小爱', '724218126669058048', 1, 'http://localhost:8080/', 'http://192.168.0.4:12001//api/client/all/order/updState/1566413657317945345', '//api/client/all/order/updState/1566413657317945345', '192.168.0.4', '192.168.0.4', 'PUT', '192.168.0.4', '51577', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '状态变更', '[\"1566413657317945345\",4]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 692, 680);
INSERT INTO `t_sys_log` VALUES ('1567031102789431298', NULL, NULL, '2022-09-06 14:05:03', '2022-09-06 14:05:03', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:9000/login', 'http://127.0.0.1:12001/api/admin/sys/user/login', '/api/admin/sys/user/login', '127.0.0.1', '127.0.0.1', 'POST', '127.0.0.1', '53157', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"123456\",\"username\":\"admin\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 327, 327);
INSERT INTO `t_sys_log` VALUES ('1581847048779571201', NULL, NULL, '2022-10-17 11:18:19', '2022-10-17 11:18:19', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:9000/login', 'http://127.0.0.1:10001/api/admin/sys/user/login', '/api/admin/sys/user/login', '127.0.0.1', '127.0.0.1', 'POST', '127.0.0.1', '56028', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"527w10n8c\",\"username\":\"admin\"}]', '{\"code\":8101,\"msg\":\"密码错误\"}', 1, 142, 141);
INSERT INTO `t_sys_log` VALUES ('1581847067423252481', NULL, NULL, '2022-10-17 11:18:23', '2022-10-17 11:18:23', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:9000/login', 'http://127.0.0.1:10001/api/admin/sys/user/login', '/api/admin/sys/user/login', '127.0.0.1', '127.0.0.1', 'POST', '127.0.0.1', '56030', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"123456\",\"username\":\"admin\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 171, 170);
INSERT INTO `t_sys_log` VALUES ('1596470942088884225', NULL, NULL, '2022-11-26 19:48:28', '2022-11-26 19:48:28', 0, 0, '╥﹏╥', '0', -1, 'http://192.168.0.5:2204/login', 'http://192.168.0.5:12001/api/admin/sys/user/login', '/api/admin/sys/user/login', '192.168.0.5', '192.168.0.5', 'POST', '192.168.0.5', '48300', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"123456\",\"username\":\"admin\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 1144, 1110);
INSERT INTO `t_sys_log` VALUES ('1596472159875690497', NULL, NULL, '2022-11-26 19:53:18', '2022-11-26 19:53:18', 0, 0, '╥﹏╥', '0', -1, 'http://39.103.135.29:2204/login', 'http://192.168.0.5:12001/api/admin/sys/user/login', '/api/admin/sys/user/login', '192.168.0.5', '192.168.0.5', 'POST', '192.168.0.5', '48362', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.SysUserController', 'base--sys--用户管理', '用户登录', '[{\"password\":\"MTIzNDU2\",\"username\":\"admin\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 417, 403);
INSERT INTO `t_sys_log` VALUES ('1596472236652425217', NULL, NULL, '2022-11-26 19:53:37', '2022-11-26 19:53:37', 0, 0, '平台主账号', '1', 0, 'http://39.103.135.29:2204/views/admin/authorityv2/authority', 'http://192.168.0.5:12001/api/admin/sys/authority/756440441913741313', '/api/admin/sys/authority/756440441913741313', '192.168.0.5', '192.168.0.5', 'PUT', '192.168.0.5', '48398', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.AuthorityController', 'base--sys--URL权限管理', 'ID编辑', '[\"756440441913741313\",{\"state\":0}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 724, 705);
INSERT INTO `t_sys_log` VALUES ('1596472249134673921', NULL, NULL, '2022-11-26 19:53:40', '2022-11-26 19:53:40', 0, 0, '平台主账号', '1', 0, 'http://39.103.135.29:2204/views/admin/authorityv2/authority', 'http://192.168.0.5:12001/api/admin/sys/authority/756440441913741314', '/api/admin/sys/authority/756440441913741314', '192.168.0.5', '192.168.0.5', 'PUT', '192.168.0.5', '48400', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.AuthorityController', 'base--sys--URL权限管理', 'ID编辑', '[\"756440441913741314\",{\"state\":0}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 755, 738);
INSERT INTO `t_sys_log` VALUES ('1596472677167591426', NULL, NULL, '2022-11-26 19:55:22', '2022-11-26 19:55:22', 0, 0, '平台主账号', '1', 0, 'http://39.103.135.29:2204/views/admin/authorityv2/authority', 'http://192.168.0.5:12001/api/admin/sys/authority/756440441888575489', '/api/admin/sys/authority/756440441888575489', '192.168.0.5', '192.168.0.5', 'PUT', '192.168.0.5', '48408', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.AuthorityController', 'base--sys--URL权限管理', 'ID编辑', '[\"756440441888575489\",{\"state\":0}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 768, 752);
INSERT INTO `t_sys_log` VALUES ('1596472686290202625', NULL, NULL, '2022-11-26 19:55:24', '2022-11-26 19:55:24', 0, 0, '平台主账号', '1', 0, 'http://39.103.135.29:2204/views/admin/authorityv2/authority', 'http://192.168.0.5:12001/api/admin/sys/authority/756440441888575490', '/api/admin/sys/authority/756440441888575490', '192.168.0.5', '192.168.0.5', 'PUT', '192.168.0.5', '48412', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.AuthorityController', 'base--sys--URL权限管理', 'ID编辑', '[\"756440441888575490\",{\"state\":0}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 1121, 1103);
INSERT INTO `t_sys_log` VALUES ('1596472930407084034', NULL, NULL, '2022-11-26 19:56:22', '2022-11-26 19:56:22', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '50593', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":1}]]', '{\"code\":10001,\"msg\":\"没有登录\"}', 0, 20, 0);
INSERT INTO `t_sys_log` VALUES ('1596473031384952834', NULL, NULL, '2022-11-26 19:56:46', '2022-11-26 19:56:46', 0, 0, '平台主账号', '1', 0, 'http://39.103.135.29:2204/views/admin/authorityv2/authority', 'http://192.168.0.5:12001/api/admin/sys/authority/756440441855021062', '/api/admin/sys/authority/756440441855021062', '192.168.0.5', '192.168.0.5', 'PUT', '192.168.0.5', '48418', 'io.github.wslxm.springbootplus2.manage.sys.controller', 'io.github.wslxm.springbootplus2.manage.sys.controller.AuthorityController', 'base--sys--URL权限管理', 'ID编辑', '[\"756440441855021062\",{\"state\":0}]', '{\"code\":200,\"data\":true,\"msg\":\"编辑成功\"}', 1, 725, 713);
INSERT INTO `t_sys_log` VALUES ('1596473078705090562', NULL, NULL, '2022-11-26 19:56:57', '2022-11-26 19:56:57', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '50593', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":1,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":218.00}],\"totalPrice\":218.00},\"msg\":\"编辑成功\"}', 1, 339, 329);
INSERT INTO `t_sys_log` VALUES ('1596473086988840961', NULL, NULL, '2022-11-26 19:56:59', '2022-11-26 19:56:59', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.5:12001//api/client/all/shoppingCart', '//api/client/all/shoppingCart', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '50593', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '加入购物车', '[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":1}]', '{\"code\":10001,\"msg\":\"没有登录\"}', 0, 10, 0);
INSERT INTO `t_sys_log` VALUES ('1596473112376963073', NULL, NULL, '2022-11-26 19:57:05', '2022-11-26 19:57:05', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '50593', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406755897345\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"totalPrice\":49.00}],\"totalPrice\":49.00},\"msg\":\"编辑成功\"}', 1, 308, 297);
INSERT INTO `t_sys_log` VALUES ('1596473120044150786', NULL, NULL, '2022-11-26 19:57:07', '2022-11-26 19:57:07', 0, 0, '╥﹏╥', '0', -1, 'http://localhost:8080/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '50593', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406755897345\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"totalPrice\":49.00}],\"totalPrice\":49.00},\"msg\":\"编辑成功\"}', 1, 304, 289);
INSERT INTO `t_sys_log` VALUES ('1596475420598923266', NULL, NULL, '2022-11-26 20:06:16', '2022-11-26 20:06:16', 0, 0, '╥﹏╥', '0', -1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '51854', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566345406755897345\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406755897345\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"totalPrice\":49.00}],\"totalPrice\":49.00},\"msg\":\"编辑成功\"}', 1, 309, 301);
INSERT INTO `t_sys_log` VALUES ('1596475428987531265', NULL, NULL, '2022-11-26 20:06:18', '2022-11-26 20:06:18', 0, 0, '╥﹏╥', '0', -1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '51854', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566345406768480258\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566345406768480258\",\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"num\":1,\"specsName\":\"粉白 L\",\"specsPrice\":65.00,\"totalPrice\":65.00}],\"totalPrice\":65.00},\"msg\":\"编辑成功\"}', 1, 319, 310);
INSERT INTO `t_sys_log` VALUES ('1596475431877406721', NULL, NULL, '2022-11-26 20:06:18', '2022-11-26 20:06:18', 0, 0, '╥﹏╥', '0', -1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/shoppingCart', '//api/client/all/shoppingCart', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '51854', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '加入购物车', '[{\"goodsSpecsId\":\"1566345406768480258\",\"num\":1}]', '{\"code\":10001,\"msg\":\"没有登录\"}', 0, 10, 0);
INSERT INTO `t_sys_log` VALUES ('1596475477402382337', NULL, NULL, '2022-11-26 20:06:29', '2022-11-26 20:06:29', 0, 0, '╥﹏╥', '0', -1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/user/login', '//api/client/all/user/login', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '51854', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UuserController', '用户表', '登录', '[{\"password\":\"123455\",\"username\":\"1720696548\"}]', '{\"code\":8101,\"msg\":\"密码错误\"}', 1, 236, 211);
INSERT INTO `t_sys_log` VALUES ('1596475503725834241', NULL, NULL, '2022-11-26 20:06:36', '2022-11-26 20:06:36', 0, 0, '╥﹏╥', '0', -1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/user/login', '//api/client/all/user/login', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '51854', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UuserController', '用户表', '登录', '[{\"password\":\"123456\",\"username\":\"1720696548\"}]', '{\"code\":200,\"data\":true,\"msg\":\"成功\"}', 1, 423, 412);
INSERT INTO `t_sys_log` VALUES ('1596475534625271810', NULL, NULL, '2022-11-26 20:06:43', '2022-11-26 20:06:43', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52262', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":1,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":218.00}],\"totalPrice\":218.00},\"msg\":\"编辑成功\"}', 1, 297, 289);
INSERT INTO `t_sys_log` VALUES ('1596475557505200129', NULL, NULL, '2022-11-26 20:06:48', '2022-11-26 20:06:48', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52262', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061442\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":1,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":218.00}],\"totalPrice\":218.00},\"msg\":\"编辑成功\"}', 1, 317, 299);
INSERT INTO `t_sys_log` VALUES ('1596475565847670786', NULL, NULL, '2022-11-26 20:06:50', '2022-11-26 20:06:50', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52262', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061443\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061443\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":1,\"specsName\":\" 黑色OP+黑色发带\",\"specsPrice\":218.00,\"totalPrice\":218.00}],\"totalPrice\":218.00},\"msg\":\"编辑成功\"}', 1, 317, 309);
INSERT INTO `t_sys_log` VALUES ('1596475571480621058', NULL, NULL, '2022-11-26 20:06:52', '2022-11-26 20:06:52', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/shoppingCart', '//api/client/all/shoppingCart', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52263', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '加入购物车', '[{\"goodsSpecsId\":\"1566351125056061443\",\"num\":1}]', '{\"code\":200,\"data\":\"1596475570272661505\",\"msg\":\"添加成功\"}', 1, 893, 886);
INSERT INTO `t_sys_log` VALUES ('1596475595706920962', NULL, NULL, '2022-11-26 20:06:57', '2022-11-26 20:06:57', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52263', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 285, 272);
INSERT INTO `t_sys_log` VALUES ('1596475631106846722', NULL, NULL, '2022-11-26 20:07:06', '2022-11-26 20:07:06', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52263', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"goodsSpecsId\":\"1566349373577625602\",\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"num\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"totalPrice\":81.98}],\"totalPrice\":81.98},\"msg\":\"编辑成功\"}', 1, 288, 276);
INSERT INTO `t_sys_log` VALUES ('1596475641219313665', NULL, NULL, '2022-11-26 20:07:08', '2022-11-26 20:07:08', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/shoppingCart', '//api/client/all/shoppingCart', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52263', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UshoppingCartController', '购物车', '加入购物车', '[{\"goodsSpecsId\":\"1566349373577625602\",\"num\":1}]', '{\"code\":200,\"data\":\"1596475639998771201\",\"msg\":\"添加成功\"}', 1, 712, 689);
INSERT INTO `t_sys_log` VALUES ('1596475701793452033', NULL, NULL, '2022-11-26 20:07:23', '2022-11-26 20:07:23', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/priceCalculation', '//api/client/all/order/priceCalculation', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52262', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '价格计算', '[[{\"goodsSpecsId\":\"1566351125056061443\",\"num\":1},{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2}]]', '{\"code\":200,\"data\":{\"specsPriceVos\":[{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061443\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":1,\"specsName\":\" 黑色OP+黑色发带\",\"specsPrice\":218.00,\"totalPrice\":218.00},{\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"goodsSpecsId\":\"1566351125056061442\",\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"num\":2,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"totalPrice\":436.00}],\"totalPrice\":654.00},\"msg\":\"编辑成功\"}', 1, 491, 482);
INSERT INTO `t_sys_log` VALUES ('1596475727412260866', NULL, NULL, '2022-11-26 20:07:29', '2022-11-26 20:07:29', 0, 0, '小爱', '724218126669058048', 1, 'http://192.168.0.5:2205/', 'http://192.168.0.5:12001//api/client/all/order/createOrder', '//api/client/all/order/createOrder', '192.168.0.4', '192.168.0.4', 'POST', '192.168.0.5', '52262', 'com.xj.shop.client.all.controller', 'com.xj.shop.client.all.controller.UorderController', '订单表', '创建订单,返回交易号', '[{\"addressId\":\"1564258321182003201\",\"specses\":[{\"goodsSpecsId\":\"1566351125056061443\",\"num\":1},{\"goodsSpecsId\":\"1566351125056061442\",\"num\":2}],\"userRemark\":\"\"}]', '{\"code\":200,\"data\":\"20221126200727000001\",\"msg\":\"添加成功\"}', 1, 1785, 1761);

-- ----------------------------
-- Table structure for t_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '指定父id',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名',
  `two_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第二路由url, 前后端分离前端使用第二路由',
  `url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单url',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图标',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `root` int(1) NOT NULL DEFAULT 0 COMMENT '目录级别(1-系统, 2-菜单 ，3-页面, 4-按钮)',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '禁用(0-启用 1-禁用)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--菜单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_menu
-- ----------------------------
INSERT INTO `t_sys_menu` VALUES ('1440255471893200897', '1', '-1', '2021-09-21 18:03:57', '2022-09-04 10:01:50', 0, 0, '0', '系统管理', '', '', 'el-icon-goods', 2, 1, 0);
INSERT INTO `t_sys_menu` VALUES ('1440255602914869250', '1', '-1', '2021-09-21 18:04:29', '2022-01-17 14:33:00', 0, 0, '1440255471893200897', '系统管理', '', '', 'el-icon-setting', 100, 2, 0);
INSERT INTO `t_sys_menu` VALUES ('1440256392576483330', '1', '-1', '2021-09-21 18:07:37', '2022-09-04 10:01:53', 0, 0, '0', '商城管理', '', '', 'layui-icon-file-b', 1, 1, 0);
INSERT INTO `t_sys_menu` VALUES ('1440271162033684482', '1', '-1', '2021-09-21 19:06:18', '2022-08-20 14:14:32', 0, 0, '1440255602914869250', '用户管理', '', '/views/admin/user/user', 'el-icon-document-remove', 10004, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1442156484086480897', '1', '-1', '2021-09-26 23:57:54', '2022-08-20 14:14:36', 0, 0, '1440255602914869250', '角色管理', '', '/views/admin/role/role', 'el-icon-document-remove', 10005, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1449698334917865473', '1', '-1', '2021-10-17 19:26:30', '2022-05-15 10:44:50', 0, 0, '1449698274373087233', '页面-5级', NULL, '', 'el-icon-document-remove', 0, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1449764190750285826', '1', '-1', '2021-10-17 23:48:11', '2022-05-15 10:44:50', 0, 0, '1440255471893200897', '增强功能', '', '', 'el-icon-copy-document', 200, 2, 0);
INSERT INTO `t_sys_menu` VALUES ('1451979503835369474', '1443465040706416642', '-1', '2021-10-24 02:31:02', '2022-08-21 19:15:27', 0, 0, '1449764190750285826', 'Banner管理', '', '/views/admin/banner/banner', 'el-icon-document-remove', 20003, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1452091447254749186', '1', '-1', '2021-10-24 09:55:54', '2022-05-15 10:44:51', 0, 0, '1440255471893200897', '代码生成', NULL, '', 'el-icon-edit', 0, 2, 0);
INSERT INTO `t_sys_menu` VALUES ('1452091513113710594', '1', '-1', '2021-10-24 09:56:10', '2022-07-30 00:02:45', 0, 0, '1452091447254749186', '数据表', NULL, '/views/gc/codeGeneration/codeGeneration', 'el-icon-document-remove', 2, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1456054437146644481', '1', '-1', '2021-11-04 08:23:24', '2022-07-30 00:02:46', 0, 0, '1452091447254749186', '生成的代码测试页', NULL, '/views/test/gcTest/gcTest', 'el-icon-document-remove', 3, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1459712656557576194', '1', '-1', '2021-11-14 10:39:51', '2022-08-21 19:16:01', 0, 0, '1440255602914869250', '部门管理', NULL, '/views/admin/dep/dep', 'el-icon-document-remove', 10006, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1459850402525622274', '1', '-1', '2021-11-14 19:47:12', '2022-08-21 11:35:11', 0, 0, '1449764190750285826', '全局配置', NULL, '/views/admin/config/config', 'el-icon-document-remove', 20001, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1461987433667141634', '1', '-1', '2021-11-20 17:19:01', '2022-05-15 10:44:53', 0, 0, '1440255471893200897', '首页', NULL, '/wel/jvmInfo', 'el-icon-menu', 0, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1462256665587916801', '1', '-1', '2021-11-21 11:08:51', '2022-08-21 11:34:37', 0, 0, '1449764190750285826', '黑/白名单', NULL, '/views/admin/blacklist/blacklist', 'el-icon-document-remove', 10007, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1462261436877152258', '1', '-1', '2021-11-21 11:27:48', '2022-08-21 19:15:49', 0, 0, '1449764190750285826', '请求日志', NULL, '/views/admin/log/log', 'el-icon-document-remove', 20005, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1468415037767946242', '1', '-1', '2021-12-08 11:00:01', '2022-08-21 19:15:41', 0, 0, '1449764190750285826', '消息管理', NULL, '/views/admin/msg/msg', 'el-icon-s-comment', 20004, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1516699625820524545', '1', '-1', '2022-04-20 16:45:44', '2022-08-20 20:38:44', 0, 0, '1440255602914869250', '菜单管理', NULL, '/views/admin/menuv2/menu', 'el-icon-document-remove', 10003, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1516699798743289857', '1', '-1', '2022-04-20 16:46:25', '2022-08-21 11:34:02', 0, 0, '1440255602914869250', '字典管理', NULL, '/views/admin/dictionaryv2/dictionary', 'el-icon-document-remove', 10002, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1516699922710138882', '1', '-1', '2022-04-20 16:46:55', '2022-08-21 11:33:59', 0, 0, '1440255602914869250', '接口管理', NULL, '/views/admin/authorityv2/authority', 'el-icon-document-remove', 10001, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1552998827391221761', '1', NULL, '2022-07-29 20:45:45', '2022-08-21 11:33:53', 0, 0, '1452091447254749186', '数据源', NULL, '/views/gc/db/datasource', 'el-icon-document-remove', 1, 3, 1);
INSERT INTO `t_sys_menu` VALUES ('1562324554975584257', '1', NULL, '2022-08-24 14:22:54', '2022-09-04 10:02:10', 0, 0, '1440256392576483330', '用户管理', NULL, '/views-shop/all/user/user', 'el-icon-document-remove', 104, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1562325569980411906', '1', NULL, '2022-08-24 14:26:56', '2022-09-04 10:02:14', 0, 0, '1440256392576483330', '分类管理', NULL, '/views-shop/all/category/category', 'el-icon-document-remove', 101, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1562397168318349313', '1', NULL, '2022-08-24 19:11:25', '2022-09-04 10:02:05', 0, 0, '1440256392576483330', '商品管理', NULL, '/views-shop/all/goods/goods', 'el-icon-document-remove', 102, 3, 0);
INSERT INTO `t_sys_menu` VALUES ('1562751728853475330', '1', NULL, '2022-08-25 18:40:19', '2022-09-04 10:02:06', 0, 0, '1440256392576483330', '订单管理', NULL, '/views-shop/all/order/order', 'el-icon-document-remove', 103, 3, 0);

-- ----------------------------
-- Table structure for t_sys_msg
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_msg`;
CREATE TABLE `t_sys_msg`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息接收人',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容( 根据消息类型区分数据-str || json)',
  `user_type` int(1) NOT NULL COMMENT '通知终端: 1-用户端信息 2-管理端消息',
  `msg_type` int(11) NOT NULL COMMENT '消息类型:  1-系统通知  2-订单业务通知 ',
  `is_read` int(1) NOT NULL DEFAULT 0 COMMENT '是否已读(0-未读 1-已读)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统增强表--消息通知' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_msg
-- ----------------------------
INSERT INTO `t_sys_msg` VALUES ('1566375138625187842', NULL, NULL, '2022-09-04 18:38:29', '2022-09-04 18:39:15', 0, 0, '1', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904183829000001\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季]的订单\",\"routePathTwo\":\"\"}', 1, 3, 1);
INSERT INTO `t_sys_msg` VALUES ('1566375139241750530', NULL, NULL, '2022-09-04 18:38:29', '2022-09-04 18:38:29', 0, 0, '685453406529261568', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904183829000001\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季]的订单\",\"routePathTwo\":\"\"}', 1, 3, 0);
INSERT INTO `t_sys_msg` VALUES ('1566412734730448898', NULL, NULL, '2022-09-04 21:07:53', '2022-09-04 21:07:53', 0, 0, '1', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904210752000001\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋]的订单\",\"routePathTwo\":\"\"}', 1, 3, 0);
INSERT INTO `t_sys_msg` VALUES ('1566412735678361601', NULL, NULL, '2022-09-04 21:07:53', '2022-09-04 21:07:53', 0, 0, '685453406529261568', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904210752000001\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋]的订单\",\"routePathTwo\":\"\"}', 1, 3, 0);
INSERT INTO `t_sys_msg` VALUES ('1566412737133785089', NULL, NULL, '2022-09-04 21:07:53', '2022-09-04 21:08:22', 0, 0, '1', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904210752000002\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童]的订单\",\"routePathTwo\":\"\"}', 1, 3, 1);
INSERT INTO `t_sys_msg` VALUES ('1566412738002006017', NULL, NULL, '2022-09-04 21:07:53', '2022-09-04 21:07:53', 0, 0, '685453406529261568', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904210752000002\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童]的订单\",\"routePathTwo\":\"\"}', 1, 3, 0);
INSERT INTO `t_sys_msg` VALUES ('1566413661289951234', NULL, NULL, '2022-09-04 21:11:34', '2022-09-04 21:11:34', 0, 0, '1', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904211132000001\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季]的订单\",\"routePathTwo\":\"\"}', 1, 3, 0);
INSERT INTO `t_sys_msg` VALUES ('1566413662791512065', NULL, NULL, '2022-09-04 21:11:34', '2022-09-04 21:11:34', 0, 0, '685453406529261568', '{\"routePath\":\"/views-shop/all/order/order?orderNo=20220904211132000001\",\"title\":\"收到新的订单\",\"message\":\"用户[小爱]创建了商品[原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季]的订单\",\"routePathTwo\":\"\"}', 1, 3, 0);

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除字段(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询code(不能重复)',
  `desc` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描叙',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '禁用(0-启用 1-禁用)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES ('1443467633444806658', '1', '-1', '2021-09-30 14:47:56', '2022-07-26 16:42:33', 0, 0, 'avue-超管', 'SYS', 'avue超管', 0);
INSERT INTO `t_sys_role` VALUES ('1447115588580159489', '1446872739607478273', '-1', '2021-10-10 16:23:36', '2022-07-28 16:08:23', 0, 0, 'avue 体验账号', 'test', '-', 0);

-- ----------------------------
-- Table structure for t_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_menu`;
CREATE TABLE `t_sys_role_menu`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除字段(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id',
  `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--角色/菜单关联' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_role_menu
-- ----------------------------
INSERT INTO `t_sys_role_menu` VALUES ('1432596175978037250', NULL, NULL, '2021-08-31 14:48:38', '2021-08-31 14:48:38', 0, 0, '1432596175629910017', '7');
INSERT INTO `t_sys_role_menu` VALUES ('1432596175978037251', NULL, NULL, '2021-08-31 14:48:38', '2021-08-31 14:48:38', 0, 0, '1432596175629910017', '21');
INSERT INTO `t_sys_role_menu` VALUES ('1432596175978037252', NULL, NULL, '2021-08-31 14:48:38', '2021-08-31 14:48:38', 0, 0, '1432596175629910017', '22');
INSERT INTO `t_sys_role_menu` VALUES ('1432596175978037253', NULL, NULL, '2021-08-31 14:48:38', '2021-08-31 14:48:38', 0, 0, '1432596175629910017', '25');
INSERT INTO `t_sys_role_menu` VALUES ('1432596175978037254', NULL, NULL, '2021-08-31 14:48:38', '2021-08-31 14:48:38', 0, 0, '1432596175629910017', '1297047088646905857');
INSERT INTO `t_sys_role_menu` VALUES ('1435124452189843458', NULL, NULL, '2021-09-07 14:15:06', '2021-09-07 14:15:06', 0, 0, '1430703281969082369', '7');
INSERT INTO `t_sys_role_menu` VALUES ('1435124452189843459', NULL, NULL, '2021-09-07 14:15:06', '2021-09-07 14:15:06', 0, 0, '1430703281969082369', '21');
INSERT INTO `t_sys_role_menu` VALUES ('1435124452189843460', NULL, NULL, '2021-09-07 14:15:06', '2021-09-07 14:15:06', 0, 0, '1430703281969082369', '22');
INSERT INTO `t_sys_role_menu` VALUES ('1435124452189843461', NULL, NULL, '2021-09-07 14:15:06', '2021-09-07 14:15:06', 0, 0, '1430703281969082369', '1297047088646905857');
INSERT INTO `t_sys_role_menu` VALUES ('1435124452198232066', NULL, NULL, '2021-09-07 14:15:06', '2021-09-07 14:15:06', 0, 0, '1430703281969082369', '25');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421374619650', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1440255471893200897');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421383008257', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1461987433667141634');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421383008258', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1452091447254749186');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421383008259', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1452091513113710594');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421383008260', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1456054437146644481');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421391396866', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1440255602914869250');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421391396867', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1516699922710138882');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421391396868', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1516699798743289857');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421395591170', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1516699625820524545');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421395591171', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1440271162033684482');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421399785473', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1442156484086480897');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421399785474', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1459712656557576194');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421399785475', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1449764190750285826');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421403979778', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1459850402525622274');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421403979779', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1462256665587916801');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421403979780', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1451979503835369474');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421408174081', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1468415037767946242');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421408174082', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1462261436877152258');
INSERT INTO `t_sys_role_menu` VALUES ('1560879421412368386', NULL, NULL, '2022-08-20 14:40:26', '2022-08-20 14:40:26', 0, 0, '1447115588580159489', '1440256392576483330');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073154', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1440255471893200897');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073155', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1461987433667141634');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073156', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1452091447254749186');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073157', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1452091513113710594');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073158', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1456054437146644481');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073159', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1440255602914869250');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073160', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1516699922710138882');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073161', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1516699798743289857');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073162', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1516699625820524545');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073163', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1440271162033684482');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073164', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1442156484086480897');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073165', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1459712656557576194');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073166', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1449764190750285826');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073167', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1462256665587916801');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073168', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1459850402525622274');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073169', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1451979503835369474');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073170', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1468415037767946242');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073171', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1462261436877152258');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073172', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1440256392576483330');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073173', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1562325569980411906');
INSERT INTO `t_sys_role_menu` VALUES ('1562329839937073174', NULL, NULL, '2022-08-24 14:43:54', '2022-08-24 14:43:54', 0, 0, '1443467633444806658', '1562324554975584257');
INSERT INTO `t_sys_role_menu` VALUES ('1562397168997826561', NULL, NULL, '2022-08-24 19:11:25', '2022-08-24 19:11:25', 0, 0, '1443467633444806658', '1562397168318349313');
INSERT INTO `t_sys_role_menu` VALUES ('1562751729520369665', NULL, NULL, '2022-08-25 18:40:19', '2022-08-25 18:40:19', 0, 0, '1443467633444806658', '1562751728853475330');

-- ----------------------------
-- Table structure for t_sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role_user`;
CREATE TABLE `t_sys_role_user`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除字段(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--角色/用户关联' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_role_user
-- ----------------------------
INSERT INTO `t_sys_role_user` VALUES ('1476437959124643841', NULL, NULL, '2021-12-30 14:20:14', '2021-12-30 14:20:14', 0, 0, '1476437958340308994', '1447115588580159489');
INSERT INTO `t_sys_role_user` VALUES ('1554676109621284865', '1', '1', '2022-08-03 11:50:46', '2022-08-03 11:50:46', 0, 0, '1460427339745763329', '1447115588580159489');
INSERT INTO `t_sys_role_user` VALUES ('1554676452501442561', '1', '1', '2022-08-03 11:52:07', '2022-08-03 11:52:07', 0, 0, '685453406529261568', '1447115588580159489');
INSERT INTO `t_sys_role_user` VALUES ('1560817143874871297', NULL, NULL, '2022-08-20 10:32:58', '2022-08-20 10:32:58', 0, 0, '1', '1443467633444806658');

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号/用户名',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `gender` int(1) NOT NULL DEFAULT 0 COMMENT '性别 (0-未知 1-男 2-女)',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '是否禁用 (0-否，1-是)',
  `dep_ids` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司/部门id,多层级前端自行分割数据',
  `position` int(1) NOT NULL DEFAULT 0 COMMENT '职位 (字典code)',
  `head_pic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号(第二账号)',
  `full_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `reg_time` datetime NULL DEFAULT NULL COMMENT '注册时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `remarks` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ext1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基础表--系统用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES ('1', NULL, '0', '2020-08-05 07:11:04', '2022-11-26 19:53:18', 0, 10, 'admin', 'f2df20d8fe668e0ef08ec3ddcf632d82', 1, 0, '1548901388543594498,1548901468621246466,1548901576150618114', 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/head/53206946-4.png', '10000', '平台主账号', '', 11, '2020-08-05 15:11:05', '2022-11-26 19:53:18', NULL, NULL, NULL, NULL);
INSERT INTO `t_sys_user` VALUES ('1460427339745763329', '1', '0', '2021-11-16 09:59:45', '2022-08-23 21:17:53', 0, 0, 'test', '992171f4f472ae8360a32663d9529339', 2, 0, '1443501889504210946,1443502090977603585,1443502302433439746', 2, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/head/22358982-11.jpg', '17600000001', 'Qqq', '0', 0, '2021-11-16 09:59:46', '2022-08-23 21:17:53', NULL, NULL, NULL, NULL);
INSERT INTO `t_sys_user` VALUES ('685453406529261568', '1', '1', '2022-05-14 21:53:57', '2022-08-03 14:14:43', 0, 0, 'hexin', 'd508eefe214884b363bf33882afb4ed3', 0, 0, '1443501889504210946,1468426496627490818', 0, 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/head/25847053-8.jpg', '17600000000', '何鑫', '1', 20, '2022-05-14 21:53:57', '2022-06-17 13:53:52', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_xj_address
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_address`;
CREATE TABLE `t_xj_address`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `full_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '电话',
  `province` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '省',
  `city` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '市',
  `area` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区',
  `specific_address` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '详细地址',
  `is_default` tinyint(1) NOT NULL COMMENT '是否默认地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '地址管理(收货)' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_address
-- ----------------------------
INSERT INTO `t_xj_address` VALUES ('1', NULL, NULL, '2022-08-25 22:12:10', '2022-08-29 22:35:40', 0, 0, '724218126669058048', '小二', '17628680000', '四川', '成都', '锦江', '海椒市不详街道', 0);
INSERT INTO `t_xj_address` VALUES ('1564258321182003201', NULL, NULL, '2022-08-29 22:26:58', '2022-09-04 21:10:56', 0, 0, '724218126669058048', '王松', '17628689969', '四川', '成都', '锦江区', '海椒市都街xxx号xxx小区xx单元', 0);
INSERT INTO `t_xj_address` VALUES ('2', NULL, NULL, '2022-08-29 21:31:07', '2022-09-04 21:10:56', 0, 0, '724218126669058048', '小二', '17628681111', '四川', '成都', '青羊', 'xxxx地址', 1);

-- ----------------------------
-- Table structure for t_xj_category
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_category`;
CREATE TABLE `t_xj_category`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `pid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '父Id (顶级父id=0)',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类code (不能重复)',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `desc` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类描叙',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '禁用(通用字典 0-否 1-是)',
  `root` int(1) NULL DEFAULT 1 COMMENT '级别 (对应123  1-一级 2-二级 3-三级)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '类别表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_category
-- ----------------------------
INSERT INTO `t_xj_category` VALUES ('1443502302433439746', NULL, '-1', '2021-09-30 17:05:42', '2022-09-04 17:07:12', 0, 0, '1443502090977603585', 'csb', '测试部', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1443502428644241409', NULL, '-1', '2021-09-30 17:06:12', '2022-09-04 17:07:12', 0, 0, '1443502157943861250', 'yyb', '运营部', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1548901388543594498', NULL, NULL, '2022-07-18 13:24:02', '2022-09-04 17:07:12', 0, 0, '0', 'TEST01', '生活用品', '测试公司1的描述', 999, 0, 1);
INSERT INTO `t_xj_category` VALUES ('1548901827913715713', NULL, NULL, '2022-07-18 13:25:47', '2022-09-04 17:07:12', 0, 0, '1548901388543594498', '11', '化妆品', '部门2desc', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1548901950412558337', NULL, NULL, '2022-07-18 13:26:16', '2022-09-04 17:07:13', 0, 0, '1548901827913715713', '11', '底妆', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1549253663384408066', NULL, NULL, '2022-07-19 12:43:51', '2022-09-04 17:07:13', 0, 0, '1548901827913715713', '12', '口红', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1562390540126846978', NULL, NULL, '2022-08-24 18:45:05', '2022-09-04 17:07:24', 0, 0, '0', 'sadfs', '可爱穿搭', '-', 1, 0, 1);
INSERT INTO `t_xj_category` VALUES ('1562391331529097218', NULL, NULL, '2022-08-24 18:48:14', '2022-09-04 17:07:13', 0, 0, '1562390540126846978', 'yf', '衣服', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1562391372369035266', NULL, NULL, '2022-08-24 18:48:23', '2022-09-04 17:07:13', 0, 0, '1562390540126846978', 'kz', '裤子', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1562391445156986882', NULL, NULL, '2022-08-24 18:48:41', '2022-09-04 17:07:13', 0, 0, '1562390540126846978', 'qz', '谁不是小公主', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1562391584869253121', NULL, NULL, '2022-08-24 18:49:14', '2022-09-04 17:07:32', 0, 0, '1562391445156986882', 'lolita ', 'Lolita', '-', 101, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1562391645325950977', NULL, NULL, '2022-08-24 18:49:28', '2022-09-04 17:07:14', 0, 0, '1562391445156986882', 'xqx', '小清新', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1566338569029550082', NULL, NULL, '2022-09-04 16:13:10', '2022-09-04 17:07:14', 0, 0, '1562391445156986882', 'hf', '汉服', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1566338887721156609', NULL, NULL, '2022-09-04 16:14:26', '2022-09-04 17:07:14', 0, 0, '0', 'ins', 'ins专区', '-', 999, 0, 1);
INSERT INTO `t_xj_category` VALUES ('1566339012518477825', NULL, NULL, '2022-09-04 16:14:56', '2022-09-04 17:07:14', 0, 0, '1566338887721156609', 'jrlw', '节日礼物', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1566339118168801282', NULL, NULL, '2022-09-04 16:15:21', '2022-09-04 17:07:14', 0, 0, '1566338887721156609', 'sg', '手工', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1566339222577610753', NULL, NULL, '2022-09-04 16:15:46', '2022-09-04 17:07:15', 0, 0, '0', 'zb', '二次元周边', '-', 999, 0, 1);
INSERT INTO `t_xj_category` VALUES ('1566339325669408769', NULL, NULL, '2022-09-04 16:16:10', '2022-09-04 17:07:15', 0, 0, '1566339222577610753', 'yszb', '原神周边', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1566339492485267458', NULL, NULL, '2022-09-04 16:16:50', '2022-09-04 17:07:15', 0, 0, '1566339222577610753', 'clzb', '从零周边', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1566339930697760770', NULL, NULL, '2022-09-04 16:18:35', '2022-09-04 17:07:15', 0, 0, '1548901827913715713', 'xs', '香水', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1566340012293750786', NULL, NULL, '2022-09-04 16:18:54', '2022-09-04 17:07:15', 0, 0, '1548901388543594498', 'rcyp', '日常用品', '-', 999, 0, 2);
INSERT INTO `t_xj_category` VALUES ('1566340297938436097', NULL, NULL, '2022-09-04 16:20:02', '2022-09-04 17:07:15', 0, 0, '1566340012293750786', 'xmn', '洗面奶', '-', 999, 0, 3);
INSERT INTO `t_xj_category` VALUES ('1566340382831149058', NULL, NULL, '2022-09-04 16:20:22', '2022-09-04 17:07:16', 0, 0, '1562391445156986882', 'cosplay', 'cosplay', '-', 999, 0, 3);

-- ----------------------------
-- Table structure for t_xj_collection
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_collection`;
CREATE TABLE `t_xj_collection`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `user_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `goods_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品Id',
  `goods_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品详细快照 (json)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '我的收藏' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_collection
-- ----------------------------

-- ----------------------------
-- Table structure for t_xj_goods
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_goods`;
CREATE TABLE `t_xj_goods`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `cover_pic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品-封面图',
  `pics` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品-轮播图',
  `labels` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签 (用于搜索)',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '商品详情 (富文本)',
  `is_mail` tinyint(1) NULL DEFAULT NULL COMMENT '是否包邮(通用字典 0-否 1-是)',
  `min_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价',
  `max_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最高价',
  `category_ids` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别Ids',
  `category_one` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别1级',
  `category_two` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别2级',
  `category_three` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别3级',
  `state` int(1) NULL DEFAULT 0 COMMENT '商品状态 (1-已上架 0-下架中)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_goods
-- ----------------------------
INSERT INTO `t_xj_goods` VALUES ('1566343776610287617', NULL, NULL, '2022-09-04 16:33:52', '2022-09-04 21:05:32', 0, 0, '安妮的包裹原创正版lolita 蔷薇op复古优雅cla系短袖连衣裙现货夏', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/50018462-O1CN01ODVW2C1KlTcPMU2ir_!!1588911204.jpg', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/02762065-O1CN01iK5Rq01KlTcNn8zkO_!!1588911204.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/03412950-O1CN01JLbbup1KlTcZmzP1A_!!1588911204.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/03438427-O1CN01ODVW2C1KlTcPMU2ir_!!1588911204.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/03433506-O1CN01KvEKN81KlTcayLIBm_!!1588911204.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/03435565-O1CN012daTkn1KlTcbp92I5_!!1588911204.jpg', NULL, '<div id=\"J_MainWrap\" class=\"main-wrap  J_TRegion\">\n<div id=\"J_SubWrap\" class=\"sub-wrap\">\n<div id=\"attributes\" class=\"attributes\">\n<ul class=\"attributes-list\">\n<li title=\"安妮的包裹\">品牌:&nbsp;安妮的包裹</li>\n<li title=\"18-24周岁\">适用年龄:&nbsp;18-24周岁</li>\n<li title=\"S M L\">尺码:&nbsp;S M L</li>\n<li title=\"其他\">面料:&nbsp;其他</li>\n<li title=\"纯色\">图案:&nbsp;纯色</li>\n<li title=\"洛丽塔\">风格:&nbsp;洛丽塔</li>\n<li title=\"娃娃领\">领型:&nbsp;娃娃领</li>\n<li title=\"高腰\">腰型:&nbsp;高腰</li>\n<li title=\"拉链\">衣门襟:&nbsp;拉链</li>\n<li title=\"裙子（送项链）\">颜色分类:&nbsp;裙子（送项链）</li>\n<li title=\"泡泡袖\">袖型:&nbsp;泡泡袖</li>\n<li title=\"单件\">组合形式:&nbsp;单件</li>\n<li title=\"蔷薇短袖\">货号:&nbsp;蔷薇短袖</li>\n<li title=\"大摆型\">裙型:&nbsp;大摆型</li>\n<li title=\"夏季\">适用季节:&nbsp;夏季</li>\n<li title=\"2022年夏季\">年份季节:&nbsp;2022年夏季</li>\n<li title=\"短袖\">袖长:&nbsp;短袖</li>\n<li title=\"长裙\">裙长:&nbsp;长裙</li>\n<li title=\"蝴蝶结 荷叶边 拉链 蕾丝\">流行元素/工艺:&nbsp;蝴蝶结 荷叶边 拉链 蕾丝</li>\n<li title=\"其他/other\">款式:&nbsp;其他/other</li>\n<li title=\"A型\">廓形:&nbsp;A型</li>\n<li title=\"聚酯纤维\">材质成分:&nbsp;聚酯纤维</li>\n</ul>\n</div>\n<div id=\"tad_second_area\" class=\"tad-stage\" data-spm=\"4\"></div>\n<div id=\"description\" class=\"J_DetailSection tshop-psm ke-post\">\n<div id=\"J_DivItemDesc\" class=\"content\">\n<p><img src=\"https://img.alicdn.com/imgextra/i3/1588911204/O1CN01UesTHu1KlTcZn6WET_!!1588911204.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i3/1588911204/O1CN01uak0YY1KlTcSVtOSe_!!1588911204.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i4/1588911204/O1CN011OIs581KlTcQ4pP3d_!!1588911204.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i3/1588911204/O1CN01RXM2w51KlTcIvoZUV_!!1588911204.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1588911204/O1CN01wOs8bK1KlTcQ4ps9G_!!1588911204.jpg\" width=\"800\" height=\"935\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/1588911204/O1CN01QxIzBl1KlTcNnEhZB_!!1588911204.jpg\" width=\"800\" height=\"966\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/1588911204/O1CN01kfXnHd1KlTcTCO5Uz_!!1588911204.jpg\" width=\"800\" height=\"983\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/1588911204/O1CN01CNotRJ1KlTcXn06of_!!1588911204.jpg\" width=\"800\" height=\"1106\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/1588911204/O1CN01HeD7Hs1KlTcaySCrU_!!1588911204.jpg\" width=\"800\" height=\"1059\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/1588911204/O1CN01k1fj6Y1KlTcXn0mPA_!!1588911204.jpg\" width=\"800\" height=\"1058\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1588911204/O1CN01OlujXu1KlTcNnEAIt_!!1588911204.jpg\" width=\"800\" height=\"1105\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1588911204/O1CN01Dz6L8o1KlTcIvpRY7_!!1588911204.jpg\" width=\"800\" height=\"1079\" align=\"absmiddle\" /></p>\n</div>\n</div>\n</div>\n<div id=\"J_AsyncDCMain\" class=\"J_AsyncDC tb-custom-area tb-shop\" data-type=\"main\"></div>\n</div>\n<div class=\"tb-price-spec\">\n<h3 class=\"spec-title\" data-spm-anchor-id=\"2013.1.0.i6.23e71df9KdwGHd\">&nbsp;</h3>\n</div>', 1, 218.00, 218.00, '1562390540126846978,1562391445156986882,1562391584869253121', '1562390540126846978', '1562391445156986882', '1562391584869253121', 0);
INSERT INTO `t_xj_goods` VALUES ('1566345405799596033', NULL, NULL, '2022-09-04 16:40:20', '2022-09-04 21:05:44', 0, 0, '秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28143824-O1CN01lxUKjo1vznZWFV0hU_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28137121-O1CN01AjBEsw1vznaNTVYkm_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28146427-O1CN010iaZKy1vznZRhnkqQ_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28140838-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg', NULL, '<div id=\"attributes\" class=\"attributes\">\n<ul class=\"attributes-list\">\n<li title=\"M L\" data-spm-anchor-id=\"2013.1.0.i5.73171a17KReu07\">尺码:&nbsp;M L</li>\n<li title=\"纯色\">图案:&nbsp;纯色</li>\n<li title=\"甜美\">风格:&nbsp;甜美</li>\n<li title=\"中腰\">腰型:&nbsp;中腰</li>\n<li title=\"套头\">衣门襟:&nbsp;套头</li>\n<li title=\"浅蓝色 粉白 黑白 短袖黑色 短袖蓝灰 蓝灰条纹长袖 黑色条纹长袖 烟粉色 深棕色+小熊蝴蝶结（大领口 蓝色+小熊蝴蝶结（大领口\">颜色分类:&nbsp;浅蓝色 粉白 黑白 短袖黑色 短袖蓝灰 蓝灰条纹长袖 黑色条纹长袖 烟粉色 深棕色+小熊蝴蝶结（大领口 蓝色+小熊蝴蝶结（大领口</li>\n<li title=\"常规\">袖型:&nbsp;常规</li>\n<li title=\"单件\">组合形式:&nbsp;单件</li>\n<li title=\"夏季\">适用季节:&nbsp;夏季</li>\n<li title=\"2016年冬季\">年份季节:&nbsp;2016年冬季</li>\n<li title=\"长袖\">袖长:&nbsp;长袖</li>\n<li title=\"中长裙\">裙长:&nbsp;中长裙</li>\n<li title=\"木耳 系带 纽扣\">流行元素/工艺:&nbsp;木耳 系带 纽扣</li>\n<li title=\"公主裙\">款式:&nbsp;公主裙</li>\n<li title=\"其他材质100%\">材质成分:&nbsp;其他材质100%</li>\n</ul>\n</div>\n<div id=\"tad_second_area\" class=\"tad-stage\" data-spm=\"4\"></div>\n<div id=\"description\" class=\"J_DetailSection tshop-psm ke-post\">\n<div id=\"J_DivItemDesc\" class=\"content\">\n<div>非条纹款：衣长90cm 胸围99cm 肩宽39cm 袖长59cm 手工测量误差1~3cm 适合70-120斤1.5米以上的小可爱哟!关于色差问题,总是没有办法杜绝。 每一个人的显示器都不同,不同显示器显示的图色效果都有差别! 同时由于宝贝在拍摄的时候光线、曝光等等原因,多少都会有一点颜色改变。有线头和轻微污渍费质量问题，介意勿拍</div>\n<div>&nbsp;</div>\n<div>条纹款：&nbsp;精品雪纺面料,,不透不易皱,短袖适合夏季;模特穿起来蓬蓬的效果是因拍摄需要穿了裙撑,,我们是厂家直供,省去了中间商的差价让利给大家,每一个您看不到的环节我们都严格对待,,只为小仙女们买的放心,穿的舒心【M小码】肩宽:36 胸围:96 裙长:88 松紧腰【L大码】肩宽:38 胸围:98 裙长:89 松紧腰适合1.45米以上的哦</div>\n<p><img src=\"https://img.alicdn.com/imgextra/i2/1960646244/O1CN01XfSBwF1vznZVYkXVd_!!1960646244.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i4/1960646244/O1CN01Mw5Ygi1vznZT1l2KW_!!1960646244.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i1/1960646244/O1CN01kUc7UN1vznZTlpLe9_!!1960646244.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i3/1960646244/O1CN01kg7Zub1vznbfz33tj_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/1960646244/O1CN01WM3Rlv1vznZThKs1Z_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1960646244/O1CN016wUl051vznZQo6pUs_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1960646244/O1CN01GLGhov1vznZVYjrzU_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1960646244/O1CN01z4sA0u1vznZTeWqMK_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1960646244/O1CN01DqSFhO1vznZQo6xoQ_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/1960646244/O1CN01IESnXf1vznZQo6E7s_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/1960646244/O1CN01WwLUgN1vznZTlqtIW_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/1960646244/O1CN0154Irni1vznZTlsAHL_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/1960646244/O1CN01rNDgfs1vznZT1nFXo_!!1960646244.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/1960646244/O1CN01YFqs2v1vznZMFc4a2_!!1960646244.jpg\" align=\"absmiddle\" /></p>\n</div>\n</div>', 1, 19.99, 49.00, '1562390540126846978,1562391445156986882,1562391584869253121', '1562390540126846978', '1562391445156986882', '1562391584869253121', 1);
INSERT INTO `t_xj_goods` VALUES ('1566349369811140610', NULL, NULL, '2022-09-04 16:56:05', '2022-09-04 17:03:07', 0, 0, '原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg', NULL, '<div id=\"attributes\" class=\"attributes\">\n<ul class=\"attributes-list\">\n<li title=\"other/其他\" data-spm-anchor-id=\"2013.1.0.i2.6b6b569fvkBMUw\">品牌:&nbsp;other/其他</li>\n<li title=\"18-24周岁\">适用年龄:&nbsp;18-24周岁</li>\n<li title=\"均码\">尺码:&nbsp;均码</li>\n<li title=\"卡通动漫\">图案:&nbsp;卡通动漫</li>\n<li title=\"甜美\">风格:&nbsp;甜美</li>\n<li title=\"洛丽塔\">甜美:&nbsp;洛丽塔</li>\n<li title=\"高腰\">腰型:&nbsp;高腰</li>\n<li title=\"丁香紫 优质版 雾蓝 优质版 丁香紫 雾蓝\">颜色分类:&nbsp;丁香紫 优质版 雾蓝 优质版 丁香紫 雾蓝</li>\n<li title=\"单件\">组合形式:&nbsp;单件</li>\n<li title=\"A字裙\">裙型:&nbsp;A字裙</li>\n<li title=\"2022年春季\">年份季节:&nbsp;2022年春季</li>\n<li title=\"无袖\">袖长:&nbsp;无袖</li>\n<li title=\"中长裙\">裙长:&nbsp;中长裙</li>\n<li title=\"蝴蝶结 拼接\">流行元素/工艺:&nbsp;蝴蝶结 拼接</li>\n<li title=\"吊带\">款式:&nbsp;吊带</li>\n<li title=\"A型\">廓形:&nbsp;A型</li>\n<li title=\"其他100%\">材质成分: 其他100%</li>\n</ul>\n</div>\n<div id=\"description\" class=\"J_DetailSection tshop-psm ke-post\">\n<div id=\"J_DivItemDesc\" class=\"content\">\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/238118287/O1CN01AEky192B5UlKQ5wXr_!!238118287.png\" width=\"662\" height=\"265\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<div>\n<p>蓬松版型，</p>\n<p>仙女范儿十足</p>\n<p>一款非常甜美的连衣裙，</p>\n<p>吊带的设计，</p>\n<p>很有少女的感觉，</p>\n<p>裙摆的荷叶边设计很甜美，</p>\n<p>搭配一件白色的上衣，</p>\n<p>就可以美美的出门了。</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n</div>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/238118287/O1CN015r5Y7t2B5Ul1dIRF6_!!238118287.jpg\" width=\"800\" height=\"785\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/238118287/O1CN01Sbb0JV2B5Ukum5zIE_!!238118287.jpg\" width=\"800\" height=\"796\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/238118287/O1CN01GvnXOq2B5UkzRdYrO_!!238118287.jpg\" width=\"800\" height=\"1047\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/238118287/O1CN018oownv2B5Ul1BszgN_!!238118287.jpg\" width=\"800\" height=\"1039\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/238118287/O1CN01DYKFTu2B5UkyiEL3V_!!238118287.jpg\" width=\"800\" height=\"1027\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/238118287/O1CN01ADbUjh2B5Ukum4Vrr_!!238118287.jpg\" width=\"800\" height=\"1092\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/238118287/O1CN01JZLXVy2B5UkxYUTLe_!!238118287.jpg\" width=\"800\" height=\"1067\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/238118287/O1CN01wd4QR92B5UkxYU8Y5_!!238118287.jpg\" width=\"800\" height=\"1067\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/238118287/O1CN01BpJfAE2B5Ul1dHhY5_!!238118287.jpg\" width=\"800\" height=\"1045\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/238118287/O1CN01GpHxhB2B5UkqCHGCB_!!238118287.jpg\" width=\"800\" height=\"1041\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/238118287/O1CN01FxiwaE2B5Ul1BsWaE_!!238118287.jpg\" width=\"800\" height=\"1054\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/238118287/O1CN011FGTfN2B5UkzFWrEf_!!238118287.jpg\" width=\"800\" height=\"1053\" align=\"absmiddle\" /></p>\n</div>\n</div>', 1, 86.98, 81.98, '1562390540126846978,1562391445156986882,1562391584869253121', '1562390540126846978', '1562391445156986882', '1562391584869253121', 1);
INSERT INTO `t_xj_goods` VALUES ('1566351124082982914', NULL, NULL, '2022-09-04 17:03:03', '2022-09-04 17:03:07', 0, 0, '原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg', 'http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46907674-O1CN01qLb0qF1gfuAptUb8K_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47249860-O1CN01YxGGYC1gfuAggV3Ra_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46213385-O1CN01dAfzUX1gfuDG8zjlh_!!2699734170-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47269403-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47363450-O1CN01v1RqFz1gfuAR95Ufz_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47561853-O1CN01N3rEEM1gfuAISsHsD_!!2699734170.jpg', NULL, '<div id=\"attributes\" class=\"attributes\">\n<ul class=\"attributes-list\">\n<li title=\"honey peach lolita\" data-spm-anchor-id=\"2013.1.0.i2.68ff17bdoYNWmY\">品牌:&nbsp;honey peach lolita</li>\n<li title=\"XS S M L\">尺码:&nbsp;XS S M L</li>\n<li title=\"其他\">面料:&nbsp;其他</li>\n<li title=\"卡通动漫\">图案:&nbsp;卡通动漫</li>\n<li title=\"甜美\">风格:&nbsp;甜美</li>\n<li title=\"洛丽塔\">甜美:&nbsp;洛丽塔</li>\n<li title=\"娃娃领\">领型:&nbsp;娃娃领</li>\n<li title=\"高腰\">腰型:&nbsp;高腰</li>\n<li title=\"绀色OP 粉色OP 浅蓝色OP 生成色OP 黑色OP 绀色OP+绀色发带 粉色OP+粉色发带 浅蓝色OP+浅蓝发带 生成色OP+生成发带 黑色OP+黑色发带\">颜色分类: 绀色OP 粉色OP 浅蓝色OP 生成色OP 黑色OP 绀色OP+绀色发带 粉色OP+粉色发带 浅蓝色OP+浅蓝发带 生成色OP+生成发带 黑色OP+黑色发带</li>\n<li title=\"灯笼袖\">袖型:&nbsp;灯笼袖</li>\n<li title=\"单件\">组合形式:&nbsp;单件</li>\n<li title=\"公主裙\">裙型:&nbsp;公主裙</li>\n<li title=\"秋季\">适用季节:&nbsp;秋季</li>\n<li title=\"2021年秋季\">年份季节:&nbsp;2021年秋季</li>\n<li title=\"长袖\">袖长:&nbsp;长袖</li>\n<li title=\"中长裙\">裙长:&nbsp;中长裙</li>\n<li title=\"公主裙\">款式:&nbsp;公主裙</li>\n<li title=\"A型\">廓形:&nbsp;A型</li>\n<li title=\"其他100%\">材质成分:&nbsp;其他100%</li>\n</ul>\n</div>\n<div id=\"tad_second_area\" class=\"tad-stage\" data-spm=\"4\"></div>\n<div id=\"description\" class=\"J_DetailSection tshop-psm ke-post\">\n<div id=\"J_DivItemDesc\" class=\"content\">\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p><img src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01WyR0ay1gfuAUcMTOQ_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01x88ZFC1gfuDDA32QJ_!!2699734170.jpg\" align=\"absmiddle\" /><img src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01vIDrai1gfuD2nzBYH_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN017SwM0Z1gfuCrZ1x0Y_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN01YxGGYC1gfuAggV3Ra_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01wqb1Hh1gfuAj3SHMq_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01fqeJ5W1gfuAiH4FBF_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01skPkL61gfuCxbvS8g_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN019wXHs11gfuAPioNwP_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN0140YkW61gfuATvZ7Ii_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01VdqwCQ1gfuAIRZO9y_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p>&nbsp;</p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN01PF3fpT1gfuCP3qOPi_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01kLBWHp1gfuCP3tgSB_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01PRSSpm1gfuCR2e3Ef_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01z37w8C1gfuCN6bD9C_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN011OT6in1gfuAN67y1t_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01Mjwime1gfuAQ1HZHP_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN01zvIjTQ1gfuASpjTdi_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01xZDLlh1gfuAQVBYjM_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01KmbHln1gfuARuhIcK_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01nNmj6E1gfuATvSCbc_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01uWEuZx1gfuAQEcSBK_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01fhKZKU1gfuAQVX6aU_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN017eFqPG1gfuAk1vueu_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01C0AFmN1gfuAh4TBLV_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01cuF9yy1gfuAh4UvR9_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01K49xdj1gfuAkWxvCM_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN010mVSsc1gfuAW9bJVK_!!2699734170.jpg\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN01zjyPwk1gfuAW9cO7y_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01F40yeP1gfuAZ0sAeO_!!2699734170.jpg\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i3/2699734170/O1CN01vTqvxr1gfuAZ1SHwZ_!!2699734170.jpg\" width=\"3024\" height=\"4032\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01NODb3u1gfuAVNwW5j_!!2699734170.jpg\" width=\"2380\" height=\"3173\" align=\"absmiddle\" /><img class=\"\" src=\"https://img.alicdn.com/imgextra/i1/2699734170/O1CN01bbFjyi1gfuAXaX8T6_!!2699734170.jpg\" width=\"3024\" height=\"4032\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i2/2699734170/O1CN01Qpd4CB1gfuAXa8A5I_!!2699734170.jpg\" width=\"3024\" height=\"4032\" align=\"absmiddle\" /></p>\n<p><img class=\"\" src=\"https://img.alicdn.com/imgextra/i4/2699734170/O1CN01vINKPa1gfuAQErtMm_!!2699734170.jpg\" width=\"3024\" height=\"3024\" align=\"absmiddle\" /></p>\n</div>\n</div>', 1, 218.00, 218.00, '1562390540126846978,1562391445156986882,1562391584869253121', '1562390540126846978', '1562391445156986882', '1562391584869253121', 1);

-- ----------------------------
-- Table structure for t_xj_goods_specs
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_goods_specs`;
CREATE TABLE `t_xj_goods_specs`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `goods_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品id',
  `specs_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品规格名',
  `specs_price` decimal(10, 2) NOT NULL COMMENT '商品规格价格',
  `specs_stock` int(10) NOT NULL COMMENT '商品库存',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_goods_specs
-- ----------------------------
INSERT INTO `t_xj_goods_specs` VALUES ('1369913307475972097', NULL, NULL, '2021-03-11 15:29:18', '2022-09-02 15:43:23', 0, 0, '1369913307274645505', '11', 11.00, 11, NULL);
INSERT INTO `t_xj_goods_specs` VALUES ('1566012856539820033', NULL, NULL, '2022-09-03 18:38:54', '2022-09-03 19:17:49', 0, 0, '1562465097064947713', '大号', 100.00, 100, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566012856548208641', NULL, NULL, '2022-09-03 18:38:54', '2022-09-03 19:17:49', 0, 0, '1562465097064947713', '小号', 99.00, 100, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566012918070259713', NULL, NULL, '2022-09-03 18:39:09', '2022-09-03 18:39:09', 0, 0, '1562423751334789121', '大号', 99.00, 100, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566012918078648321', NULL, NULL, '2022-09-03 18:39:09', '2022-09-03 18:39:09', 0, 0, '1562423751334789121', '小号', 10.00, 1, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566012918078648322', NULL, NULL, '2022-09-03 18:39:09', '2022-09-03 18:39:09', 0, 0, '1562423751334789121', '小号', 10.00, 1, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566013012664397825', NULL, NULL, '2022-09-03 18:39:31', '2022-09-03 18:39:31', 0, 0, '1369830695323512834', 'M', 0.01, 10000, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566013012664397826', NULL, NULL, '2022-09-03 18:39:31', '2022-09-03 18:39:31', 0, 0, '1369830695323512834', 'L', 498.00, 10000, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566013012664397827', NULL, NULL, '2022-09-03 18:39:31', '2022-09-03 18:39:31', 0, 0, '1369830695323512834', 'M', 498.00, 10000, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566013094356856833', NULL, NULL, '2022-09-03 18:39:51', '2022-09-03 18:39:51', 0, 0, '1367005614109028353', '1米6可爱型', 1999.00, 10, 1);
INSERT INTO `t_xj_goods_specs` VALUES ('1566013094356856834', NULL, NULL, '2022-09-03 18:39:51', '2022-09-03 18:39:51', 0, 0, '1367005614109028353', '1米6可爱型', 0.10, 2, 5);
INSERT INTO `t_xj_goods_specs` VALUES ('1566016467944615937', NULL, NULL, '2022-09-03 18:53:15', '2022-09-03 19:15:53', 0, 0, '1562418382088806402', 'A', 2.00, 2, 1);
INSERT INTO `t_xj_goods_specs` VALUES ('1566016467944615938', NULL, NULL, '2022-09-03 18:53:15', '2022-09-03 18:53:15', 0, 0, '1562418382088806402', '3', 1.00, 1, 1);
INSERT INTO `t_xj_goods_specs` VALUES ('1566016467948810242', NULL, NULL, '2022-09-03 18:53:15', '2022-09-03 18:53:15', 0, 0, '1562418382088806402', '1', 1.00, 100, 100);
INSERT INTO `t_xj_goods_specs` VALUES ('1566343777826635777', NULL, NULL, '2022-09-04 16:33:52', '2022-09-04 16:33:52', 0, 0, '1566343776610287617', 'S', 218.00, 100, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566343777830830082', NULL, NULL, '2022-09-04 16:33:52', '2022-09-04 16:33:52', 0, 0, '1566343776610287617', 'M', 218.00, 100, 1);
INSERT INTO `t_xj_goods_specs` VALUES ('1566343777835024385', NULL, NULL, '2022-09-04 16:33:52', '2022-09-04 16:33:52', 0, 0, '1566343776610287617', 'L', 218.00, 100, 2);
INSERT INTO `t_xj_goods_specs` VALUES ('1566345406755897345', NULL, NULL, '2022-09-04 16:40:20', '2022-09-04 16:40:20', 0, 0, '1566345405799596033', '黑白 L', 49.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566345406760091649', NULL, NULL, '2022-09-04 16:40:20', '2022-09-04 16:40:20', 0, 0, '1566345405799596033', '黑白 M', 49.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566345406768480258', NULL, NULL, '2022-09-04 16:40:20', '2022-09-04 16:40:20', 0, 0, '1566345405799596033', '粉白 L', 65.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566345406772674561', NULL, NULL, '2022-09-04 16:40:20', '2022-09-04 16:40:20', 0, 0, '1566345405799596033', '粉白 M ', 65.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566345406772674562', NULL, NULL, '2022-09-04 16:40:20', '2022-09-04 16:40:20', 0, 0, '1566345405799596033', '小熊蝴蝶结', 19.99, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566349373577625602', NULL, NULL, '2022-09-04 16:56:06', '2022-09-04 16:56:06', 0, 0, '1566349369811140610', '丁香紫', 81.98, 100, 1);
INSERT INTO `t_xj_goods_specs` VALUES ('1566349373586014209', NULL, NULL, '2022-09-04 16:56:06', '2022-09-04 16:56:06', 0, 0, '1566349369811140610', '雾蓝', 86.98, 100, 2);
INSERT INTO `t_xj_goods_specs` VALUES ('1566351125056061442', NULL, NULL, '2022-09-04 17:03:04', '2022-09-04 17:03:04', 0, 0, '1566351124082982914', ' 生成色OP+生成发带 ', 218.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566351125056061443', NULL, NULL, '2022-09-04 17:03:04', '2022-09-04 17:03:04', 0, 0, '1566351124082982914', ' 黑色OP+黑色发带', 218.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566351125056061444', NULL, NULL, '2022-09-04 17:03:04', '2022-09-04 17:03:04', 0, 0, '1566351124082982914', '粉色OP+粉色发带', 218.00, 999, 0);
INSERT INTO `t_xj_goods_specs` VALUES ('1566351125056061445', NULL, NULL, '2022-09-04 17:03:04', '2022-09-04 17:03:04', 0, 0, '1566351124082982914', '浅蓝色OP+浅蓝发带', 218.00, 999, 0);

-- ----------------------------
-- Table structure for t_xj_order
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_order`;
CREATE TABLE `t_xj_order`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `unit_price` decimal(10, 2) NOT NULL COMMENT '单价',
  `num` int(10) NULL DEFAULT NULL COMMENT '购买数量',
  `total_price` decimal(10, 2) NOT NULL COMMENT '总价',
  `order_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单号',
  `pay_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '交易号',
  `goods_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品id',
  `goods_specs_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品规格id',
  `address_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货地址id',
  `goods_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品详情-快照 (json数据 订单查询商品使用)',
  `address_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货地址详情-快照 (json数据 订单查询商品使用)',
  `state` int(1) NOT NULL COMMENT '订单状态 (1-已下单 2-已支付 3-已发货  4-已完成  5-用户取消 6-平台拒单  7-支付失败  8-订单超时)',
  `logistics_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `user_remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户备注 (提交订单的备注)',
  `platform_remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '平台备注 (订单异常下的备注)',
  `pay_time` datetime NULL DEFAULT NULL COMMENT '支付时间',
  `deliver_time` datetime NULL DEFAULT NULL COMMENT '发货时间',
  `success_time` datetime NULL DEFAULT NULL COMMENT '完成时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_order
-- ----------------------------
INSERT INTO `t_xj_order` VALUES ('1566365923676360706', NULL, NULL, '2022-09-04 18:01:52', '2022-09-04 18:01:54', 0, 0, '724218126669058048', 81.98, 1, 81.98, '20220904180152000001', '20220904180152000001', '1566349369811140610', '1566349373577625602', '1', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"createTime\":\"2022-09-04T16:56:05\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:56:06\",\"goodsId\":\"1566349369811140610\",\"id\":\"1566349373577625602\",\"sort\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"specsStock\":100}],\"id\":\"1566349369811140610\",\"isMail\":1,\"maxPrice\":81.98,\"minPrice\":86.98,\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg\",\"state\":1}', '{\"area\":\"锦江\",\"city\":\"成都\",\"createTime\":\"2022-08-25T22:12:10\",\"fullName\":\"小二\",\"id\":\"1\",\"isDefault\":false,\"phone\":\"17628680000\",\"province\":\"四川\",\"specificAddress\":\"海椒市不详街道\",\"userId\":\"724218126669058048\"}', 2, NULL, '下单啦', NULL, '2022-09-04 18:01:55', NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566365923701526530', NULL, NULL, '2022-09-04 18:01:52', '2022-09-04 18:11:21', 0, 0, '724218126669058048', 65.00, 2, 130.00, '20220904180152000002', '20220904180152000001', '1566345405799596033', '1566345406768480258', '1', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"createTime\":\"2022-09-04T16:40:20\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:40:20\",\"goodsId\":\"1566345405799596033\",\"id\":\"1566345406768480258\",\"sort\":0,\"specsName\":\"粉白 L\",\"specsPrice\":65.00,\"specsStock\":999}],\"id\":\"1566345405799596033\",\"isMail\":1,\"maxPrice\":49.00,\"minPrice\":19.99,\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28143824-O1CN01lxUKjo1vznZWFV0hU_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28137121-O1CN01AjBEsw1vznaNTVYkm_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28146427-O1CN010iaZKy1vznZRhnkqQ_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28140838-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"state\":1}', '{\"area\":\"锦江\",\"city\":\"成都\",\"createTime\":\"2022-08-25T22:12:10\",\"fullName\":\"小二\",\"id\":\"1\",\"isDefault\":false,\"phone\":\"17628680000\",\"province\":\"四川\",\"specificAddress\":\"海椒市不详街道\",\"userId\":\"724218126669058048\"}', 4, '123456789', '下单啦', '随便备注一下', '2022-09-04 18:01:55', NULL, '2022-09-04 18:11:22');
INSERT INTO `t_xj_order` VALUES ('1566373075392409602', NULL, NULL, '2022-09-04 18:30:17', '2022-09-04 18:30:17', 0, 0, '724218126669058048', 81.98, 1, 81.98, '20220904183017000001', '20220904183017000001', '1566349369811140610', '1566349373577625602', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"createTime\":\"2022-09-04T16:56:05\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:56:06\",\"goodsId\":\"1566349369811140610\",\"id\":\"1566349373577625602\",\"sort\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"specsStock\":100}],\"id\":\"1566349369811140610\",\"isMail\":1,\"maxPrice\":81.98,\"minPrice\":86.98,\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":true,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 1, NULL, '', NULL, NULL, NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566374347130662914', NULL, NULL, '2022-09-04 18:35:20', '2022-09-04 18:35:20', 0, 0, '724218126669058048', 81.98, 1, 81.98, '20220904183520000001', '20220904183520000001', '1566349369811140610', '1566349373577625602', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"createTime\":\"2022-09-04T16:56:05\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:56:06\",\"goodsId\":\"1566349369811140610\",\"id\":\"1566349373577625602\",\"sort\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"specsStock\":100}],\"id\":\"1566349369811140610\",\"isMail\":1,\"maxPrice\":81.98,\"minPrice\":86.98,\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":true,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 1, NULL, '', NULL, NULL, NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566374907934273538', NULL, NULL, '2022-09-04 18:37:34', '2022-09-04 18:47:07', 0, 0, '724218126669058048', 81.98, 1, 81.98, '20220904183734000001', '20220904183734000001', '1566349369811140610', '1566349373577625602', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"createTime\":\"2022-09-04T16:56:05\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:56:06\",\"goodsId\":\"1566349369811140610\",\"id\":\"1566349373577625602\",\"sort\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"specsStock\":100}],\"id\":\"1566349369811140610\",\"isMail\":1,\"maxPrice\":81.98,\"minPrice\":86.98,\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":true,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 2, NULL, '', NULL, '2022-09-04 18:47:08', NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566375137715023873', NULL, NULL, '2022-09-04 18:38:29', '2022-09-04 18:47:03', 0, 0, '724218126669058048', 81.98, 1, 81.98, '20220904183829000001', '20220904183829000001', '1566349369811140610', '1566349373577625602', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"createTime\":\"2022-09-04T16:56:05\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:56:06\",\"goodsId\":\"1566349369811140610\",\"id\":\"1566349373577625602\",\"sort\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"specsStock\":100}],\"id\":\"1566349369811140610\",\"isMail\":1,\"maxPrice\":81.98,\"minPrice\":86.98,\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":true,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 2, NULL, '', NULL, '2022-09-04 18:47:03', NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566412732926898177', NULL, NULL, '2022-09-04 21:07:52', '2022-09-04 21:09:49', 0, 0, '724218126669058048', 218.00, 2, 436.00, '20220904210752000001', '20220904210752000001', '1566351124082982914', '1566351125056061442', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"createTime\":\"2022-09-04T17:03:03\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T17:03:04\",\"goodsId\":\"1566351124082982914\",\"id\":\"1566351125056061442\",\"sort\":0,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"specsStock\":999}],\"id\":\"1566351124082982914\",\"isMail\":1,\"maxPrice\":218.00,\"minPrice\":218.00,\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46907674-O1CN01qLb0qF1gfuAptUb8K_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47249860-O1CN01YxGGYC1gfuAggV3Ra_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46213385-O1CN01dAfzUX1gfuDG8zjlh_!!2699734170-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47269403-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47363450-O1CN01v1RqFz1gfuAR95Ufz_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47561853-O1CN01N3rEEM1gfuAISsHsD_!!2699734170.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":true,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 2, NULL, '买两个商品', NULL, '2022-09-04 21:09:49', NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566412732935286786', NULL, NULL, '2022-09-04 21:07:52', '2022-09-04 21:09:49', 0, 0, '724218126669058048', 49.00, 1, 0.01, '20220904210752000002', '20220904210752000001', '1566345405799596033', '1566345406755897345', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/00770107-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"createTime\":\"2022-09-04T16:40:20\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:40:20\",\"goodsId\":\"1566345405799596033\",\"id\":\"1566345406755897345\",\"sort\":0,\"specsName\":\"黑白 L\",\"specsPrice\":49.00,\"specsStock\":999}],\"id\":\"1566345405799596033\",\"isMail\":1,\"maxPrice\":49.00,\"minPrice\":19.99,\"name\":\"秋天可爱连衣裙洛丽塔日系可爱公主日常Lolita裙子萝莉塔裙女童\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28143824-O1CN01lxUKjo1vznZWFV0hU_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28137121-O1CN01AjBEsw1vznaNTVYkm_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28146427-O1CN010iaZKy1vznZRhnkqQ_!!1960646244.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/28140838-O1CN01mBPydx1vznoDGFaBD_!!0-item_pic.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":true,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 2, NULL, '买两个商品', NULL, '2022-09-04 21:09:49', NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1566413657317945345', NULL, NULL, '2022-09-04 21:11:33', '2022-09-04 21:14:17', 0, 0, '724218126669058048', 81.98, 12, 983.76, '20220904211132000001', '20220904211132000001', '1566349369811140610', '1566349373577625602', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/44963770-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg\",\"createTime\":\"2022-09-04T16:56:05\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T16:56:06\",\"goodsId\":\"1566349369811140610\",\"id\":\"1566349373577625602\",\"sort\":1,\"specsName\":\"丁香紫\",\"specsPrice\":81.98,\"specsStock\":100}],\"id\":\"1566349369811140610\",\"isMail\":1,\"maxPrice\":81.98,\"minPrice\":86.98,\"name\":\"原创洛丽塔lolita天空之城软妹日系jsk吊带连衣裙子洋装仙女夏季\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/52978511-O1CN01Dpq97b2B5UkzS245B_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53491113-O1CN01R8vdo32B5UnZ36BL5_!!238118287-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578047-O1CN01xtSazb2B5Uy5nHaNM_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53475445-O1CN01eMC60B2B5UwHyZ2hN_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53564508-O1CN01hWy9i22B5UkqCEViz_!!238118287.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/53578468-O1CN01fxouQS2B5UkvwzHXF_!!238118287.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":false,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 4, '123456789', '', '这个订单用户想早点发货', '2022-09-04 21:11:37', '2022-09-04 21:13:44', '2022-09-04 21:14:17');
INSERT INTO `t_xj_order` VALUES ('1596475725113782274', NULL, NULL, '2022-11-26 20:07:28', '2022-11-26 20:07:28', 0, 0, '724218126669058048', 218.00, 1, 218.00, '20221126200728000001', '20221126200727000001', '1566351124082982914', '1566351125056061443', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"createTime\":\"2022-09-04T17:03:03\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T17:03:04\",\"goodsId\":\"1566351124082982914\",\"id\":\"1566351125056061443\",\"sort\":0,\"specsName\":\" 黑色OP+黑色发带\",\"specsPrice\":218.00,\"specsStock\":999}],\"id\":\"1566351124082982914\",\"isMail\":1,\"maxPrice\":218.00,\"minPrice\":218.00,\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46907674-O1CN01qLb0qF1gfuAptUb8K_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47249860-O1CN01YxGGYC1gfuAggV3Ra_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46213385-O1CN01dAfzUX1gfuDG8zjlh_!!2699734170-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47269403-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47363450-O1CN01v1RqFz1gfuAR95Ufz_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47561853-O1CN01N3rEEM1gfuAISsHsD_!!2699734170.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":false,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 1, NULL, '', NULL, NULL, NULL, NULL);
INSERT INTO `t_xj_order` VALUES ('1596475725113782275', NULL, NULL, '2022-11-26 20:07:28', '2022-11-26 20:07:28', 0, 0, '724218126669058048', 218.00, 2, 436.00, '20221126200728000002', '20221126200727000001', '1566351124082982914', '1566351125056061442', '1564258321182003201', '{\"categoryIds\":\"1562390540126846978,1562391445156986882,1562391584869253121\",\"categoryOne\":\"1562390540126846978\",\"categoryThree\":\"1562391584869253121\",\"categoryTwo\":\"1562391445156986882\",\"coverPic\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/38231170-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg\",\"createTime\":\"2022-09-04T17:03:03\",\"goodsSpecsList\":[{\"createTime\":\"2022-09-04T17:03:04\",\"goodsId\":\"1566351124082982914\",\"id\":\"1566351125056061442\",\"sort\":0,\"specsName\":\" 生成色OP+生成发带 \",\"specsPrice\":218.00,\"specsStock\":999}],\"id\":\"1566351124082982914\",\"isMail\":1,\"maxPrice\":218.00,\"minPrice\":218.00,\"name\":\"原创正版梦境游乐园洛丽塔Lolita日常可爱蝴蝶结连衣裙公主裙春秋\",\"pics\":\"http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46907674-O1CN01qLb0qF1gfuAptUb8K_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47249860-O1CN01YxGGYC1gfuAggV3Ra_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/46213385-O1CN01dAfzUX1gfuDG8zjlh_!!2699734170-0-lubanu-s.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47269403-O1CN01gZiP8n1gfuN3DvSEv_!!0-item_pic.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47363450-O1CN01v1RqFz1gfuAR95Ufz_!!2699734170.jpg,http://xijia-sz.oss-cn-shenzhen.aliyuncs.com/oss/file/image/gc/47561853-O1CN01N3rEEM1gfuAISsHsD_!!2699734170.jpg\",\"state\":1}', '{\"area\":\"锦江区\",\"city\":\"成都\",\"createTime\":\"2022-08-29T22:26:58\",\"fullName\":\"王松\",\"id\":\"1564258321182003201\",\"isDefault\":false,\"phone\":\"17628689969\",\"province\":\"四川\",\"specificAddress\":\"海椒市都街xxx号xxx小区xx单元\",\"userId\":\"724218126669058048\"}', 1, NULL, '', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_xj_order_log
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_order_log`;
CREATE TABLE `t_xj_order_log`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单id',
  `operation_full_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人姓名',
  `operation_user_type` int(2) NULL DEFAULT NULL COMMENT '操作人端（对应jwtuser的Type）',
  `operation_type` int(1) NOT NULL COMMENT '操作类型 (字典code 1-用户下单,更多)',
  `operation_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作类型名称',
  `front_state` int(2) NULL DEFAULT NULL COMMENT '操作前状态',
  `after_state` int(2) NOT NULL COMMENT '操作后状态',
  `remarks` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `ext1` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `ext2` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `ext3` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单变化相关记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_order_log
-- ----------------------------
INSERT INTO `t_xj_order_log` VALUES ('1566365924498444289', NULL, NULL, '2022-09-04 18:01:52', '2022-09-04 18:01:52', 0, 0, '1566365923676360706', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566365924502638593', NULL, NULL, '2022-09-04 18:01:52', '2022-09-04 18:01:52', 0, 0, '1566365923701526530', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566365933713330178', NULL, NULL, '2022-09-04 18:01:54', '2022-09-04 18:01:54', 0, 0, '1566365923676360706', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566365933717524481', NULL, NULL, '2022-09-04 18:01:54', '2022-09-04 18:01:54', 0, 0, '1566365923701526530', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566367518795337731', NULL, NULL, '2022-09-04 18:08:12', '2022-09-04 18:08:12', 0, 0, '1566365923701526530', NULL, NULL, 4, '平台发货', 2, 3, '订单发货', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566368312336621570', NULL, NULL, '2022-09-04 18:11:21', '2022-09-04 18:11:21', 0, 0, '1566365923701526530', NULL, NULL, 6, '用户收货', 3, 4, '用户收货', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566373077829300225', NULL, NULL, '2022-09-04 18:30:18', '2022-09-04 18:30:18', 0, 0, '1566373075392409602', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566374350041509890', NULL, NULL, '2022-09-04 18:35:21', '2022-09-04 18:35:21', 0, 0, '1566374347130662914', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566374969858977793', NULL, NULL, '2022-09-04 18:37:49', '2022-09-04 18:37:49', 0, 0, '1566374907934273538', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566375139887673346', NULL, NULL, '2022-09-04 18:38:29', '2022-09-04 18:38:29', 0, 0, '1566375137715023873', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566377294635855874', NULL, NULL, '2022-09-04 18:47:03', '2022-09-04 18:47:03', 0, 0, '1566375137715023873', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566377294648438786', NULL, NULL, '2022-09-04 18:47:03', '2022-09-04 18:47:03', 0, 0, '1566375137715023873', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566377312713306114', NULL, NULL, '2022-09-04 18:47:07', '2022-09-04 18:47:07', 0, 0, '1566374907934273538', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566412739058970626', NULL, NULL, '2022-09-04 21:07:54', '2022-09-04 21:07:54', 0, 0, '1566412732926898177', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566412739063164930', NULL, NULL, '2022-09-04 21:07:54', '2022-09-04 21:07:54', 0, 0, '1566412732935286786', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566413026221993986', NULL, NULL, '2022-09-04 21:09:02', '2022-09-04 21:09:02', 0, 0, '1566412732926898177', NULL, NULL, 3, '用户取消', 1, 5, '用户取消订单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566413101522333697', NULL, NULL, '2022-09-04 21:09:20', '2022-09-04 21:09:20', 0, 0, '1566412732935286786', NULL, NULL, 9, '平台改价', 1, 1, '订单改价, 修改前：49.00 修改后：0.01', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566413222410563585', NULL, NULL, '2022-09-04 21:09:49', '2022-09-04 21:09:49', 0, 0, '1566412732926898177', NULL, NULL, 2, '用户支付', 5, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566413222414757889', NULL, NULL, '2022-09-04 21:09:49', '2022-09-04 21:09:49', 0, 0, '1566412732935286786', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566413663672315906', NULL, NULL, '2022-09-04 21:11:35', '2022-09-04 21:11:35', 0, 0, '1566413657317945345', NULL, NULL, 1, '用户下单', NULL, 1, '用户下单', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566413674007080962', NULL, NULL, '2022-09-04 21:11:37', '2022-09-04 21:11:37', 0, 0, '1566413657317945345', NULL, NULL, 2, '用户支付', 1, 2, '订单支付成功', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566414208143306754', NULL, NULL, '2022-09-04 21:13:44', '2022-09-04 21:13:44', 0, 0, '1566413657317945345', NULL, NULL, 4, '平台发货', 2, 3, '订单发货', NULL, NULL, NULL);
INSERT INTO `t_xj_order_log` VALUES ('1566414346521784322', NULL, NULL, '2022-09-04 21:14:17', '2022-09-04 21:14:17', 0, 0, '1566413657317945345', NULL, NULL, 6, '用户收货', 3, 4, '用户收货', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for t_xj_shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_shopping_cart`;
CREATE TABLE `t_xj_shopping_cart`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0-正常  1-删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `goods_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品Id',
  `goods_specs_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格id',
  `num` int(11) NULL DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '购物车' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_xj_shopping_cart
-- ----------------------------
INSERT INTO `t_xj_shopping_cart` VALUES ('1566365714489643010', NULL, NULL, '2022-09-04 18:01:02', '2022-09-04 21:12:18', 1, 0, '724218126669058048', '1566345405799596033', '1566345406768480258', 2);
INSERT INTO `t_xj_shopping_cart` VALUES ('1566365786602311681', NULL, NULL, '2022-09-04 18:01:19', '2022-09-04 21:12:20', 1, 0, '724218126669058048', '1566349369811140610', '1566349373577625602', 1);
INSERT INTO `t_xj_shopping_cart` VALUES ('1566376946701561858', NULL, NULL, '2022-09-04 18:45:40', '2022-09-04 21:12:19', 1, 0, '724218126669058048', '1566343776610287617', '1566343777826635777', 1);
INSERT INTO `t_xj_shopping_cart` VALUES ('1566377107829944322', NULL, NULL, '2022-09-04 18:46:18', '2022-09-04 18:46:18', 0, 0, '724218126669058048', '1566345405799596033', '1566345406755897345', 1);
INSERT INTO `t_xj_shopping_cart` VALUES ('1566377108253569025', NULL, NULL, '2022-09-04 18:46:19', '2022-09-04 18:46:19', 0, 0, '724218126669058048', '1566345405799596033', '1566345406755897345', 1);
INSERT INTO `t_xj_shopping_cart` VALUES ('1566412557256863746', NULL, NULL, '2022-09-04 21:07:10', '2022-09-04 21:07:10', 0, 0, '724218126669058048', '1566351124082982914', '1566351125056061442', 2);
INSERT INTO `t_xj_shopping_cart` VALUES ('1596475570272661505', NULL, NULL, '2022-11-26 20:06:51', '2022-11-26 20:06:51', 0, 0, '724218126669058048', '1566351124082982914', '1566351125056061443', 1);
INSERT INTO `t_xj_shopping_cart` VALUES ('1596475639998771201', NULL, NULL, '2022-11-26 20:07:08', '2022-11-26 20:07:08', 0, 0, '724218126669058048', '1566349369811140610', '1566349373577625602', 1);

-- ----------------------------
-- Table structure for t_xj_user
-- ----------------------------
DROP TABLE IF EXISTS `t_xj_user`;
CREATE TABLE `t_xj_user`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建账户id',
  `update_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新账户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除(0：正常 1：删除)',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '乐观锁',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号/用户名',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱地址',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `gender` int(1) NOT NULL DEFAULT 0 COMMENT '性别 (0-未知 1-男 2-女)',
  `head_pic` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `full_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `reg_time` datetime NULL DEFAULT NULL COMMENT '注册时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `disable` int(1) NOT NULL DEFAULT 0 COMMENT '是否禁用 (通用字典 0-否，1-是)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '工具--用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_xj_user
-- ----------------------------
INSERT INTO `t_xj_user` VALUES ('683567436179574784', NULL, NULL, '2022-05-09 16:59:47', '2022-08-24 14:52:42', 0, 0, '15281519020', '1275096725@qq.com', '7c9e788606f3428553ab31de105c6475', 0, 'https://tva3.sinaimg.cn/large/0072Vf1pgy1foxkizc9w8j31kw0w0e45.jpg', NULL, '曦夜', NULL, NULL, 0, '2022-05-09 16:59:47', '2022-05-09 17:01:16', 0);
INSERT INTO `t_xj_user` VALUES ('698200386263191553', NULL, NULL, '2022-07-03 18:33:40', '2022-08-24 15:15:41', 0, 0, '2191879899', '2191879899@qq.com', 'd63386ab6da8d4523c85b9b6a55b4a30', 2, 'https://tva3.sinaimg.cn/large/0072Vf1pgy1foxkizc9w8j31kw0w0e45.jpg', NULL, '佳佳想吃糖ฅ', NULL, NULL, 0, '2022-07-03 18:34:05', '2022-07-03 19:06:21', 0);
INSERT INTO `t_xj_user` VALUES ('705020998231134208', NULL, NULL, '2022-07-07 21:48:34', '2022-08-25 23:07:26', 0, 0, '2121180858', '2121180858@qq.com', '7ab881551431ea3510a2ff5f0fc85311', 0, 'https://tva3.sinaimg.cn/large/0072Vf1pgy1foxkizc9w8j31kw0w0e45.jpg', NULL, '芊芊', NULL, NULL, NULL, '2022-07-07 21:48:35', '2022-07-07 21:49:01', 0);
INSERT INTO `t_xj_user` VALUES ('724218126669058048', NULL, NULL, '2022-08-29 21:11:04', '2022-11-26 20:06:35', 0, 0, '1720696548', '1720696548@qq.com', 'b7153c171c0485e23a0dd6f97a4d4840', 0, 'https://tva3.sinaimg.cn/large/0072Vf1pgy1foxkizc9w8j31kw0w0e45.jpg', NULL, '小爱', NULL, NULL, NULL, '2022-08-29 21:11:07', '2022-11-26 20:06:36', 0);

SET FOREIGN_KEY_CHECKS = 1;
