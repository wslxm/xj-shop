package com.xj.shop.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("all")
public interface Shop{

    // -
    @Getter
    @AllArgsConstructor
    enum GoodsState implements IEnum<Integer> {
        V0(0, "下架中"),    // -
        V1(1, "已上架"),    // -
        ;
        private Integer value;
        private String desc;
    }

    // -
    @Getter
    @AllArgsConstructor
    enum IsMail implements IEnum<Integer> {
        V1(1, "包邮"),    // -
        V0(0, "不包邮"),    // -
        ;
        private Integer value;
        private String desc;
    }

    // -
    @Getter
    @AllArgsConstructor
    enum OperationType implements IEnum<Integer> {
        V1(1, "用户下单"),    // -
        V2(2, "用户支付"),    // -
        V3(3, "用户取消"),    // -
        V4(4, "平台发货"),    // -
        V5(5, "平台拒单"),    // -
        V6(6, "用户收货"),    // -
        V7(7, "支付失败"),    // -支付回调，检查到支付失败触发
        V8(8, "订单自动超时"),    // -订单创建后超过15分钟自动超时
        V9(9, "平台改价"),    // -用户下单未支付情况下改价
        ;
        private Integer value;
        private String desc;
    }

    // -
    @Getter
    @AllArgsConstructor
    enum OrderState implements IEnum<Integer> {
        V1(1, "已下单"),    // -
        V2(2, "已支付"),    // -
        V3(3, "已发货"),    // -
        V4(4, "已完成"),    // -
        V5(5, "用户取消"),    // -
        V6(6, "平台拒单"),    // -
        V7(7, "支付失败"),    // -
        V8(8, "订单超时"),    // 15分钟时间不支付自动超时
        ;
        private Integer value;
        private String desc;
    }
}
