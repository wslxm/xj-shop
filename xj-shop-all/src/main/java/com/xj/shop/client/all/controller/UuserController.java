package com.xj.shop.client.all.controller;

import com.xj.shop.client.all.model.LoginUserDTO;
import com.xj.shop.client.all.model.RegisterUserDTO;
import com.xj.shop.manage.all.model.dto.UserDTO;
import com.xj.shop.manage.all.model.vo.UserVO;
import com.xj.shop.manage.all.service.UserService;
import io.github.wslxm.springbootplus2.common.auth.util.JwtUtil;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.github.wslxm.springbootplus2.core.utils.validated.ValidUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 工具--用户表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 11:17:09
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/user")
@Api(value = "UserController", tags = "用户表")
public class UuserController extends BaseController<UserService> {

    @PostMapping("register")
    @ApiOperation(value = "注册账号")
    public Result<String> register(@RequestBody @Validated RegisterUserDTO dto) {
        return Result.success(baseService.register(dto));
    }

    @PostMapping("sendEmailAuthCode")
    @ApiOperation(value = "发送电子邮件验证码")
    public Result<Boolean> sendEmailAuthCode(@RequestParam String email) {
        ValidUtil.isEmail(email, "邮箱格式错误");
        return Result.success(baseService.sendEmailAuthCode(email));
    }

    @PostMapping(value = "login")
    @ApiOperation(value = "登录")
    public Result<Boolean> login(@RequestBody @Validated LoginUserDTO dto) {
        return Result.success(baseService.login(dto));
    }

    @GetMapping(value = "findLoginUser")
    @ApiOperation(value = "查询当前登录人的信息")
    public Result<UserVO> findLoginUser() {
        return Result.success(baseService.findId(JwtUtil.getJwtUser(request).getUserId()));
    }

    @PutMapping(value = "/updLoginUser")
    @ApiOperation(value = "编辑当前登录人的个人信息")
    public Result<Boolean> updLoginUser(@RequestBody @Validated UserDTO dto) {
        String userId = JwtUtil.getJwtUser(request).getUserId();
        return Result.successUpdate(baseService.upd(userId, dto));
    }
}
