package com.xj.shop.client.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.enums.Shop;
import com.xj.shop.manage.all.model.query.GoodsQuery;
import com.xj.shop.manage.all.model.vo.GoodsVO;
import com.xj.shop.manage.all.service.GoodsService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商品表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/goods")
@Api(value = "UgoodsController", tags = "商品表")
public class UgoodsController extends BaseController<GoodsService> {

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表查询")
    public Result<IPage<GoodsVO>> findPage(@ModelAttribute @Validated GoodsQuery query) {
        query.setState(Shop.GoodsState.V1.getValue());
        return Result.success(baseService.findPage(query));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "ID查询")
    public Result<GoodsVO> findId(@PathVariable String id) {
        return Result.success(baseService.findId(id));
    }

}
