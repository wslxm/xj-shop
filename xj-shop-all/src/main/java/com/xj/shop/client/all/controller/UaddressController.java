package com.xj.shop.client.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.dto.AddressDTO;
import com.xj.shop.manage.all.model.query.AddressQuery;
import com.xj.shop.manage.all.model.vo.AddressVO;
import com.xj.shop.manage.all.model.vo.GoodsVO;
import com.xj.shop.manage.all.service.AddressService;
import io.github.wslxm.springbootplus2.common.auth.util.JwtUtil;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 地址管理(收货) 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/address")
@Api(value = "UaddressController", tags = "地址管理(收货)")
public class UaddressController extends BaseController<AddressService> {

	@GetMapping(value = "/findPage")
	@ApiOperation(value = "列表查询")
	public Result<IPage<AddressVO>> findPage(@ModelAttribute @Validated AddressQuery query) {
		query.setUserId(JwtUtil.getJwtUser(request).getUserId());
		return Result.success(baseService.findPage(query));
	}

	@GetMapping(value = "/{id}")
	@ApiOperation(value = "ID查询")
	public Result<AddressVO> findId(@PathVariable String id) {
		return Result.success(baseService.findId(id));
	}

	@PostMapping
	@ApiOperation(value = "添加")
	public Result<String> insert(@RequestBody @Validated AddressDTO dto) {
		dto.setUserId(JwtUtil.getJwtUser(request).getUserId());
		return Result.successInsert(baseService.insert(dto));
	}


	@PutMapping(value = "/{id}")
	@ApiOperation(value = "ID编辑")
	public Result<Boolean> upd(@PathVariable String id, @RequestBody @Validated AddressDTO dto) {
		return Result.successUpdate(baseService.upd(id, dto));
	}

	@DeleteMapping(value = "/updIsDefault/{id}")
	@ApiOperation(value = "设置为默认地址")
	public Result<Boolean> updIsDefault(@PathVariable String id) {
		return Result.successDelete(baseService.updIsDefault(id));
	}

	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "ID删除")
	public Result<Boolean> del(@PathVariable String id) {
		return Result.successDelete(baseService.del(id));
	}

}
