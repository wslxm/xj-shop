package com.xj.shop.client.all.model;

import io.github.wslxm.springbootplus2.core.base.model.Convert;
import io.github.wslxm.springbootplus2.core.utils.validated.RegUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 基础表--系统用户 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-02-20 13:55:26
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "RegisterUserDTO 对象", description = "注册用户对象")
public class RegisterUserDTO extends Convert {

    private static final long serialVersionUID = -655254791525634066L;

    @ApiModelProperty(value = "账号/用户名", position = 0)
    @Length(min = 2, max = 16, message = " 必须>=2 和 <=16位")
    @NotBlank(message="账号/用户名 不能为空")
    private String username;

    @ApiModelProperty(value = "昵称", position = 7)
    @Length(min = 2, max = 16, message = "昵称 必须>=2 和 <=16位")
    @NotBlank(message="昵称 不能为空")
    private String nickname;

    @ApiModelProperty(value = "邮箱地址", position = 1)
    @Pattern(regexp = RegUtil.EMAIL,message = RegUtil.EMAIL_MSG)
    @NotBlank(message="邮箱地址不能为空")
    private String email;

    @ApiModelProperty(value = "邮箱验证码", position = 1)
    @Length(min = 2, max = 16, message = "请输入4位的验证码")
    @NotBlank(message="邮箱验证码 不能为空")
    private String emailCode;

    @ApiModelProperty(value = "密码", position = 2)
    @Length(min = 6, max = 18, message = "密码 必须>=6 和 <=18位")
    @NotBlank(message="密码 不能为空")
    private String password;

}

