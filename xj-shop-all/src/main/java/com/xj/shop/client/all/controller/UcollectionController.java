package com.xj.shop.client.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.query.CategoryQuery;
import com.xj.shop.manage.all.model.query.CollectionQuery;
import com.xj.shop.manage.all.model.vo.CategoryVO;
import com.xj.shop.manage.all.model.vo.CollectionVO;
import com.xj.shop.manage.all.service.CollectionService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 我的收藏 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:52:04
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/collection")
@Api(value = "CollectionController", tags = "我的收藏")
public class UcollectionController extends BaseController<CollectionService> {

	@GetMapping(value = "/findPage")
	@ApiOperation(value = "列表查询")
	public Result<IPage<CollectionVO>> findPage(@ModelAttribute @Validated CollectionQuery query) {
		return Result.success(baseService.findPage(query));
	}




	@PutMapping(value = "/collect/{id}")
	@ApiOperation(value = "收藏/取消收藏")
	public Result<Boolean> collect(@PathVariable String id, @RequestParam  String goodsId) {
		return Result.successUpdate(baseService.collect(id,goodsId));
	}

}
