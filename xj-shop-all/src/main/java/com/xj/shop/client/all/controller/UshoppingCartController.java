package com.xj.shop.client.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.dto.ShoppingCartDTO;
import com.xj.shop.manage.all.model.query.ShoppingCartQuery;
import com.xj.shop.manage.all.model.vo.ShoppingCartVO;
import com.xj.shop.manage.all.service.ShoppingCartService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 购物车 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/shoppingCart")
@Api(value = "ShoppingCartController", tags = "购物车")
public class UshoppingCartController extends BaseController<ShoppingCartService> {

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表查询")
    public Result<IPage<ShoppingCartVO>> findPage(@ModelAttribute @Validated ShoppingCartQuery query) {
        return Result.success(baseService.findPage(query));
    }


    @PostMapping
    @ApiOperation(value = "加入购物车")
    public Result<String> insert(@RequestBody @Validated ShoppingCartDTO dto) {
        return Result.successInsert(baseService.insert(dto));
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "删除购物车商品")
    public Result<Boolean> del(@PathVariable String id) {
        return Result.successDelete(baseService.del(id));
    }

}
