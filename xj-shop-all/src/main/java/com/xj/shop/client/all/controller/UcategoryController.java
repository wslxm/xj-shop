package com.xj.shop.client.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.query.CategoryQuery;
import com.xj.shop.manage.all.model.vo.CategoryVO;
import com.xj.shop.manage.all.service.CategoryService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 类别表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/category")
@Api(value = "UcategoryController", tags = "类别表")
public class UcategoryController extends BaseController<CategoryService> {

    @GetMapping(value = "/tree")
    @ApiOperation(value = "树结构数据查询")
    public Result<List<CategoryVO>> tree(@ModelAttribute @Validated CategoryQuery query) {
        List<CategoryVO> tree = baseService.tree(query);
        return Result.success(tree);
    }

    @GetMapping(value = "/page")
    @ApiOperation(value = "分页查询")
    public Result<IPage<CategoryVO>> findPage(@ModelAttribute @Validated CategoryQuery query) {
        return Result.success(baseService.findPage(query));
    }

}
