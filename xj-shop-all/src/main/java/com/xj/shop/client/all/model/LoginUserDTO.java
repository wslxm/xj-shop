package com.xj.shop.client.all.model;

import io.github.wslxm.springbootplus2.core.base.model.Convert;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 基础表--系统用户 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-02-20 13:55:26
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "LoginUserDTO 对象", description = "登录用户对象")
public class LoginUserDTO extends Convert {

    private static final long serialVersionUID = -655254791525634066L;

    @ApiModelProperty(value = "账号/邮箱", position = 0)
    @Length(min = 2, max = 30, message = "账号/邮箱 必须>=2 和 <=16位")
    @NotBlank
    private String username;

    @ApiModelProperty(value = "密码", position = 2)
    @Length(min = 6, max = 18, message = "密码 必须>=6 和 <=18位")
    @NotBlank
    private String password;

}

