package com.xj.shop.client.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.dto.OrderDTO;
import com.xj.shop.manage.all.model.dto.OrderPriceCalculationDTO;
import com.xj.shop.manage.all.model.query.OrderQuery;
import com.xj.shop.manage.all.model.vo.OrderPriceCalculationVO;
import com.xj.shop.manage.all.model.vo.OrderVO;
import com.xj.shop.manage.all.service.OrderService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_CLIENT + "/all/order")
@Api(value = "OrderController", tags = "订单表")
public class UorderController extends BaseController<OrderService> {

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表查询")
    public Result<IPage<OrderVO>> findPage(@ModelAttribute @Validated OrderQuery query) {
        return Result.success(baseService.findPage(query));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "ID查询")
    public Result<OrderVO> findId(@PathVariable String id) {
        return Result.success(baseService.findId(id));
    }

    @PostMapping("/createOrder")
    @ApiOperation(value = "创建订单,返回交易号")
    public Result<String> createOrder(@RequestBody @Validated OrderDTO dto) {
        return Result.successInsert(baseService.createOrder(dto));
    }

    @PutMapping(value = "/updState/{id}")
    @ApiOperation(value = "状态变更")
    public Result<Boolean> updState(@PathVariable String id, @RequestParam Integer state) {
        return Result.successUpdate(baseService.updState(id, state));
    }

    @PutMapping(value = "/pay")
    @ApiOperation(value = "修改订单为已支付(模拟接口,实际为支付回调处理)")
    public Result<Boolean> pay(@RequestParam String payNo) {
        return Result.successUpdate(baseService.pay(payNo));
    }

    @PostMapping(value = "/priceCalculation")
    @ApiOperation(value = "价格计算")
    public Result<OrderPriceCalculationVO> priceCalculation(@RequestBody List<OrderPriceCalculationDTO> dtos) {
        return Result.successUpdate(baseService.priceCalculation(dtos));
    }
}
