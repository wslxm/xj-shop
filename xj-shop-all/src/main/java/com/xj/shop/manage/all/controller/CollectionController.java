//package com.xj.shop.manage.all.controller;
//
//import com.xj.shop.manage.all.service.CollectionService;
//import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
//import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
//import io.swagger.annotations.Api;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * 我的收藏 前端控制器
// *
// * <p>
// * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
// * </p>
// *
// * @author ws
// * @email 1720696548@qq.com
// * @date 2022-08-24 18:52:04
// */
//@RestController
//@RequestMapping(BaseConstant.Uri.API_ADMIN + "/all/collection")
//@Api(value = "CollectionController", tags = "我的收藏")
//public class CollectionController extends BaseController<CollectionService> {
//
//
//
//}
