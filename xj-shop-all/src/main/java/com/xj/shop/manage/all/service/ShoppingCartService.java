package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.entity.ShoppingCart;
import com.xj.shop.manage.all.model.vo.ShoppingCartVO;
import com.xj.shop.manage.all.model.dto.ShoppingCartDTO;
import com.xj.shop.manage.all.model.query.ShoppingCartQuery;

/**
 * 购物车 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
public interface ShoppingCartService extends IService<ShoppingCart> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<ShoppingCartVO> findPage(ShoppingCartQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return ShoppingCartVO
     */
    ShoppingCartVO findId(String id);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String insert(ShoppingCartDTO dto);

    /**
     * id 编辑
     *
     * @param id id
     * @param dto dto
     * @return boolean
     */
    boolean upd( String id, ShoppingCartDTO dto);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean del(String id);

}

