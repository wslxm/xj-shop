package com.xj.shop.manage.all.model.dto;

import io.github.wslxm.springbootplus2.core.base.model.Convert;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 订单表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderDTO 对象", description = "订单表")
public class OrderDTO extends Convert {

    private static final long serialVersionUID = -722372347985793024L;

    /**
     * 收货地址id 
     */
    @Length(min = 0, max = 32, message = "收货地址id 必须>=0 和 <=32位")
    @NotNull
    private String addressId;

    /**
     * 用户备注 (提交订单的备注) 
     */
    private String userRemark;

    /**
     * 商品规格
     */
    @NotNull(message = "没有选择商品")
    @Size(min = 1, message = "没有选择商品")
    private List<Specs> specses;

    /**
     * 商品规格
     */
    @Data
    public static class Specs {
        /**
         * 购买数量
         */
        private Integer num;

        /**
         * 商品规格id
         */
        @Length(min = 0, max = 32, message = "商品规格id 必须>=0 和 <=32位")
        private String goodsSpecsId;
    }
}

