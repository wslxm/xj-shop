package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.dto.OrderLogDTO;
import com.xj.shop.manage.all.model.entity.OrderLog;
import com.xj.shop.manage.all.model.query.OrderLogQuery;
import com.xj.shop.manage.all.model.vo.OrderLogVO;

import java.util.List;

/**
 * 订单变化相关记录表 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
public interface OrderLogService extends IService<OrderLog> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<OrderLogVO> findPage(OrderLogQuery query);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String insert(OrderLogDTO dto);


    /**
     * 添加（批量）
     *
     * @param dto dto
     * @return 主键id
     */
    boolean insertBatch(List<OrderLogDTO> dtos);


}

