package com.xj.shop.manage.all.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.github.wslxm.springbootplus2.core.base.model.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 订单变化相关记录表 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderLogVO 对象", description = "订单变化相关记录表")
public class OrderLogVO extends BaseVo {

    private static final long serialVersionUID = -722372531926994944L;
    
    /** 
     * 订单id 
     */
    private String orderId;

    /** 
     * 操作类型 (字典code 1-用户下单,更多) 
     */
    private Integer operationType;

    /**
     * 操作人姓名 / 昵称 (操作姓名保存姓名,不存在存昵称
     */
    private String operationFullName;

    /**
     * 操作人类型
     */
    private Integer operationUserType;

    /** 
     * 操作类型名称 
     */
    private String operationName;

    /** 
     * 操作前状态 
     */
    private Integer frontState;

    /** 
     * 操作后状态 
     */
    private Integer afterState;

    /** 
     * 备注信息 
     */
    private String remarks;

    /** 
     * 扩展字段1 
     */
    private String ext1;

    /** 
     * 扩展字段2 
     */
    private String ext2;

    /** 
     * 扩展字段3 
     */
    private String ext3;

}

