package com.xj.shop.manage.all.model.vo;

import io.github.wslxm.springbootplus2.core.base.model.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商品表 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "GoodsVO 对象", description = "商品表")
public class GoodsVO extends BaseVo {

    private static final long serialVersionUID = -722371817309868032L;

    /**
     * 商品名称 
     */
    private String name;

    /**
     * 商品-封面图 
     */
    private String coverPic;

    /**
     * 商品-轮播图 
     */
    private String pics;

    /**
     * 商品详情 (富文本) 
     */
    private String details;

    /**
     * 是否包邮(通用字典 0-否 1-是) 
     */
    private Integer isMail;

    /**
     * 标签
     */
    private String labels;

    /**
     * 类型Ids 
     */
    private String categoryIds;

    /**
     * 类别1级 
     */
    private String categoryOne;

    /**
     * 类别1级 名称
     */
    private String categoryOneName;

    /**
     * 类别2级 
     */
    private String categoryTwo;

    /**
     * 类别2级  名称
     */
    private String categoryTwoName;

    /**
     * 类别3级 
     */
    private String categoryThree;

    /**
     * 类别3级 名称
     */
    private String categoryThreeName;

    /**
     * 状态
     */
    private Integer state;
    /**
     * 最低价
     */
    private BigDecimal minPrice;
    /**
     * 最搞价
     */
    private BigDecimal maxPrice;

    /**
     * 商品规格(id查询存在)
     */
    private List<GoodsSpecsVO> goodsSpecsList;
    /**
     * 销量
     */
    private Integer salesNum;

}

