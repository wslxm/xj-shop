package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.dto.GoodsSpecsDTO;
import com.xj.shop.manage.all.model.entity.GoodsSpecs;
import com.xj.shop.manage.all.model.query.GoodsSpecsQuery;
import com.xj.shop.manage.all.model.vo.GoodsSpecsVO;

import java.util.List;

/**
 * 商品表 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:43
 */
public interface GoodsSpecsService extends IService<GoodsSpecs> {



    /**
     * 商品id 查询
     *
     * @param goodsId goodsId
     * @return GoodsSpecsVO
     */
    List<GoodsSpecsVO> findByGoodsId(String goodsId);

    /**
     * 添加或更新商品规格（批量）
     *
     * @param dtos dto
     * @return 主键id
     */
    boolean updGoodsSpecsBatch(List<GoodsSpecsDTO> dtos,String goodsId);




}

