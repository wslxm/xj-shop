package com.xj.shop.manage.all.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xj.shop.manage.all.mapper.AddressMapper;
import com.xj.shop.manage.all.model.dto.AddressDTO;
import com.xj.shop.manage.all.model.entity.Address;
import com.xj.shop.manage.all.model.query.AddressQuery;
import com.xj.shop.manage.all.model.vo.AddressVO;
import com.xj.shop.manage.all.service.AddressService;
import io.github.wslxm.springbootplus2.common.auth.util.JwtUtil;
import io.github.wslxm.springbootplus2.core.base.service.impl.BaseServiceImpl;
import io.github.wslxm.springbootplus2.core.utils.BeanDtoVoUtil;
import io.github.wslxm.springbootplus2.core.utils.validated.ValidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 地址管理(收货) ServiceImpl
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
@Service
public class AddressServiceImpl extends BaseServiceImpl<AddressMapper, Address> implements AddressService {

    @Override
    public IPage<AddressVO> findPage(AddressQuery query) {
        LambdaQueryWrapper<Address> queryWrapper = new LambdaQueryWrapper<Address>()
                .eq(query.getIsDefault() != null, Address::getIsDefault, query.getIsDefault())
                .eq(query.getUserId() != null, Address::getUserId, query.getUserId())
                .orderByDesc(Address::getCreateTime);
        Page<Address> page = this.page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);
        return BeanDtoVoUtil.pageVo(page, AddressVO.class);
    }

    @Override
    public AddressVO findId(String id) {
        return BeanDtoVoUtil.convert(this.getById(id), AddressVO.class);
    }

    @Override
    public String insert(AddressDTO dto) {
        // 不能超过10个地址
        long count = this.count(new LambdaQueryWrapper<Address>().eq(Address::getUserId, dto.getUserId()));
        ValidUtil.isTrue(count >= 10, "地址管理不能超过地址上限10");
        // 保存
        Address entity = dto.convert(Address.class);
        boolean b = this.save(entity);

        // 处理默认地址
        if (dto.getIsDefault()) {
            this.updIsDefault(entity.getId());
        }

        return entity.getId();
    }

    @Override
    public boolean upd(String id, AddressDTO dto) {
        Address entity = dto.convert(Address.class);
        entity.setId(id);
        this.updateById(entity);

        // 设置默认地址
        if (dto.getIsDefault()) {
            this.updIsDefault(id);
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updIsDefault(String id) {
        String userId = JwtUtil.getJwtUser(request).getUserId();

        // 判断当前用户是否地址id是否正常
        long count = this.count(new LambdaQueryWrapper<Address>()
                .eq(Address::getUserId, userId)
                .eq(Address::getId, id)
        );
        ValidUtil.isTrue(count == 0, "没有找到该用户的改地址数据,请检查数据");

        // 清除当前用户之前的默认地址
        this.update(new Address(), new LambdaUpdateWrapper<Address>()
                .set(Address::getIsDefault, false)
                .eq(Address::getUserId, userId)
                .eq(Address::getIsDefault, true)
        );

        // 设置新的默认地址
        Address entity = new Address();
        entity.setId(id);
        entity.setIsDefault(true);
        return this.updateById(entity);
    }

    @Override
    public boolean del(String id) {
        return this.removeById(id);
    }

}
