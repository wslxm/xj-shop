package com.xj.shop.manage.all.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseDto;

/**
 * 我的收藏 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:52:04
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "CollectionDTO 对象", description = "我的收藏")
public class CollectionDTO extends BaseDto {

    private static final long serialVersionUID = -722371197672755200L;
    
    /** 
     * 用户id 
     */
    private Integer userId;

    /** 
     * 商品Id 
     */
    private String goodsId;

    /** 
     * 商品规格Id 
     */
    private String goodsSpecsId;

    /** 
     * 商品详细快照 (json) 
     */
    private String goodsDetails;

}

