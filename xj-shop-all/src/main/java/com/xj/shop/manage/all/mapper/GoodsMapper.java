package com.xj.shop.manage.all.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.entity.Goods;
import com.xj.shop.manage.all.model.vo.GoodsVO;
import com.xj.shop.manage.all.model.query.GoodsQuery;

import java.util.List;

/**
 * 商品表 Mapper
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    /**
     * id 查询
     *
     * @param id
     * @return List<Goods>
     */
    GoodsVO findId(String id);

    /**
     * 列表查询/分页查询
     *
     * @param page
     * @param query
     * @return java.util.List<Goods>
     */
    List<GoodsVO> list(IPage<GoodsVO> page, GoodsQuery query);

}

