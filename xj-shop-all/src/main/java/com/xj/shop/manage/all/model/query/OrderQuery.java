package com.xj.shop.manage.all.model.query;

import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

/**
 * 订单表 Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderQuery 对象", description = "订单表")
public class OrderQuery extends BaseQuery {

    private static final long serialVersionUID = -722372348472332288L;
    
    /** 
     * 用户姓名/昵称
     */
    @Length(min=0, max=32,message = "用户姓名/昵称/手机号 必须>=0 和 <=32位")
    private String keyword;


    /**
     * 用户Id
     */
    @Length(min=0, max=32,message = "商品id 必须>=0 和 <=32位")
    private String userId;

    /**
     * 订单号
     */
    @Length(min=0, max=32,message = "商品id 必须>=0 和 <=32位")
    private String orderNo;


    /** 
     * 商品id 
     */
    @Length(min=0, max=32,message = "商品id 必须>=0 和 <=32位")
    private String goodsId;

    /** 
     * 订单状态 (1-已下单 2-已支付 3-已发货  4-已完成  5-用户取消 6-平台拒单  7-支付失败  8-订单超时) 
     */
    @Range(min=0, max=9L,message = "订单状态  必须>=0 和 <=9")
    private Integer state;

}

