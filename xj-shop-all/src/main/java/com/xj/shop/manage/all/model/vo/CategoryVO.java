package com.xj.shop.manage.all.model.vo;

import io.github.wslxm.springbootplus2.core.base.model.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 类别表 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "CategoryVO 对象", description = "类别表")
public class CategoryVO extends BaseVo {

    private static final long serialVersionUID = -722317156175450112L;
    
    /** 
     * 父Id (顶级父id=0) 
     */
    private String pid;

    /** 
     * 分类code (不能重复) 
     */
    private String code;

    /** 
     * 分类名称 
     */
    private String name;

    /** 
     * 分类描叙 
     */
    private String desc;

    /** 
     * 排序 
     */
    private Integer sort;

    /** 
     * 禁用(通用字典 0-否 1-是) 
     */
    private Integer disable;

    /** 
     * 级别 (对应123  1-一级 2-二级 3-三级) 
     */
    private Integer root;

    /**
     * 下级数据
     */
    private List<CategoryVO> children;

}

