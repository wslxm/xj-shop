package com.xj.shop.manage.all.model.dto;


import io.github.wslxm.springbootplus2.core.base.model.Convert;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 订单价格计算
 *
 * @author wangsong
 * @email 1720696548@qq.com
 * @date 2022/8/26 11:19
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderPriceCalculationDTO 对象", description = "订单价格计算")
public class OrderPriceCalculationDTO extends Convert {


	/**
	 * 规格id
	 */
	@NotBlank(message = "商品规格id不能为空")
	private String goodsSpecsId;
	/**
	 * 商品数量
	 */
	@NotNull(message = "数量不能为空")
	@Range(min = 1,max = 999999999,message = "商品数量不能小于1")
	private Integer num;

}
