package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.dto.CategoryDTO;
import com.xj.shop.manage.all.model.entity.Category;
import com.xj.shop.manage.all.model.query.CategoryQuery;
import com.xj.shop.manage.all.model.vo.CategoryVO;

import java.util.List;

/**
 * 类别表 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
public interface CategoryService extends IService<Category> {

    /**
     * 树结构数据查询
     *
     * @param query query
     * @return 分页列表数据
     */
    List<CategoryVO> tree(CategoryQuery query);

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<CategoryVO> findPage(CategoryQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return CategoryVO
     */
    CategoryVO findId(String id);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String insert(CategoryDTO dto);

    /**
     * id 编辑
     *
     * @param id id
     * @param dto dto
     * @return boolean
     */
    boolean upd( String id, CategoryDTO dto);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean del(String id);

}

