package com.xj.shop.manage.all.model.vo;

import io.github.wslxm.springbootplus2.core.base.model.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 订单表 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderVO 对象", description = "订单表")
public class OrderVO extends BaseVo {

    private static final long serialVersionUID = -722372348866596864L;

    /**
     * 用户id 
     */
    private String userId;

    /**
     * 单价 
     */
    private BigDecimal unitPrice;

    /**
     * 购买数量 
     */
    private Integer num;

    /**
     * 总价 
     */
    private BigDecimal totalPrice;

    /**
     * 订单号 
     */
    private String orderNo;

    /**
     * 交易号
     */
    private String payNo;

    /**
     * 商品id 
     */
    private String goodsId;

    /**
     * 商品规格id 
     */
    private String goodsSpecsId;

    /**
     * 收货地址id 
     */
    private String addressId;

    /**
     * 商品详情-快照 (json数据 订单查询商品使用) 
     */
    private String goodsDetails;

    /**
     * 收货地址详情-快照 (json数据 订单查询商品使用) 
     */
    private String addressDetails;

    /**
     * 订单状态 (1-已下单 2-已支付 3-已发货  4-已完成  5-用户取消 6-平台拒单  7-支付失败  8-订单超时) 
     */
    private Integer state;

    /**
     * 物流单号 
     */
    private String logisticsNo;

    /**
     * 用户备注 (提交订单的备注) 
     */
    private String userRemark;

    /**
     * 平台备注 (订单异常下的备注) 
     */
    private String platformRemark;

    /**
     * 支付时间 
     */
    private LocalDateTime payTime;

    /**
     * 发货时间 
     */
    private LocalDateTime deliverTime;

    /**
     * 完成时间 
     */
    private LocalDateTime successTime;

    /**
     * 用户姓名
     */
    private String fullName;
    /**
     * 用户昵称
     */
    private String nickname;
    /**
     * 用户电话
     */
    private String phone;


}

