package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 订单变化相关记录表 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_order_log")
@ApiModel(value = "OrderLog 对象", description = "订单变化相关记录表")
public class OrderLog extends BaseEntity {

    private static final long serialVersionUID = -722372531121688576L;

    /**
     * 订单id 
     */
    @TableField(value = "order_id")
    private String orderId;

    /**
     * 操作类型 (字典code 1-用户下单,更多) 
     */
    @TableField(value = "operation_type")
    private Integer operationType;

    /**
     * 操作人姓名 / 昵称 (操作姓名保存姓名,不存在存昵称
     */
    @TableField(value = "operation_full_name")
    private String operationFullName;

    /**
     * 操作人类型
     */
    @TableField(value = "operation_user_type")
    private Integer operationUserType;

    /**
     * 操作类型名称 
     */
    @TableField(value = "operation_name")
    private String operationName;

    /**
     * 操作前状态 
     */
    @TableField(value = "front_state")
    private Integer frontState;

    /**
     * 操作后状态 
     */
    @TableField(value = "after_state")
    private Integer afterState;

    /**
     * 备注信息 
     */
    @TableField(value = "remarks")
    private String remarks;

    /**
     * 扩展字段1 
     */
    @TableField(value = "ext1")
    private String ext1;

    /**
     * 扩展字段2 
     */
    @TableField(value = "ext2")
    private String ext2;

    /**
     * 扩展字段3 
     */
    @TableField(value = "ext3")
    private String ext3;

}

