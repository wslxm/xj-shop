package com.xj.shop.manage.all.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseVo;

/**
 * 工具--用户表 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 11:17:09
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "UserVO 对象", description = "工具--用户表")
public class UserVO extends BaseVo {

    private static final long serialVersionUID = -722256712224083968L;
    
    /** 
     * 账号/用户名 
     */
    private String username;

    /** 
     * 邮箱地址 
     */
    private String email;

    /** 
     * 密码 
     */
    private String password;

    /** 
     * 性别 (0-未知 1-男 2-女) 
     */
    private Integer gender;

    /** 
     * 头像 
     */
    private String headPic;

    /** 
     * 手机号 
     */
    private String phone;

    /** 
     * 昵称 
     */
    private String nickname;

    /** 
     * 姓名 
     */
    private String fullName;

    /** 
     * 地址 
     */
    private String address;

    /** 
     * 年龄 
     */
    private Integer age;

    /** 
     * 注册时间 
     */
    private LocalDateTime regTime;

    /** 
     * 最后登录时间 
     */
    private LocalDateTime endTime;

    /** 
     * 是否禁用 (通用字典 0-否，1-是) 
     */
    private Integer disable;

}

