//package com.xj.shop.manage.all.controller;
//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//import io.swagger.annotations.*;
//import com.xj.shop.manage.all.model.vo.ShoppingCartVO;
//import com.xj.shop.manage.all.model.dto.ShoppingCartDTO;
//import com.xj.shop.manage.all.model.query.ShoppingCartQuery;
//import com.xj.shop.manage.all.service.ShoppingCartService;
//import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
//import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
//import io.github.wslxm.springbootplus2.core.result.Result;
//
///**
// * 购物车 前端控制器
// *
// * <p>
// * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
// * </p>
// *
// * @author ws
// * @email 1720696548@qq.com
// * @date 2022-08-24 18:57:35
// */
//@RestController
//@RequestMapping(BaseConstant.Uri.API_ADMIN + "/all/shoppingCart")
//@Api(value = "ShoppingCartController", tags = "购物车")
//public class ShoppingCartController extends BaseController<ShoppingCartService> {
//
//
//}
