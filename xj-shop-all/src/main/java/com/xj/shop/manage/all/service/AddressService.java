package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.dto.AddressDTO;
import com.xj.shop.manage.all.model.entity.Address;
import com.xj.shop.manage.all.model.query.AddressQuery;
import com.xj.shop.manage.all.model.vo.AddressVO;

/**
 * 地址管理(收货) Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
public interface AddressService extends IService<Address> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<AddressVO> findPage(AddressQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return AddressVO
     */
    AddressVO findId(String id);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String insert(AddressDTO dto);

    /**
     * id 编辑
     *
     * @param id id
     * @param dto dto
     * @return boolean
     */
    boolean upd( String id, AddressDTO dto);


    /**
     * 编辑 为默认地址
     *
     * @param id
     * @return
     */
    boolean updIsDefault(String id);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean del(String id);

}

