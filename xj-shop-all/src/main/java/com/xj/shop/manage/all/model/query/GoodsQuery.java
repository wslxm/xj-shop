package com.xj.shop.manage.all.model.query;

import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 商品表 Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "GoodsQuery 对象", description = "商品表")
public class GoodsQuery extends BaseQuery {

    private static final long serialVersionUID = -722371816835911680L;

    /**
     * 搜索 商品名模糊 + 标签模糊  2级+3级分类 搜索
     */
    private String keyword;

    /**
     * 商品名称 
     */
    private String name;

    /**
     * 商品状态
     */
    private Integer state;

    /**
     * 价格区间 小
     */
    private String minPrice;

    /**
     * 价格区间 大
     */
    private String maxPrice;

    /**
     * 分类 ids
     */
    private String categoryIds;

    /**
     * 三级分类id
     */
    private String categoryThree;


}

