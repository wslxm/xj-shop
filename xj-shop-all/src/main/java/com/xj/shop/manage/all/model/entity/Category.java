package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;

/**
 * 类别表 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_category")
@ApiModel(value = "Category 对象", description = "类别表")
public class Category extends BaseEntity {

    private static final long serialVersionUID = -722317155290451968L;
    
    /** 
     * 父Id (顶级父id=0) 
     */
    @TableField(value = "pid")
    private String pid;

    /** 
     * 分类code (不能重复) 
     */
    @TableField(value = "code")
    private String code;

    /** 
     * 分类名称 
     */
    @TableField(value = "`name`")
    private String name;

    /** 
     * 分类描叙 
     */
    @TableField(value = "`desc`")
    private String desc;

    /** 
     * 排序 
     */
    @TableField(value = "`sort`")
    private Integer sort;

    /** 
     * 禁用(通用字典 0-否 1-是) 
     */
    @TableField(value = "`disable`")
    private Integer disable;

    /** 
     * 级别 (对应123  1-一级 2-二级 3-三级) 
     */
    @TableField(value = "root")
    private Integer root;

}

