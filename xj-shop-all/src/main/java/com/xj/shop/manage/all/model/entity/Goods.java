package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 商品表 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_goods")
@ApiModel(value = "Goods 对象", description = "商品表")
public class Goods extends BaseEntity {

    private static final long serialVersionUID = -722371816437452800L;

    /**
     * 商品名称 
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 商品-封面图 
     */
    @TableField(value = "cover_pic")
    private String coverPic;

    /**
     * 商品-轮播图 
     */
    @TableField(value = "pics")
    private String pics;

    /**
     * 标签
     */
    @TableField(value = "labels")
    private String labels;

    /**
     * 商品详情 (富文本) 
     */
    @TableField(value = "details")
    private String details;

    /**
     * 是否包邮(通用字典 0-否 1-是) 
     */
    @TableField(value = "is_mail")
    private Integer isMail;

    /**
     * 类型Ids 
     */
    @TableField(value = "category_ids")
    private String categoryIds;

    /**
     * 类别1级 
     */
    @TableField(value = "category_one")
    private String categoryOne;

    /**
     * 类别2级 
     */
    @TableField(value = "category_two")
    private String categoryTwo;

    /**
     * 类别3级 
     */
    @TableField(value = "category_three")
    private String categoryThree;


    /**
     * 商品状态 (1-已上架 0-下架中)
     */
    @TableField(value = "state")
    private Integer state;

    /**
     * 最低价
     */
    @TableField(value = "min_price")
    private BigDecimal minPrice;

    /**
     * 最高价
     */
    @TableField(value = "max_price")
    private BigDecimal maxPrice;

}

