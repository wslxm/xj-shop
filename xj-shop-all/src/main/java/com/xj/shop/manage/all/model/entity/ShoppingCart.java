package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 购物车 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_shopping_cart")
@ApiModel(value = "ShoppingCart 对象", description = "购物车")
public class ShoppingCart extends BaseEntity {

    private static final long serialVersionUID = -722372584850722816L;
    
    /** 
     * 用户id 
     */
    @TableField(value = "user_id")
    private String userId;

    /** 
     * 商品Id 
     */
    @TableField(value = "goods_id")
    private String goodsId;

    /**
     * 规格id
     */
    private String goodsSpecsId;

    /**
     * 数量
     */
    private Integer num;
}

