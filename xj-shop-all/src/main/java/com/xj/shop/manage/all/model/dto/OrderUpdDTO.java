package com.xj.shop.manage.all.model.dto;

import io.github.wslxm.springbootplus2.core.base.model.Convert;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 订单表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderUpdDTO 对象", description = "订单表")
public class OrderUpdDTO extends Convert {

    private static final long serialVersionUID = -722372347985793024L;
    /**
     * 平台备注 (订单异常下的备注)
     */
    private String platformRemark;

    /**
     * 物流单号
     */
    private String logisticsNo;
}

