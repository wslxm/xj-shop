package com.xj.shop.manage.all.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.github.wslxm.springbootplus2.core.base.model.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 商品表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "GoodsDTO 对象", description = "商品表")
public class GoodsDTO extends BaseDto {

    private static final long serialVersionUID = -722371816307429376L;
    
    /** 
     * 商品名称 
     */
    @NotNull(message = "商品名称不能为空")
    private String name;

    /** 
     * 商品-封面图 
     */
    private String coverPic;

    /** 
     * 商品-轮播图 
     */
    private String pics;

    /**
     * 标签
     */
    private String labels;

    /** 
     * 商品详情 (富文本) 
     */
    private String details;

    /** 
     * 是否包邮(通用字典 0-否 1-是) 
     */
    private Integer isMail;

    /** 
     * 类型Ids 
     */
    private String categoryIds;


    /**
     * 规格信息
     */
    @Valid
    @Size(min = 1,message = "必须存在1条规格数据")
    @NotNull(message = "必须存在1条规格数据")
    private List<GoodsSpecsDTO> goodsSpecsList;

}

