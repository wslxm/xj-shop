package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;

/**
 * 工具--用户表 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 11:17:09
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_user")
@ApiModel(value = "User 对象", description = "工具--用户表")
public class User extends BaseEntity {

    private static final long serialVersionUID = -722256711422971904L;
    
    /** 
     * 账号/用户名 
     */
    @TableField(value = "username")
    private String username;

    /** 
     * 邮箱地址 
     */
    @TableField(value = "email")
    private String email;

    /** 
     * 密码 
     */
    @TableField(value = "password")
    private String password;

    /** 
     * 性别 (0-未知 1-男 2-女) 
     */
    @TableField(value = "gender")
    private Integer gender;

    /** 
     * 头像 
     */
    @TableField(value = "head_pic")
    private String headPic;

    /** 
     * 手机号 
     */
    @TableField(value = "phone")
    private String phone;

    /** 
     * 昵称 
     */
    @TableField(value = "nickname")
    private String nickname;

    /** 
     * 姓名 
     */
    @TableField(value = "full_name")
    private String fullName;

    /** 
     * 地址 
     */
    @TableField(value = "address")
    private String address;

    /** 
     * 年龄 
     */
    @TableField(value = "age")
    private Integer age;

    /** 
     * 注册时间 
     */
    @TableField(value = "reg_time")
    private LocalDateTime regTime;

    /** 
     * 最后登录时间 
     */
    @TableField(value = "end_time")
    private LocalDateTime endTime;

    /** 
     * 是否禁用 (通用字典 0-否，1-是) 
     */
    @TableField(value = "`disable`")
    private Integer disable;

}

