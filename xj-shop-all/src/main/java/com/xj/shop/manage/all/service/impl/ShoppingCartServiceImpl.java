package com.xj.shop.manage.all.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xj.shop.enums.Shop;
import com.xj.shop.manage.all.mapper.ShoppingCartMapper;
import com.xj.shop.manage.all.model.dto.ShoppingCartDTO;
import com.xj.shop.manage.all.model.entity.Goods;
import com.xj.shop.manage.all.model.entity.GoodsSpecs;
import com.xj.shop.manage.all.model.entity.ShoppingCart;
import com.xj.shop.manage.all.model.query.ShoppingCartQuery;
import com.xj.shop.manage.all.model.vo.ShoppingCartVO;
import com.xj.shop.manage.all.service.GoodsService;
import com.xj.shop.manage.all.service.GoodsSpecsService;
import com.xj.shop.manage.all.service.ShoppingCartService;
import io.github.wslxm.springbootplus2.common.auth.util.JwtUtil;
import io.github.wslxm.springbootplus2.core.base.service.impl.BaseServiceImpl;
import io.github.wslxm.springbootplus2.core.utils.BeanDtoVoUtil;
import io.github.wslxm.springbootplus2.core.utils.validated.ValidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 购物车 ServiceImpl
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
@Service
public class ShoppingCartServiceImpl extends BaseServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

    @Autowired
    private GoodsSpecsService goodsSpecsService;
    @Autowired
    private GoodsService goodsService;

    @Override
    public IPage<ShoppingCartVO> findPage(ShoppingCartQuery query) {
        query.setUserId(JwtUtil.getJwtUser(request).getUserId());
        Page<ShoppingCartVO> page = new Page<>(query.getCurrent(), query.getSize());
        List<ShoppingCartVO> list = baseMapper.list(page, query);
        page.setRecords(list);
        return page;
    }

    @Override
    public ShoppingCartVO findId(String id) {
        return BeanDtoVoUtil.convert(this.getById(id), ShoppingCartVO.class);
    }

    @Override
    public String insert(ShoppingCartDTO dto) {
        // 判断规格
        GoodsSpecs goodsSpecs = goodsSpecsService.getById(dto.getGoodsSpecsId());
        ValidUtil.isTrue(goodsSpecs == null, "没有获取到商品规格数据");

        // 判断是否已加入购物车
        long count = this.count(new LambdaQueryWrapper<ShoppingCart>().eq(ShoppingCart::getGoodsSpecsId, dto.getGoodsSpecsId()));
        ValidUtil.isTrue(count > 0, "商品重复加入购物车");

        // 验证商品
        Goods goods = goodsService.getOne(new LambdaQueryWrapper<Goods>()
                .select(Goods.class, info -> !"details".equals(info.getColumn()))
                .eq(Goods::getId, goodsSpecs.getGoodsId()));
        ValidUtil.isTrue(goods == null, "没有获取到商品数据");
        ValidUtil.isTrue(goods.getState().equals(Shop.GoodsState.V0.getValue()), "商品已下架");

        ShoppingCart entity = dto.convert(ShoppingCart.class);
        entity.setGoodsId(goods.getId());
        entity.setUserId(JwtUtil.getJwtUser(request).getUserId());
        boolean b = this.save(entity);
        return entity.getId();
    }

    @Override
    public boolean upd(String id, ShoppingCartDTO dto) {
        ShoppingCart entity = dto.convert(ShoppingCart.class);
        entity.setId(id);
        return this.updateById(entity);
    }

    @Override
    public boolean del(String id) {
        return this.removeById(id);
    }
}
