package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 商品表 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:43
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_goods_specs")
@ApiModel(value = "GoodsSpecs 对象", description = "商品表")
public class GoodsSpecs extends BaseEntity {

    private static final long serialVersionUID = -722371863799533568L;

    /**
     * 商品id 
     */
    @TableField(value = "goods_id")
    private String goodsId;

    /**
     * 商品规格名 
     */
    @TableField(value = "specs_name")
    private String specsName;

    /**
     * 商品规格价格 
     */
    @TableField(value = "specs_price")
    private BigDecimal specsPrice;

    /**
     * 商品库存 
     */
    @TableField(value = "specs_stock")
    private Integer specsStock;

    /**
     * 排序
     */
    @TableField(value = "sort")
    private Integer sort;

}

