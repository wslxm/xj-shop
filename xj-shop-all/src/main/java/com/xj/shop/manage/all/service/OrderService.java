package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.dto.OrderDTO;
import com.xj.shop.manage.all.model.dto.OrderPriceCalculationDTO;
import com.xj.shop.manage.all.model.dto.OrderUpdDTO;
import com.xj.shop.manage.all.model.entity.Order;
import com.xj.shop.manage.all.model.query.OrderQuery;
import com.xj.shop.manage.all.model.vo.OrderPriceCalculationVO;
import com.xj.shop.manage.all.model.vo.OrderVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单表 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
public interface OrderService extends IService<Order> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<OrderVO> findPage(OrderQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return OrderVO
     */
    OrderVO findId(String id);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String createOrder(OrderDTO dto);

    /**
     * id 编辑
     *
     * @param id id
     * @param dto dto
     * @return boolean
     */
    boolean upd(String id, OrderUpdDTO dto);


    /**
     * 发货
     * @param id
     * @param logisticsNo
     * @return
     */
    boolean deliverGoods(String id, String logisticsNo);

    /**
     * 改价
     *
     * @param id         id
     * @param totalPrice 改后的总价
     * @return boolean
     */
    boolean updTotalPrice(String id, BigDecimal totalPrice);


    /**
     * 状态变动
     *
     * @param id
     * @param state
     * @return
     */
    boolean updState(String id, Integer state);

    /**
     * 支付
     *
     * @param id
     * @return
     */
    boolean pay(String payNo);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean del(String id);

    /**
     * 商品价格计算
     *
     * @param dtos
     * @return
     */
    OrderPriceCalculationVO priceCalculation(List<OrderPriceCalculationDTO> dtos);

}

