package com.xj.shop.manage.all.model.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 * 价格计算返回结果
 *
 * @author wangsong
 * @email 1720696548@qq.com
 * @date 2022/8/26 11:26
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderPriceCalculationVO 对象", description = "价格计算")
public class OrderPriceCalculationVO {


    /**
     * 所有规格的总价
     */
    private BigDecimal totalPrice;

    /**
     * 每个规格的价格
     */
    private List<SpecsPriceVO> specsPriceVos;


    @Data
    public static class SpecsPriceVO {
        /**
         * 规格id
         */
        private String goodsSpecsId;
        /**
         * 该规格 单价
         */
        private BigDecimal specsPrice;

        /**
         * 该规格 总价
         */
        private BigDecimal totalPrice;
        /**
         * 数量
         */
        private Integer num;

        /**
         * 商品名称
         */
        private String name;

        /**
         * 规格名称
         */
        private String specsName;

        /**
         * 商品图片
         */
        private String coverPic;

    }
}
