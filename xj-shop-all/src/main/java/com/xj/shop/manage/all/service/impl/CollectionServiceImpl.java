package com.xj.shop.manage.all.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xj.shop.enums.Shop;
import com.xj.shop.manage.all.mapper.CollectionMapper;
import com.xj.shop.manage.all.model.entity.Collection;
import com.xj.shop.manage.all.model.entity.Goods;
import com.xj.shop.manage.all.model.query.CollectionQuery;
import com.xj.shop.manage.all.model.vo.CollectionVO;
import com.xj.shop.manage.all.model.vo.GoodsVO;
import com.xj.shop.manage.all.service.CollectionService;
import com.xj.shop.manage.all.service.GoodsService;
import io.github.wslxm.springbootplus2.common.auth.entity.JwtUser;
import io.github.wslxm.springbootplus2.common.auth.util.JwtUtil;
import io.github.wslxm.springbootplus2.core.base.service.impl.BaseServiceImpl;
import io.github.wslxm.springbootplus2.core.utils.BeanDtoVoUtil;
import io.github.wslxm.springbootplus2.core.utils.validated.ValidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




/**
 * 我的收藏 ServiceImpl
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:52:04
 */
@Service
public class CollectionServiceImpl extends BaseServiceImpl<CollectionMapper, Collection> implements CollectionService {

    @Autowired
    private GoodsService goodsService;

    @Override
    public IPage<CollectionVO> findPage(CollectionQuery query) {
        LambdaQueryWrapper<Collection> queryWrapper = new LambdaQueryWrapper<Collection>()
                .eq(query.getUserId() != null, Collection::getUserId, query.getUserId())
                .orderByDesc(Collection::getCreateTime);
        Page<Collection> page = this.page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);
        return BeanDtoVoUtil.pageVo(page, CollectionVO.class);
    }

    @Override
    public CollectionVO findId(String id) {
        return BeanDtoVoUtil.convert(this.getById(id), CollectionVO.class);
    }


    @Override
    public boolean collect(String id, String goodsId) {
        JwtUser jwtUser = JwtUtil.getJwtUser(request);

        // 验证商品 排除详情
        Goods goods = goodsService.getOne(new LambdaQueryWrapper<Goods>()
                .select(Goods.class, info -> !"details".equals(info.getColumn()))
                .eq(Goods::getId, goodsId));
        ValidUtil.isTrue(goods == null, "没有获取到商品数据");
        ValidUtil.isTrue(goods.getState().equals(Shop.GoodsState.V0.getValue()), "商品已下架");

        // 我的收藏
        Collection collection = this.getById(id);
        collection.setUserId(jwtUser.getUserId());
        collection.setGoodsId(goodsId);
        collection.setGoodsDetails(JSON.toJSONString(BeanDtoVoUtil.convert(goods, GoodsVO.class)));
        return this.save(collection);
    }
}
