package com.xj.shop.manage.all.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;

/**
 * 订单变化相关记录表 Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderLogQuery 对象", description = "订单变化相关记录表")
public class OrderLogQuery extends BaseQuery {

    private static final long serialVersionUID = -722372531515953152L;
    
    /** 
     * 订单id 
     */
    @Length(min=0, max=32,message = "订单id 必须>=0 和 <=32位")
    private String orderId;

}

