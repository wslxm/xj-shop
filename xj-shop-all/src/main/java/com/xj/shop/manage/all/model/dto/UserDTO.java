package com.xj.shop.manage.all.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseDto;

/**
 * 工具--用户表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 11:17:09
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "UserDTO 对象", description = "工具--用户表")
public class UserDTO extends BaseDto {

    private static final long serialVersionUID = -722256711242616832L;
    
    /** 
     * 账号/用户名 
     */
    @Length(min=0, max=32,message = "账号/用户名 必须>=0 和 <=32位")
    private String username;

    /** 
     * 邮箱地址 
     */
    private String email;

    /** 
     * 密码 
     */
    @Length(min=0, max=32,message = "密码 必须>=0 和 <=32位")
    private String password;

    /** 
     * 性别 (0-未知 1-男 2-女) 
     */
    @Range(min=0, max=9L,message = "性别  必须>=0 和 <=9")
    private Integer gender;

    /** 
     * 头像 
     */
    private String headPic;

    /** 
     * 手机号 
     */
    private String phone;

    /** 
     * 昵称 
     */
    private String nickname;

    /** 
     * 姓名 
     */
    private String fullName;

    /** 
     * 地址 
     */
    private String address;

    /** 
     * 年龄 
     */
    private Integer age;

    /** 
     * 注册时间 
     */
    private LocalDateTime regTime;

    /** 
     * 最后登录时间 
     */
    private LocalDateTime endTime;

    /** 
     * 是否禁用 (通用字典 0-否，1-是) 
     */
    @Range(min=0, max=9L,message = "是否禁用  必须>=0 和 <=9")
    private Integer disable;

}

