package com.xj.shop.manage.all.model.vo;

import io.github.wslxm.springbootplus2.core.base.model.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 购物车 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "ShoppingCartVO 对象", description = "购物车")
public class ShoppingCartVO extends BaseVo {

    private static final long serialVersionUID = -722372585626669056L;
    
    /** 
     * 用户id 
     */
    private String userId;

    /** 
     * 商品Id 
     */
    private String goodsId;

    /**
     * 规格id
     */
    private String goodsSpecsId;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品图片
     */
    private String coverPic;
    /**
     * 商品规格名称
     */
    private String goodsSpecsName;

    /**
     * 商品规格单价
     */
    private String specsPrice;
}

