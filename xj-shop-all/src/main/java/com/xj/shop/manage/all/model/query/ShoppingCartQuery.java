package com.xj.shop.manage.all.model.query;

import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 购物车 Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "ShoppingCartQuery 对象", description = "购物车")
public class ShoppingCartQuery extends BaseQuery {

    private static final long serialVersionUID = -722372585240793088L;

    /**
     * 用户id
     */
    private String userId;

}

