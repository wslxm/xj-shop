package com.xj.shop.manage.all.model.query;

import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 地址管理(收货) Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "AddressQuery 对象", description = "地址管理(收货)")
public class AddressQuery extends BaseQuery {

    private static final long serialVersionUID = -722371073412304896L;

    /**
     * 是否默认地址
     */
    private Integer isDefault;

    /**
     * 用户id
     */
    private String userId;
}

