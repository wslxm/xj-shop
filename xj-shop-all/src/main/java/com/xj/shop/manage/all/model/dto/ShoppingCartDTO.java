package com.xj.shop.manage.all.model.dto;

import io.github.wslxm.springbootplus2.core.base.model.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 购物车 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:35
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "ShoppingCartDTO 对象", description = "购物车")
public class ShoppingCartDTO extends BaseDto {

    private static final long serialVersionUID = -722372584716505088L;


    /**
     * 规格Id
     */
    private String goodsSpecsId;

    /**
     * 数量
     */
    private Integer num;

}

