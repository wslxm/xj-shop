package com.xj.shop.manage.all.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseVo;

/**
 * 商品表 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:43
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "GoodsSpecsVO 对象", description = "商品表")
public class GoodsSpecsVO extends BaseVo {

    private static final long serialVersionUID = -722371864609034240L;
    
    /** 
     * 商品id 
     */
    private String goodsId;

    /** 
     * 商品规格名 
     */
    private String specsName;

    /** 
     * 商品规格价格 
     */
    private BigDecimal specsPrice;

    /** 
     * 商品库存 
     */
    private Integer specsStock;

    /**
     * 商品库存
     */
    private Integer sort;
}

