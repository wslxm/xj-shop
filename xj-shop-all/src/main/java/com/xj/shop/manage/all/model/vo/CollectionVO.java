package com.xj.shop.manage.all.model.vo;

import io.github.wslxm.springbootplus2.core.base.model.BaseVo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

/**
 * 我的收藏 VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:52:04
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "CollectionVO 对象", description = "我的收藏")
public class CollectionVO extends BaseVo {

    private static final long serialVersionUID = -722371198561947648L;
    
    /** 
     * 用户id 
     */
    private Integer userId;

    /** 
     * 商品Id 
     */
    private String goodsId;


    /** 
     * 商品详细快照 (json) 
     */
    private String goodsDetails;

}

