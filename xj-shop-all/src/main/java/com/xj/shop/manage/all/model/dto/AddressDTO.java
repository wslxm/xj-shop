package com.xj.shop.manage.all.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseDto;

/**
 * 地址管理(收货) DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "AddressDTO 对象", description = "地址管理(收货)")
public class AddressDTO extends BaseDto {

    private static final long serialVersionUID = -722371072804130816L;
    
    /** 
     * 用户id 
     */
    @Length(min=0, max=32,message = "用户id 必须>=0 和 <=32位")
    private String userId;

    /** 
     * 姓名 
     */
    @Length(min=0, max=32,message = "姓名 必须>=0 和 <=32位")
    private String fullName;

    /** 
     * 电话 
     */
    @Length(min=0, max=20,message = "电话 必须>=0 和 <=20位")
    private String phone;

    /** 
     * 省 
     */
    @Length(min=0, max=32,message = "省 必须>=0 和 <=32位")
    private String province;

    /** 
     * 市 
     */
    @Length(min=0, max=32,message = "市 必须>=0 和 <=32位")
    private String city;

    /** 
     * 区 
     */
    @Length(min=0, max=32,message = "区 必须>=0 和 <=32位")
    private String area;

    /** 
     * 详细地址 
     */
    @Length(min=0, max=256,message = "详细地址 必须>=0 和 <=256位")
    private String specificAddress;

    /** 
     * 是否默认地址 
     */
    private Boolean isDefault;

}

