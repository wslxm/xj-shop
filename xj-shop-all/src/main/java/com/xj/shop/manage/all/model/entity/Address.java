package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;

/**
 * 地址管理(收货) Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_address")
@ApiModel(value = "Address 对象", description = "地址管理(收货)")
public class Address extends BaseEntity {

    private static final long serialVersionUID = -722371072963514368L;
    
    /** 
     * 用户id 
     */
    @TableField(value = "user_id")
    private String userId;

    /** 
     * 姓名 
     */
    @TableField(value = "full_name")
    private String fullName;

    /** 
     * 电话 
     */
    @TableField(value = "phone")
    private String phone;

    /** 
     * 省 
     */
    @TableField(value = "province")
    private String province;

    /** 
     * 市 
     */
    @TableField(value = "city")
    private String city;

    /** 
     * 区 
     */
    @TableField(value = "area")
    private String area;

    /** 
     * 详细地址 
     */
    @TableField(value = "specific_address")
    private String specificAddress;

    /** 
     * 是否默认地址 
     */
    @TableField(value = "is_default")
    private Boolean isDefault;

}

