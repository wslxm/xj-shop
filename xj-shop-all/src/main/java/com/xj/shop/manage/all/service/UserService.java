package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.client.all.model.LoginUserDTO;
import com.xj.shop.client.all.model.RegisterUserDTO;
import com.xj.shop.manage.all.model.dto.UserDTO;
import com.xj.shop.manage.all.model.entity.User;
import com.xj.shop.manage.all.model.query.UserQuery;
import com.xj.shop.manage.all.model.vo.UserVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 工具--用户表 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 11:17:09
 */
public interface UserService extends IService<User> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<UserVO> findPage(UserQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return UserVO
     */
    UserVO findId(String id);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String insert(UserDTO dto);

    /**
     * id 编辑
     *
     * @param id id
     * @param dto dto
     * @return boolean
     */
    boolean upd(String id, UserDTO dto);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean del(String id);

    /**
     * 注册账号
     * @author wangsong
     * @param dto
     * @date 2022/8/25 0025 22:02
     * @return java.lang.String
     * @version 1.0.0
     */
    public String register(@RequestBody @Validated RegisterUserDTO dto);


    /**
     * 登录
     * @author wangsong
     * @param dto
     * @date 2022/8/25 0025 22:02
     * @return java.lang.Boolean
     * @version 1.0.0
     */
    Boolean login(@RequestBody @Validated LoginUserDTO dto);


    /**
     * 发送电子邮件验证码
     * 文档地址: https://www.hutool.cn/docs/#/extra/%E9%82%AE%E4%BB%B6%E5%B7%A5%E5%85%B7-MailUtil
     * @author wangsong
     * @mail 1720696548@qq.com
     * @date 2022/2/20 0020 14:23
     * @version 1.0.0
     */
    Boolean sendEmailAuthCode(String email);

}

