package com.xj.shop.manage.all.model.dto;

import com.xj.shop.enums.Shop;
import io.github.wslxm.springbootplus2.core.base.model.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

/**
 * 订单变化相关记录表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "OrderLogDTO 对象", description = "订单变化相关记录表")
public class OrderLogDTO extends BaseDto {

    private static final long serialVersionUID = -722372530983276544L;
    
    /** 
     * 订单id 
     */
    private String orderId;

     /**
     * 操作类型枚举 (字典code 1-用户下单,更多)
     */
    private Shop.OperationType operationType;

    /**
     * 操作前状态 
     */
    @Range(min=0, max=99L,message = "操作前状态 必须>=0 和 <=99")
    private Integer frontState;

    /** 
     * 操作后状态 
     */
    @Range(min=0, max=99L,message = "操作后状态 必须>=0 和 <=99")
    private Integer afterState;

    /** 
     * 备注信息 
     */
    private String remarks;

//    /**
//     * 扩展字段1
//     */
//    private String ext1;
//
//    /**
//     * 扩展字段2
//     */
//    private String ext2;
//
//    /**
//     * 扩展字段3
//     */
//    private String ext3;

}

