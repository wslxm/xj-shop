package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.dto.GoodsDTO;
import com.xj.shop.manage.all.model.entity.Goods;
import com.xj.shop.manage.all.model.query.GoodsQuery;
import com.xj.shop.manage.all.model.vo.GoodsVO;

/**
 * 商品表 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
public interface GoodsService extends IService<Goods> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<GoodsVO> findPage(GoodsQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return GoodsVO
     */
    GoodsVO findId(String id);

    /**
     * 添加
     *
     * @param dto dto
     * @return 主键id
     */
    String insert(GoodsDTO dto);

    /**
     * id 编辑
     *
     * @param id id
     * @param dto dto
     * @return boolean
     */
    boolean upd(String id, GoodsDTO dto);

    /**
     * 商品上下架
     *
     * @param id id
     * @param state dto
     * @return boolean
     */
    boolean updState(String id, Integer state);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean del(String id);

}

