package com.xj.shop.manage.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import com.xj.shop.manage.all.model.vo.OrderLogVO;
import com.xj.shop.manage.all.model.dto.OrderLogDTO;
import com.xj.shop.manage.all.model.query.OrderLogQuery;
import com.xj.shop.manage.all.service.OrderLogService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;

/**
 * 订单变化相关记录表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_ADMIN + "/all/orderLog")
@Api(value = "OrderLogController", tags = "订单变化相关记录表")
public class OrderLogController extends BaseController<OrderLogService> {

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表查询")
    public Result<IPage<OrderLogVO>> findPage(@ModelAttribute @Validated OrderLogQuery query) {
        return Result.success(baseService.findPage(query));
    }


}
