package com.xj.shop.manage.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.dto.GoodsDTO;
import com.xj.shop.manage.all.model.query.GoodsQuery;
import com.xj.shop.manage.all.model.vo.GoodsVO;
import com.xj.shop.manage.all.service.GoodsService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商品表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:32
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_ADMIN + "/all/goods")
@Api(value = "GoodsController", tags = "商品表")
public class GoodsController extends BaseController<GoodsService> {

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表查询")
    public Result<IPage<GoodsVO>> findPage(@ModelAttribute @Validated GoodsQuery query) {
        return Result.success(baseService.findPage(query));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "ID查询")
    public Result<GoodsVO> findId(@PathVariable String id) {
        return Result.success(baseService.findId(id));
    }

    @PostMapping
    @ApiOperation(value = "添加")
    public Result<String> insert(@RequestBody @Validated GoodsDTO dto) {
        return Result.successInsert(baseService.insert(dto));
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "ID编辑")
    public Result<Boolean> upd(@PathVariable String id, @RequestBody @Validated GoodsDTO dto) {
        return Result.successUpdate(baseService.upd(id, dto));
    }

    @PutMapping(value = "/{id}/state")
    @ApiOperation(value = "商品上下架")
    public Result<Boolean> updState(@PathVariable String id, @RequestParam Integer state) {
        return Result.successUpdate(baseService.updState(id, state));
    }


    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "ID删除")
    public Result<Boolean> del(@PathVariable String id) {
        return Result.successDelete(baseService.del(id));
    }

}
