package com.xj.shop.manage.all.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.shop.manage.all.model.entity.Collection;
import com.xj.shop.manage.all.model.query.CollectionQuery;
import com.xj.shop.manage.all.model.vo.CollectionVO;

/**
 * 我的收藏 Service
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:52:04
 */
public interface CollectionService extends IService<Collection> {

    /**
     * 列表查询
     *
     * @param query query
     * @return 分页列表数据
     */
    IPage<CollectionVO> findPage(CollectionQuery query);

    /**
     * id 查询
     *
     * @param id id
     * @return CollectionVO
     */
    CollectionVO findId(String id);


    /**
     * 收藏/取消收藏
     *
     * @param id
     * @return
     */
    boolean collect(String id,String goodsId);

}

