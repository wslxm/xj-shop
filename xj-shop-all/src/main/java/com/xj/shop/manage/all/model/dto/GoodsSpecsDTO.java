package com.xj.shop.manage.all.model.dto;

import io.github.wslxm.springbootplus2.core.base.model.BaseDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 商品表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:54:43
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "GoodsSpecsDTO 对象", description = "商品表")
public class GoodsSpecsDTO extends BaseDto {

    private static final long serialVersionUID = -722371863677898752L;
    /**
     * 商品规格名id
     */
    private String id;

    /**
     * 商品规格名 
     */
    @Length(min = 0, max = 32, message = "商品规格名 必须>=0 和 <=32位")
    @NotBlank(message = "商品规格名不能为空")
    private String specsName;

    /**
     * 商品规格价格 
     */
    @Range(min = 0, max = 9999999999L, message = "商品规格价格 必须>0 和 <=9999999999")
    @NotNull(message = "商品规格单价不能为空")
    private BigDecimal specsPrice;

    /**
     * 商品库存 
     */
    @Range(min = 1, max = 9999999999L, message = "商品库存 必须>=1 和 <=9999999999")
    @NotNull(message = "商品库存不能为空")
    private Integer specsStock;

    /**
     * 规格排序
     */
    @Range(min = 0, max = 9999999999L, message = "规格排序 必须>=0 和 <=9999999999")
    @NotNull(message = "规格排序不能为空")
    private Integer sort;
}

