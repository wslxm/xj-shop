package com.xj.shop.manage.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.dto.OrderUpdDTO;
import com.xj.shop.manage.all.model.query.OrderQuery;
import com.xj.shop.manage.all.model.vo.OrderVO;
import com.xj.shop.manage.all.service.OrderService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 订单表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_ADMIN + "/all/order")
@Api(value = "OrderController", tags = "订单表")
public class OrderController extends BaseController<OrderService> {

    @GetMapping(value = "/findPage")
    @ApiOperation(value = "列表查询")
    public Result<IPage<OrderVO>> findPage(@ModelAttribute @Validated OrderQuery query) {
        return Result.success(baseService.findPage(query));
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "ID查询")
    public Result<OrderVO> findId(@PathVariable String id) {
        return Result.success(baseService.findId(id));
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "ID编辑(只能备注/物流单号)")
    public Result<Boolean> upd(@PathVariable String id, @RequestBody @Validated OrderUpdDTO dto) {
        return Result.successUpdate(baseService.upd(id, dto));
    }


    @PutMapping(value = "/deliverGoods/{id}")
    @ApiOperation(value = "发货")
    public Result<Boolean> deliverGoods(@PathVariable String id, @RequestParam String logisticsNo) {
        return Result.successUpdate(baseService.deliverGoods(id, logisticsNo));
    }


    @PutMapping(value = "/updTotalPrice/{id}")
    @ApiOperation(value = "改价")
    public Result<Boolean> updTotalPrice(@PathVariable String id, @RequestParam BigDecimal totalPrice) {
        return Result.successUpdate(baseService.updTotalPrice(id, totalPrice));
    }

    @PutMapping(value = "/updState/{id}")
    @ApiOperation(value = "状态变更")
    public Result<Boolean> updState(@PathVariable String id, @RequestParam Integer state) {
        return Result.successUpdate(baseService.updState(id, state));
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "ID删除")
    public Result<Boolean> del(@PathVariable String id) {
        return Result.successDelete(baseService.del(id));
    }

}
