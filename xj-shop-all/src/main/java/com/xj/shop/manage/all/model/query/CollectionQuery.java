package com.xj.shop.manage.all.model.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;

/**
 * 我的收藏 Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:52:04
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "CollectionQuery 对象", description = "我的收藏")
public class CollectionQuery extends BaseQuery {

    private static final long serialVersionUID = -722371198159294464L;
    
    /** 
     * 用户id 
     */
    private Integer userId;

}

