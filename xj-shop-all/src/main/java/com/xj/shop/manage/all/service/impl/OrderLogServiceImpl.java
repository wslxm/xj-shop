package com.xj.shop.manage.all.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xj.shop.manage.all.mapper.OrderLogMapper;
import com.xj.shop.manage.all.model.dto.OrderLogDTO;
import com.xj.shop.manage.all.model.entity.OrderLog;
import com.xj.shop.manage.all.model.query.OrderLogQuery;
import com.xj.shop.manage.all.model.vo.OrderLogVO;
import com.xj.shop.manage.all.service.OrderLogService;
import io.github.wslxm.springbootplus2.core.base.service.impl.BaseServiceImpl;
import io.github.wslxm.springbootplus2.core.utils.BeanDtoVoUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * 订单变化相关记录表 ServiceImpl
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:57:22
 */
@Service
public class OrderLogServiceImpl extends BaseServiceImpl<OrderLogMapper, OrderLog> implements OrderLogService {

    @Override
    public IPage<OrderLogVO> findPage(OrderLogQuery query) {
        LambdaQueryWrapper<OrderLog> queryWrapper = new LambdaQueryWrapper<OrderLog>()
                .eq(StringUtils.isNotBlank(query.getOrderId()), OrderLog::getOrderId, query.getOrderId())
                .orderByDesc(OrderLog::getCreateTime);
        Page<OrderLog> page = this.page(new Page<>(query.getCurrent(), query.getSize()), queryWrapper);
        return BeanDtoVoUtil.pageVo(page, OrderLogVO.class);
    }


    @Override
    public String insert(OrderLogDTO dto) {
        OrderLog entity = dto.convert(OrderLog.class);
        entity.setOperationType(dto.getOperationType().getValue());
        entity.setOperationName(dto.getOperationType().getDesc());
        boolean b = this.save(entity);
        return entity.getId();
    }

    @Override
    public boolean insertBatch(List<OrderLogDTO> dtos) {
        List<OrderLog> orderLogs = new ArrayList<>();
        for (OrderLogDTO dto : dtos) {
            OrderLog entity = dto.convert(OrderLog.class);
            entity.setOperationType(dto.getOperationType().getValue());
            entity.setOperationName(dto.getOperationType().getDesc());
            orderLogs.add(entity);
        }
        this.saveBatch(orderLogs);
        return true;
    }

}
