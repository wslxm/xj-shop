package com.xj.shop.manage.all.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseEntity;

/**
 * 订单表 Entity
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:56:38
 */
@Data
@ToString(callSuper = true)
@TableName("t_xj_order")
@ApiModel(value = "Order 对象", description = "订单表")
public class Order extends BaseEntity {

    private static final long serialVersionUID = -722372348073873408L;
    
    /** 
     * 用户id 
     */
    @TableField(value = "user_id")
    private String userId;

    /** 
     * 单价 
     */
    @TableField(value = "unit_price")
    private BigDecimal unitPrice;

    /** 
     * 购买数量 
     */
    @TableField(value = "num")
    private Integer num;

    /** 
     * 总价 
     */
    @TableField(value = "total_price")
    private BigDecimal totalPrice;

    /** 
     * 订单号 
     */
    @TableField(value = "order_no")
    private String orderNo;

    /**
     * 交易号
     */
    @TableField(value = "pay_no")
    private String payNo;

    /** 
     * 商品id 
     */
    @TableField(value = "goods_id")
    private String goodsId;

    /** 
     * 商品规格id 
     */
    @TableField(value = "goods_specs_id")
    private String goodsSpecsId;

    /** 
     * 收货地址id 
     */
    @TableField(value = "address_id")
    private String addressId;

    /** 
     * 商品详情-快照 (json数据 订单查询商品使用) 
     */
    @TableField(value = "goods_details")
    private String goodsDetails;

    /** 
     * 收货地址详情-快照 (json数据 订单查询商品使用) 
     */
    @TableField(value = "address_details")
    private String addressDetails;

    /** 
     * 订单状态 (1-已下单 2-已支付 3-已发货  4-已完成  5-用户取消 6-平台拒单  7-支付失败  8-订单超时) 
     */
    @TableField(value = "state")
    private Integer state;

    /** 
     * 物流单号 
     */
    @TableField(value = "logistics_no")
    private String logisticsNo;

    /** 
     * 用户备注 (提交订单的备注) 
     */
    @TableField(value = "user_remark")
    private String userRemark;

    /** 
     * 平台备注 (订单异常下的备注) 
     */
    @TableField(value = "platform_remark")
    private String platformRemark;

    /** 
     * 支付时间 
     */
    @TableField(value = "pay_time")
    private LocalDateTime payTime;

    /** 
     * 发货时间 
     */
    @TableField(value = "deliver_time")
    private LocalDateTime deliverTime;

    /** 
     * 完成时间 
     */
    @TableField(value = "success_time")
    private LocalDateTime successTime;

}

