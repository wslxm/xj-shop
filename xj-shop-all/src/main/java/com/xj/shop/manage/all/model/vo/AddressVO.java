package com.xj.shop.manage.all.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseVo;

/**
 * 地址管理(收货) VO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 18:51:34
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "AddressVO 对象", description = "地址管理(收货)")
public class AddressVO extends BaseVo {

    private static final long serialVersionUID = -722371073802375168L;
    
    /** 
     * 用户id 
     */
    private String userId;

    /** 
     * 姓名 
     */
    private String fullName;

    /** 
     * 电话 
     */
    private String phone;

    /** 
     * 省 
     */
    private String province;

    /** 
     * 市 
     */
    private String city;

    /** 
     * 区 
     */
    private String area;

    /** 
     * 详细地址 
     */
    private String specificAddress;

    /** 
     * 是否默认地址 
     */
    private Boolean isDefault;

}

