package com.xj.shop.manage.all.model.query;

import io.github.wslxm.springbootplus2.core.base.model.BaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 类别表 Query
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "CategoryQuery 对象", description = "类别表")
public class CategoryQuery extends BaseQuery {

    private static final long serialVersionUID = -722317155697299456L;
    
    @ApiModelProperty(value = "id集合--非必传,没有获取所有", position = 1)
    private List<String> ids;

    @ApiModelProperty(value = "父id--非必传,没有获取所有",position = 2)
    private String pid;

    @ApiModelProperty(value = "是否禁用--非必传,（0-否，1-是)",position = 4)
    private Integer disable;

    @ApiModelProperty(value = "层级",position = 4)
    private Integer root;

}

