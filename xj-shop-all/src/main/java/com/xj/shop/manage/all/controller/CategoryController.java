package com.xj.shop.manage.all.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xj.shop.manage.all.model.dto.CategoryDTO;
import com.xj.shop.manage.all.model.query.CategoryQuery;
import com.xj.shop.manage.all.model.vo.CategoryVO;
import com.xj.shop.manage.all.service.CategoryService;
import io.github.wslxm.springbootplus2.core.base.controller.BaseController;
import io.github.wslxm.springbootplus2.core.constant.BaseConstant;
import io.github.wslxm.springbootplus2.core.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 类别表 前端控制器
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>
 *
 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
@RestController
@RequestMapping(BaseConstant.Uri.API_ADMIN + "/all/category")
@Api(value = "CategoryController", tags = "类别表")
public class CategoryController extends BaseController<CategoryService> {

    @GetMapping(value = "/tree")
    @ApiOperation(value = "树结构数据查询")
    public Result<List<CategoryVO>> tree(@ModelAttribute @Validated CategoryQuery query) {
        List<CategoryVO> tree = baseService.tree(query);
        return Result.success(tree);
    }



    @GetMapping(value = "/{id}")
    @ApiOperation(value = "ID查询")
    public Result<CategoryVO> findId(@PathVariable String id) {
        return Result.success(baseService.findId(id));
    }

    @PostMapping
    @ApiOperation(value = "添加")
    public Result<String> insert(@RequestBody @Validated CategoryDTO dto) {
        return Result.successInsert(baseService.insert(dto));
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "ID编辑")
    public Result<Boolean> upd(@PathVariable String id, @RequestBody @Validated CategoryDTO dto) {
        return Result.successUpdate(baseService.upd(id, dto));
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "ID删除")
    public Result<Boolean> del(@PathVariable String id) {
        return Result.successDelete(baseService.del(id));
    }

}
