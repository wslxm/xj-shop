package com.xj.shop.manage.all.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.github.wslxm.springbootplus2.core.base.model.BaseDto;

/**
 * 类别表 DTO
 *
 * <p>
 * ::本代码由[兮家小二]提供的代码生成器生成,如有问题,请手动修改 ::作者CSDN:https://blog.csdn.net/qq_41463655
 * </p>

 * @author ws
 * @email 1720696548@qq.com
 * @date 2022-08-24 15:17:19
 */
@Data
@ToString(callSuper = true)
@ApiModel(value = "CategoryDTO 对象", description = "类别表")
public class CategoryDTO extends BaseDto {

    private static final long serialVersionUID = -722317155189788672L;
    
    /** 
     * 父Id (顶级父id=0) 
     */
    @Length(min=0, max=32,message = "父Id  必须>=0 和 <=32位")
    private String pid;

    /** 
     * 分类code (不能重复) 
     */
    @Length(min=0, max=32,message = "分类code  必须>=0 和 <=32位")
    private String code;

    /** 
     * 分类名称 
     */
    @Length(min=0, max=32,message = "分类名称 必须>=0 和 <=32位")
    private String name;

    /** 
     * 分类描叙 
     */
    @Length(min=0, max=128,message = "分类描叙 必须>=0 和 <=128位")
    private String desc;

    /** 
     * 排序 
     */
    @Range(min=0, max=1215752191L,message = "排序 必须>=0 和 <=1215752191")
    private Integer sort;

    /** 
     * 禁用(通用字典 0-否 1-是) 
     */
    @Range(min=0, max=9L,message = "禁用 必须>=0 和 <=9")
    private Integer disable;

    /** 
     * 级别 (对应123  1-一级 2-二级 3-三级) 
     */
    private Integer root;

}

